<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="row" style="margin-top: 50px">
                <div class="col-lg-12 col-lg-offset-2 col-sm-12 col-sm-offset-2 col-md-12 col-md-offset-2 col-xs-12 col-xs-offset-2 text-center">
                    <h3><strong>Estamos preparando novidades para o nosso site, aguardem.</strong></h3>
                </div>
                <div class="col-lg-12 col-lg-offset-2 col-sm-12 col-sm-offset-2 col-md-12 col-md-offset-2 col-xs-12 col-xs-offset-2">
                    <img src="pagina-em-manutencao.png" class="img-responsive"/>
                </div>
            </div>
        </div>
    </body>
</html>