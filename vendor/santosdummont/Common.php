<?php

namespace santosdummont;

use DateTime,
    santosdummont\PHPMailer,
    Application\Controller\Common\Imagem,
    Application\Controller\Common\Arquivos;

class Common extends \Exception
{

    static function relatarExcecaoParaAdmin($msg)
    {
        //self::dispararEmail($msg);
    }

    static function retornoMsgXml($xmlstr)
    {
        $msg = '';
        $errors = simplexml_load_string($xmlstr, "SimpleXMLElement", LIBXML_NOCDATA);

        $list = json_encode($errors);
        $listerros = json_decode($list, TRUE);
        foreach ($listerros as $item) {
            $msg .= $item['code'] . '-' . $item['message'] . '<br/>';
        };

        return $msg;
    }

    static function encrypt_decrypt($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'foottsdevsis';
        $secret_iv = 'foottsdevsis';

        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    static function returnValor($str)
    {
        $string = preg_replace('/[^0-9,]/', '', $str);
        return str_replace(",", ".", $string);
    }

    static function montarDadosParaEmail($dados = [], $titulo = "", $info = [], $tipo = null)
    {
        $assunto = ($titulo == "") ? "Falha no Sistema!" : $titulo;

        $ip = self::getUserIP();
        $email = $dados['EMAIL'];
        $id = $dados['US_ID'];

        if ($tipo == 1 || $tipo == 2) {
            $link = $info['url'] . "/Login/Reset/" . self::encrypt_decrypt('encrypt', $id);
        }

        $html = '<html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>Santos Dumont - Notificações automática</title>
                    </head>
                    <body>
                        <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                            <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                <img src="' . URL . PATHSTATIC_URL . '/' . $info['logotipo'] . '">
                            </div>
                            <div class="container" style="padding:20px">
                                <table>
                                    <tr>
                                        <td>
                                            <h2><b>Santos Dumont - Notificações automática</b></h2>
                                            <br/>';
        if ($tipo == 1) {
            $html .= 'Olá, Você realizou um reset de senha.<br/>';
            $html .= 'Para gerar uma nova senha você deve clicar no link abaixo<br/><br/>';
            $html .= 'E-mail: ' . $email . '<br/>';
            $html .= 'IP: ' . $ip . ' <br/><br/>';
            $html .= 'Gere uma nova senha clicando aqui <a href="' . $link . '" target="_blank">Gerar Senha</a><br/>';
        } else if ($tipo == 2) {
            $html .= 'Olá, Foi realizado um cadastro no site.<br/>';
            $html .= 'Para gerar uma senha de acesso você deve clicar no link abaixo<br/><br/>';
            $html .= 'E-mail: ' . $email . '<br/>';
            $html .= 'IP: ' . $ip . ' <br/><br/>';
            $html .= 'Gere uma nova senha clicando aqui <a href="' . $link . '" target="_blank">Gerar Senha</a><br/>';
        }

        $html .= '</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';

        return self::configurarEmail($assunto, $html, $info);
    }

    static function getUserIP()
    {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    private static function configurarEmail($assunto, $html, $info = [])
    {
        $mail = new PHPMailer();

        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        //$mail->SMTPDebug = 2;
        $mail->SMTPSecure = "ssl";
        $mail->Host = $info['host'];
        $mail->Port = $info['porta'];
        $mail->SMTPAuth = true;
        $mail->Username = $info['username'];
        $mail->Password = $info['senha'];
        $mail->From = $info['from'];
        $mail->FromName = $info['fromname'];
        $mail->IsHTML(true);
        $mail->CharSet = 'utf-8';
        $mail->Subject = $assunto;
        $mail->Body = $html;

        return $mail;
    }

    static function dispararEmail($dados = [], $email = "", $titulo = "", $info = [], $tipo = null)
    {
        if (ENVIA_EMAIL) {
            $mail = self::montarDadosParaEmail($dados, $titulo, $info, $tipo);

            $mail->AddAddress($email, $dados['NOME_COMPLETO']);

            $envio = $mail->Send();

            $mail->ClearAllRecipients();
            $mail->ClearAttachments();

            return $envio;
        } else {
            return true;
        }
    }

    static function dispararEmailPersonalizado($assunto, $html, $email, $info, $cc = null)
    {
        if (ENVIA_EMAIL) {
            $mail = self::configurarEmail($assunto, $html, $info);
            $mail->AddAddress($email, $email);
            if (!empty($cc)) {
                $mail->addCC($cc, $cc);
                $mail->addCC('jeancesaral@gmail.com', 'jeancesaral@gmail.com');
            }
            $envio = $mail->Send();
            $mail->ClearAddresses();

            return $envio;
        } else {
            return true;
        }
    }

    static function validarEmail($email)
    {
        if (!preg_match("/^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$/", $email)) {
            self::gerarMensagem(OPCAOAVISO, 'E-MAIL INVÁLIDO');
        }
    }

    static function validaSeEstaLogado($permissaoUsuario = NULL)
    {
        if (!Session::get('logado')) {
            Common::gerarMensagem(OPCAOAVISO, 'LOGIN EXPIRADO', FALSE);
            Common::retornarPagina('Login');
        }

        self::validarPermissaoModulo($permissaoUsuario);
    }

    static function tratarCampoTipoArray($dados = [])
    {
        $resultado = '';
        foreach ($dados as $dado) {
            $resultado .= $dado . ',';
        }

        $resultado = rtrim($resultado, ',');

        return $resultado;
    }

    private static function validarPermissaoModulo($permissaoUsuario)
    {
        $permissao = array_key_exists($permissaoUsuario, $_SESSION["_MODULOSPERMITIDOS"]);

        if (!$permissao) {
            Common::gerarMensagem(OPCAOAVISO, 'SEM ACESSO A ESSE MÓDULO', FALSE);
            Common::retornarPagina('PainelAdministrativo');
        }
    }

    static function camuflarSenha($string)
    {
        $string = preg_replace('/[^0-9]/', '', hash('sha512', $string));
        $string = strtoupper(preg_replace('/[^a-z]/', '', hash('sha512', $string)));
        $string = base64_encode($string);
        $string = substr($string, 0, 6);

        return $string;
    }

    static function gerarSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        $retorno = '';
        $caracteres = '';

        $caracteres .= $lmin;

        if ($maiusculas)
            $caracteres .= $lmai;
        if ($numeros)
            $caracteres .= $num;
        if ($simbolos)
            $caracteres .= $simb;

        $len = strlen($caracteres);

        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand - 1];
        }

        return $retorno;
    }

    static function retornarPagina($url = "")
    {
        header('Location: ' . SITE_URL . '/' . $url);
        exit;
    }

    static function voltar()
    {
        if (empty($_SERVER['HTTP_REFERER'])) {
            $_SERVER['HTTP_REFERER'] = SITE_URL;
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }

    static function validarData($date = null)
    {
        $d = DateTime::createFromFormat('d/m/Y', $date);
        return ($d && $d->format('d/m/Y') === $date);
    }

    static function validarCampoObrigatorio($dados = [], $apelido = FALSE)
    {
        $msg = 0;
        $campo = "<ul>";
        $i = 0;
        if (!empty($dados)) {
            foreach ($dados as $key => $value) {
                $i++;
                if (empty($value)) {
                    $msg += 1;
                    if ($apelido) {
                        $key = $dados['#APELIDO#'][$i - 1];
                    }
                    $campo .= "<li>" . ucfirst($key) . "</li>";
                }
            }
            $campo = MSG_ALERT_CAMPOS_OBRIGATORIOS . $campo;
        } else {
            $msg = 1;
            $campo = "<ul> ACESSO NÃO PERMITIDO!";
        }

        if ($msg > 0) {
            self::gerarMensagem(OPCAOAVISO, $campo . "</ul>");
        }

        unset($dados['#APELIDO#']);
        return $dados;
    }

    static function StrReplaceMultiplo($parametros, $subject)
    {
        foreach ($parametros as $key => $value) {
            $subject = str_replace($key, $value, $subject);
        }

        return $subject;
    }

    static function validarLink($link)
    {
        //        if ($link != null && filter_var($link, FILTER_VALIDATE_URL) === FALSE) {
        //            self::gerarMensagem(OPCAOAVISO, AVISO_MSG_LINKINVALIDO);
        //            self::voltar();
        //        }
        return $link;
    }

    static function contarArray($dados = [])
    {
        $total = (sizeof($dados));
        $n = ZERO;
        foreach ($dados as $value) {
            if (empty($value)) {
                $n++;
            }
        }

        return $total - $n;
    }

    static function removerCaracteresEspeciais($nome)
    {
        $string = preg_replace("/[^a-zA-Z0-9_]/", "", strtr(utf8_decode($nome), utf8_decode("áàãâéêíóôõúüçñÁÀÃÂÉÊÍÓÔÕÚÜÇÑ "), "aaaaeeiooouucnAAAAEEIOOOUUCN_"));
        return str_replace('_', ' ', $string);
    }

    static function removerEspacosPontos($str)
    {
        $nome = preg_replace('/( )+/', ' ', $str);
        $string = preg_replace("/[^a-zA-Z0-9]/", "-", strtr(utf8_decode($nome), utf8_decode("áàãâéêíóôõúüçñÁÀÃÂÉÊÍÓÔÕÚÜÇÑ "), "aaaaeeiooouucnAAAAEEIOOOUUCN-"));
        return str_replace('_', '-', $string);
    }

    static function removerEspacosDuplos($str)
    {
        $str = preg_replace('/( )+/', ' ', $str);
    }

    static function validarCNPJ($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        $erro = ZERO;

        if (strlen($cnpj) != 14)
            $erro++;

        switch ($cnpj) {
            case '00000000000000':
            case '11111111111111':
            case '22222222222222':
            case '33333333333333':
            case '44444444444444':
            case '55555555555555':
            case '66666666666666':
            case '77777777777777':
            case '88888888888888':
            case '99999999999999':
                $erro++;
        }

        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;
        if ($cnpj[12] != ($resto < 2 ? 0 : 11 - $resto))
            $erro++;

        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($erro > ZERO) {
            self::gerarMensagem(OPCAOAVISO, "CNPJ inválido!!");
        }

        return $cnpj[13] == ($resto < 2 ? 0 : 11 - $resto);
    }

    static function formatarData($data)
    {
        return implode('-', array_reverse(explode('/', $data)));
    }

    static function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;

        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public static function validarInputsObrigatorio($dados = [], $redir = null, $modulo = null)
    {
        $qtde = 0;
        $campos = "<ul align='left'>";
        $msg = "Campos obrigatórios não preenchido!";
        $situacao = "danger";
        $acao = 'acao';

        foreach ($dados as $key => $value) {
            if (empty($value)) {

                $qtde += 1;
                $campos .= "<li>" . ucfirst($key) . "</li>";
            }
        }

        $msg .= '<br>' . $campos . "</ul>";

        if ($qtde > 0) {
            self::alert($msg, $situacao, $acao, $modulo);
            self::redir($redir);
        }
    }

    public static function validarAcessoObrigatorio($dados = [], $redir = null)
    {
        $qtde = 0;
        $campos = "<ul align='left'>";
        $msg = "Campos obrigatório não preenchido!";
        $situacao = "alert-danger";
        $acao = 'acao';

        foreach ($dados as $key => $value) {
            if (empty($value)) {

                $qtde += 1;
                $campos .= "<li>" . ucfirst($key) . "</li>";
            }
        }

        $msg .= '<br>' . $campos . "</ul>";

        if ($qtde > 0) {
            self::alert($msg, $situacao, $acao);
            if (empty($redir)) {
                self::voltar();
            } else {
                self::redir($redir);
            }
        }
    }

    public static function validarEmBranco($dado)
    {

        $qtde = 0;

        if (strlen($dado) == 0) {
            $qtde = 1;
        }

        if (($qtde == 1)) {
            $msg = "Campo obrigatório não preenchido!";
            $situacao = "alert-danger";
            $acao = 'acao';
            self::alert($msg, $situacao, $acao);
            self::voltar();
        }
    }

    public static function alert($msg = 'Operação realizada com Sucesso', $situacao = '', $acao = 'acao', $modulo = null)
    {

        if ($situacao == '') {
            $situacao = " uk-notify uk-notify-top-center ";
        }
        Session::set($acao, TRUE);
        Session::set("acao-class", $situacao);
        Session::set("acao-msg", $msg);

        if ($modulo != null) {
            Session::set("acao-tab", $modulo);
        }
    }

    static function gerarMensagem($tipoDaMsg, $msg = "", $redirecionar = TRUE)
    {
        Session::set("sessao", TRUE);

        if ($tipoDaMsg === OPCAOSUCESSO) {
            Session::set("class", "success");
            Session::set("msg", '<i class="uk-icon-check"></i> ' . $msg);
        }

        if ($tipoDaMsg === OPCAOAVISO) {
            Session::set("class", "warning");
            Session::set("msg", '<i class="uk-icon-warning"></i> ' . $msg);
        }

        if ($tipoDaMsg === OPCAOERRO) {
            $msg = MSG_ERRO_EXCECAOPADRAO . "<br><br> Causa: " . $msg;
            Session::set("class", "danger");
            Session::set("msg", '<i class="uk-icon-danger"></i> ' . $msg);
        }

        if ($redirecionar) {
            self::voltar();
        }
    }

    public static function redir($url = "")
    {
        header('location: ' . SITE_URL . '/' . $url);
        exit();
    }

    public static function retornoWSLista($param)
    {
        return $param['list'];
    }

    public static function remoteDir($pasta)
    {

        rmdir($pasta);
    }

    public static function imagem_existe($pasta = null, $imagem, $tipo, $op = 'large')
    {
        if ($tipo == 1) {
            if (is_file(FOTOPARCEIRO_PATH . '/' . $imagem)) {
                $file_headers = file_exists(FOTOPARCEIRO_PATH . '/' . $imagem);

                if (!$file_headers) {
                    return IMG_PADRAO_PARCEIRO;
                } else {
                    return FOTOPARCEIROS_URL . '/' . $imagem;
                }
            } else {
                return IMG_PADRAO_PARCEIRO;
            }
        }

        if ($tipo == 2) {

            if (!empty($pasta)) {
                $pasta = '/' . $pasta;
            }

            $file_headers = file_exists(FOTOPRODUTO_PATH . $pasta . '/' . $op . '/' . $imagem);

            if (!$file_headers) {
                return IMG_PADRAO_PRODUTO;
            } else {
                return '/public_html/static/fotoproduto' . $pasta . '/' . $op . '/' . $imagem;
            }
        }

        if ($tipo == 3) {
            if (is_file(FOTOCLIENTE_PATH . '/' . $pasta . '/' . $imagem)) {
                $file_headers = file_exists(FOTOCLIENTE_PATH . '/' . $pasta . '/' . $imagem);

                if (!$file_headers) {
                    return IMG_PADRAO_CLIENTE;
                } else {
                    return FOTOCLIENTE_URL . '/' . $pasta . '/' . $imagem;
                }
            } else {
                return IMG_PADRAO_CLIENTE;
            }
        }
    }

    public function dir_is_empty($dir) {
        if(is_dir($dir)){
            $count = 0;
            foreach (new \DirectoryIterator( $dir ) as $fileInfo) {
                if ( $fileInfo->isDot() || $fileInfo->getBasename() == '.DS_Store' ) {
                    continue;
                }
                $count++;
            }
            return ($count === 0);
        }else{
            return true;
        }
    }

    public function removeDir($dir){
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileinfo) {
            $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
            $todo($fileinfo->getRealPath());
        }

        rmdir($dir);
    }

    public static function reArrayFiles(&$file_post)
    {
        $file_ary = [];
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }

    public static function salvarImagemAjax($file, $pasta, $config)
    {
        $Imagem = new Imagem($config);

        if (!file_exists($pasta)) {
            mkdir($pasta, 0777, true);
        }

        return $Imagem->executarajax($pasta, $file, 1);
    }

    public function salvarImagem($file, $pasta, $config = null)
    {
        if(!empty($config)){
            $Imagem = new Imagem($config);
        }else{
            $Imagem = new Imagem();
        }

        if (!file_exists($pasta)) {
            mkdir($pasta, 0777, true);
        }

        return $Imagem->executar($pasta, $file, 1);
    }

    public function validarImagem($file, $config)
    {
        $Imagem = new Imagem($config);

        return $Imagem->validarImagem($file);
    }

    public function removerImagem($caminho_imagem)
    {
        if (file_exists($caminho_imagem))
            @unlink($caminho_imagem);
        clearstatcache(TRUE, $caminho_imagem);
    }

    public function salvarArquivo($arquivo)
    {
        $arquivos = new Arquivos;

        return $arquivos->executar(PASTAARQUIVOS_URL, $arquivo);
    }

    public function removerArquivo($nome_arquivo)
    {
        $arquivos = new Arquivos;

        $arquivos->deletarDocumentoDiretorio(PASTAARQUIVOS_URL, $nome_arquivo);
    }
}
