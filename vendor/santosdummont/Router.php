<?php
namespace santosdummont;

use santosdummont\Request;

class Router
{

    public static function run(Request $request)
    {
        // Obtêm os segmentos da URL a partir do objeto $request
        $controlador = ucfirst($request->getControlador());
        $metodo = $request->getMetodo();
        $args = (array) $request->getArgs();

    	if (empty($controlador)) {
            $controlador = 'Home';
        }
    
        $caminho = CONTROLLER_PATH;
        $arquivo = $caminho . DIRECTORY_SEPARATOR . $controlador . '.php';

        $pasta = explode('/', PASTAS_CONTROLLER);

        if (!file_exists($arquivo)) {
            foreach ($pasta as $pastas) {
                $controladorTMP = ucfirst($request->getControlador());

                $testaArquivo = $caminho . DIRECTORY_SEPARATOR . $pastas . DIRECTORY_SEPARATOR . $controladorTMP . '.php';

                if (file_exists($testaArquivo)) {
                    $arquivo = $caminho . DIRECTORY_SEPARATOR . $pastas . DIRECTORY_SEPARATOR . $controladorTMP . '.php';
                    $controlador = 'Application\Controller\\' . $pastas . '\\' . ucfirst($controladorTMP);
                }
            }
        } else {
            $controlador = 'Application\Controller\\' . ucfirst($controlador);
        }

        if (!file_exists($arquivo)) {
            self::error("Controlador=> {$controlador} não foi encontrado");
            exit();
        }

        $controlador = new $controlador();

        if (!is_callable(array($controlador, $metodo))) {
            self::error("Método=> {$metodo} não foi encontrado");
            return;
        }

        call_user_func_array(array($controlador, $metodo), $args);
    }

    protected static function error($msg)
    {
        throw new \Exception($msg);
    }
}
