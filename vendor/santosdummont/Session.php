<?php
namespace santosdummont;

class Session
{

    public static function inicializar()
    {
        ob_start();
        if (!isset($_SESSION)) {
            session_name('Sessaosantosdummont');
            session_start();
        }
        session_regenerate_id();
    }

    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function get($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
    }

    public static function delete($key)
    {
        unset($_SESSION[$key]);
    }

    public static function destroy()
    {
        session_destroy();
    }
}
