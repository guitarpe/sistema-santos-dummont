<?php

namespace santosdummont;

use santosdummont\Session,
    santosdummont\Common,
    santosdummont\ReCaptcha;

require_once SYSTEM_PATH . 'vendor/santosdummont/vendor/autoload.php';

class Controller
{

    protected $session;

    public function __construct()
    {
        Session::inicializar();
    }

    protected function loadView($nome, $vars = null)
    {
        if (is_array($vars) && count($vars) > 0) {
            extract($vars, EXTR_PREFIX_SAME, 'data');
        }

        $arquivo = VIEW_PATH . '/' . $nome . '.phtml';

        if (!file_exists($arquivo)) {
            $this->error("Houve um erro. Essa View {$nome} nao existe.");
        }

        require_once($arquivo);
    }

    protected function loadModel($nome, $apelido = "")
    {
        $this->$nome = new $nome();

        if ($apelido !== '') {
            $this->$apelido = &$this->$nome; //Passando por referencia
        }
    }

    protected function error($msg)
    {
        throw new \Exception($msg);
    }

    protected function validarRetornoServico($retorno = array())
    {
        if ($retorno['O_WRET_COD'] > ZERO) {
            Common::gerarMensagem(OPCAOAVISO, $retorno['O_ARET_DESC_CURTO']);
        }
    }

    protected function carregarMenu()
    {
        $token = Session::get('token');

        $request = new Request();
        $dados['controller'] = $request->getControlador();
        $dados['metodo'] = $request->getMetodo() == 'main' ? $request->getControlador() : $request->getMetodo();
        $dados['config'] = Common::retornoWSLista($this->model->Configuracoes($token))[0];
        $dados['notificacoes'] = Common::retornoWSLista($this->model->getNotificacoes($token))[0];

        //menu
        if (empty(Session::get('ses-menu'))) {
            $menu = Common::retornoWSLista($this->model->ExibirMenu($token));
            Session::set("ses-menu", $menu);

            $dados['menu'] = $menu;
        } else {
            $dados['menu'] = Session::get('ses-menu');
        }

        //dados do usuário
        if (empty(Session::get('ses-usu'))) {
            $usu = Common::retornoWSLista($this->model->UsuarioLogado($token))[0];
            Session::set("ses-usu", $usu);

            $dados['logado'] = $usu;
        } else {
            $dados['logado'] = Session::get('ses-usu');
        }

        if ($dados['logado']['O_TOKEN_INVALIDO'] == 'S') {
            Session::destroy();
            Common::redir('Login');
        }

        return $dados;
    }

    protected function prepararView($view, $dados = array())
    {
        $selecionado = Session::get('selecionado');

        if (empty($dados['menu'])) {
            $dados += $this->carregarMenu();
        }

        $dados['selecionado'] = $selecionado;

        $this->loadView("Template/cabecalho", $dados);
        $this->loadView("Template/mainmenu", $dados);
        $this->loadView("Template/topbar", $dados);
        $this->loadView($view, $dados);
        $this->loadView("Template/rodape", $dados);
    }

    protected function prepararBasicView($view, $dados = array())
    {
        $this->loadView($view, $dados);
    }

    protected function prepararViewLogin($view, $dados = array())
    {
        $dados['config'] = Common::retornoWSLista($this->model->ConfiguracoesNoToken())[0];

        $this->loadView("Template/cablogin", $dados);
        $this->loadView($view, $dados);
        $this->loadView("Template/rodapelogin", $dados);
    }

    protected function loadException(\Exception $msg, $voltar = TRUE)
    {
        Common::relatarExcecaoParaAdmin($msg);
        Common::gerarMensagem(OPCAOERRO, $msg->getMessage(), $voltar);
    }

    function validarCaptcha($captcha, $ip)
    {
        $recaptcha = new ReCaptcha(CAPTCHA_SECRET);

        $resp = $recaptcha->verify($captcha, $ip);

        if ($resp->isSuccess()) {
            return ['erro' => false, 'message' => 'Sucesso!'];
        } else {
            return ['erro' => true, 'message' => $resp->getErrorCodes()];
        }
    }

    function consumir_cetrus($dados)
    {
        if (!empty($dados['sku'])) {
            $url = 'http://' . $dados['ip'] . URL_API_CONSULTAR . '{"IdUsuario": "' . INT_ID_USUARIO . '", "Senha": "' . INT_ID_SENHA . '", "Produto_Id": "' . $dados['sku'] . '"}';
        } else {
            $url = 'http://' . $dados['ip'] . URL_API_CONSULTAR . '{"IdUsuario": "' . INT_ID_USUARIO . '", "Senha": "' . INT_ID_SENHA . '", "Produto_Id": ""}'; //260-9829
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_ENCODING, "utf-8");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $return = curl_exec($ch);

        if ($return !== FALSE) {
            $json = preg_replace('/[\x00-\x1F\x7F]/', '', '{"erro":0, "produtos":' . self::remover_carac_esp($return) . '}');
        } else {
            $json = '{"erro":1, "msg":' . curl_error($ch) . '}';

            self::enviarEmailNotificacaoCetrus(curl_error($ch));
        }
        curl_close($ch);

        return $json;
    }

    function consulta_cetrus($dados)
    {
        $url = 'http://' . $dados['ip'] . URL_APU_CONSUMIR . '{"IdUsuario": "' . INT_ID_USUARIO . '", "Senha": "' . INT_ID_SENHA . '", "Tabela": "PRODUTOS", "Chave_Id": "' . $dados['sku'] . '"}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if ($result !== FALSE) {
            $retorno = '{"erro":0, "retorno": ' . $result . '}';
        } else {
            $retorno = '{"erro":1, "retorno": [' . curl_error($ch) . '}';
        }

        curl_close($ch);

        $ret = json_decode($retorno, true);

        return $ret;
    }

    function sair()
    {
        Session::destroy();
        Common::retornarPagina(get_called_class());
    }

    function remover_carac_esp($url)
    {
        $url = html_entity_decode($url);
        $url = preg_replace('/[>]/', '', $url);
        $url = preg_replace('/[<]/', '', $url);

        return $url;
    }

    function log_registra($acao, $result, $ok)
    {
        if ($ok === true) {
            $message_erro = 'SUCESSO';
            $nome_arq = 'log_exec_';
        } else {
            $message_erro = $result;
            $nome_arq = 'log_erro_';
        }

        $log_txt = '';
        $arquivo = LOGS_URL;
        $arquivo_log = $arquivo . '/' . $nome_arq . date("Ymd") . ".log";

        $arq = @fopen($arquivo_log, "ab");
        $log_txt .= date("d/m/Y G:i:s") . " ";
        $log_txt .= $_SERVER["SCRIPT_NAME"] . " ";
        $log_txt .= $acao . " ";
        $log_txt .= $result . " ";
        $log_txt .= $message_erro . "\r\n";

        @fwrite($arq, $log_txt);
        @fclose($arq);
    }

    public function validar_cpf($str)
    {
        // Extrai somente os números
        $cpf = preg_replace('/[^0-9]/', '', (string) $str);

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    }

    public function validar_cnpj($str)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $str);

        // Valida tamanho
        if (strlen($cnpj) != 14) {
            return false;
        }

        // Lista de CNPJs inválidos
        $invalidos = [
            '00000000000000',
            '11111111111111',
            '22222222222222',
            '33333333333333',
            '44444444444444',
            '55555555555555',
            '66666666666666',
            '77777777777777',
            '88888888888888',
            '99999999999999'
        ];

        // Verifica se o CNPJ está na lista de inválidos
        if (in_array($cnpj, $invalidos)) {
            return false;
        }

        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($cnpj[12] != ($resto < 2 ? 0 : 11 - $resto)) {
            return false;
        }

        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        return $cnpj[13] == ($resto < 2 ? 0 : 11 - $resto);
    }

    private function enviarEmailNotificacaoCetrus($erro)
    {

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        $config = Common::retornoWSLista($this->model->ConfigEnvioEmail())[0];

        $info = array(
            'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
            'fromname' => $config['NOME_CONTATO_DEFAULT'],
            'url' => $config['URL_SISTEMA'],
            'logotipo' => $config['LOGOTIPO']
        );

        $email = $config['EMAIL_REC_NOTIFICACOES'];
        $emailcc = 'guitarpe@hotmail.com';

        $assunto = 'Santos Dumont - Erro de Sincronização de Produtos';


        $html = '<html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>' . $assunto . '</title>
                    </head>
                    <body>
                        <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                            <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                <img src="' . URL_IMG_MAIL . '/' . $config['LOGOTIPO'] . '">
                            </div>
                            <div class="container" style="padding:20px">
                                <table>
                                    <tr>
                                        <td>
                                            Ocorreu um erro ao sincronizar os produtos:<br/>
                                            Erro: ' . $erro . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br/>Acesse o painel administrativo nas configurações ou entre em contato com a Cetrus
                                            <br/>
                                            <br/>
                                            <h3><strong><a href="' . URL . '/admin" target="_blank">Painel Administrativo</a></strong></h3>
                                            <br/>
                                            <h5>Registro enviado em: ' . gmdate("d/m/Y H:i:s") . '</h5>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';

        $enviado = Common::dispararEmailPersonalizado('Santos Dumont - Sincronização de Produtos', $html, $email, $info, $emailcc);
    }
}
