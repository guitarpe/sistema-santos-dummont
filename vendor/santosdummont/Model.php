<?php

namespace santosdummont;

class Model
{

    private $_sistema;

    function __construct($sistema = '')
    {
        $this->_sistema = $sistema ? $sistema : get_called_class();
    }

    protected function autenticacaoWebServices($metodo, $parametros = [])
    {
        try {

            $options = array(
                'location'          => WS_LINK,
                'uri'               => WS_URI,
                'trace'             => WS_TRACE,
                'exceptions'        => true,
                'soap_version'      => SOAP_1_1,
                'cache_wsdl'        => WSDL_CACHE_NONE,
                'stream_context'    => stream_context_create(
                    [
                        'ssl' => [
                            'verify_peer'      => false,
                            'verify_peer_name' => false,
                        ]
                    ]
                )
            );

            ini_set('soap.wsdl_cache_enabled', 0);
            ini_set('soap.wsdl_cache_ttl', 0);

            $client = new \SoapClient(NULL, $options);

            $autenticacao = new \stdClass();
            $autenticacao->sistema = 'SantosDummont';
            $autenticacao->ip = filter_input(INPUT_SERVER, 'REQUEST_URI');

            $header_params = new \SoapVar($autenticacao, SOAP_ENC_OBJECT);
            $header = new \SoapHeader("WSSantosDummont", "authHeader", $header_params, false);

            $client->__setSoapHeaders($header);
            $retorno = $client->chamaMetodos($parametros, $metodo);

            $list = json_decode($retorno, true);

            $token_invalido = $this->verificarToken($list);

            if ($token_invalido == "S") {

                Session::destroy();

                $msg = "Token expirado, realize o login novamente";
                $situacao = "alert-danger";
                $acao = 'acao';

                Common::alert($msg, $situacao, $acao);
                //Common::redir('Login');
            }

            return $retorno;
        } catch (\SoapFault $e) {
            echo "ERRO-WS- $metodo CAUSA[" . $e->getMessage() . "] Não foi possível estabelecer conexão com o servidor de WS, tente mais tarde.";
            Common::relatarExcecaoParaAdmin($e);
            Common::gerarMensagem(OPCAOERRO, $e->getMessage(), FALSE);
        }
    }

    public function ExibirMenu($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDExibirMenu', $parametros), true);
    }

    public function MenuSelecionado($token, $metodo, $controller)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_METODO' => "$metodo",
            'I_CONTROLLER' => "$controller"
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDMenuSelecionado', $parametros), true);
    }

    public function Configuracoes($token = null)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDConfiguracoesADM', $parametros), true);
    }

    public function ConfiguracoesNoToken()
    {
        $parametros = [];

        return json_decode(self::autenticacaoWebServices('servicoSDConfiguracoesNotoken', $parametros), true);
    }

    public function getNotificacoes($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDLerNotificacoes', $parametros), true);
    }

    public function UsuarioLogado($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDUsuarioLogado', $parametros), true);
    }

    public function ListaMenusCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDListaMenusCadastrados', $parametros), true);
    }

    public function ListaFabricantesCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDListaFabricantes', $parametros), true);
    }

    public function LerNotificacao($token, $op, $tipo)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_OP' => $op,
            'I_TIPO' => $tipo
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDNotificacaoLida', $parametros), true);
    }

    public function ConfigEnvioEmail()
    {
        $parametros = [];

        return json_decode(self::autenticacaoWebServices('servicoSDConfigEnvioEmail', $parametros), true);
    }

    protected function verificarToken($list)
    {
        $token = null;

        if ($list['erro'] == 1) {
            $token = $list['att_token'];
        } else {

            foreach ($list['list'] as $key => $data) {
                if (is_array($data)) {
                    foreach ($data as $key => $dados) {
                        $token = $data['O_TOKEN_INVALIDO'];
                        break;
                    }
                } else {
                    $token = $list['list']['O_TOKEN_INVALIDO'];
                    break;
                }
            }
        }

        return $token;
    }
}
