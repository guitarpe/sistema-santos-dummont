<?php
$url = 'https://www.santosdumontcomp.com.br/admin/Automatico/SincronizarPedidosCron';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);

if ($result !== FALSE) {
    $ret = json_decode($result, true);
} else {
    $ret = curl_error($ch);
}

return $ret;
