<?php
$url = filter_input(INPUT_SERVER, 'HTTP_HOST');

if ($url === 'localhost') {
    $diretorio = '/portfolio/santosdumont';
    define('URL', 'http://localhost');
    define('SITE_URL', '/portfolio/santosdumont');
    define('FOTOCLIENTE_PATH', substr(realpath(dirname(__FILE__)), 0, -7) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotocliente');
    define('FOTOPARCEIRO_PATH', substr(realpath(dirname(__FILE__)), 0, -7) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotoparceiro');
    define('FOTOUSUARIO_PATH', substr(realpath(dirname(__FILE__)), 0, -7) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotousuario');
    define('BANNERS_PATH', substr(realpath(dirname(__FILE__)), 0, -7) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'banners');
    define('ENVIA_EMAIL', true);
} else {
    $diretorio = '/admin';
    define('URL', 'https://www.santosdumontcomp.com.br');
    define('SITE_URL', $diretorio);
    define('FOTOCLIENTE_PATH', substr(realpath(dirname(__FILE__)), 0, -12) . DIRECTORY_SEPARATOR . 'sistema' . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotocliente');
    define('FOTOPARCEIRO_PATH', substr(realpath(dirname(__FILE__)), 0, -12) . DIRECTORY_SEPARATOR . 'sistema' . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotoparceiro');
    define('FOTOUSUARIO_PATH', substr(realpath(dirname(__FILE__)), 0, -12) . DIRECTORY_SEPARATOR . 'sistema' . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotousuario');
    define('BANNERS_PATH', substr(realpath(dirname(__FILE__)), 0, -12) . DIRECTORY_SEPARATOR . 'sistema' . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'banners');
    define('ENVIA_EMAIL', true);
}

define('TITLE_SITE', 'Santos Dumont - ADM');
define('URL_SITE', $diretorio);
define('IMAGEM_URL', $diretorio . '/public_html/static/img');
define('LOGS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/logs');
define('PASTAARQUIVOS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/arquivos');
define('FOTOPRODUTO_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/fotoproduto');
define('FOTOPARCEIROS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/fotoparceiro');
define('FOTOUSUARIOS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/fotousuario');
define('FOTOCLIENTES_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/fotocliente');
define('FOTOCATEGORIAS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/fotocategoria');
define('BANNERS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/banners');
define('MODAIS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/modais');
define('PATHIMAGEUPLOAD', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/img');
define('PATHSTATIC', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static');
define('THUMBFOTOPRODUTO_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/fotoproduto/thumbs');
define('SYSTEM_PATH', substr(realpath(dirname(__FILE__)), 0, -6));
define('CONTROLLER_PATH', substr(realpath(dirname(__FILE__)), 0, -6) . 'src' . DIRECTORY_SEPARATOR . 'Application' . DIRECTORY_SEPARATOR . 'Controller');
define('URL_ATUAL', filter_input(INPUT_SERVER, 'HTTP_HOST') . filter_input(INPUT_SERVER, 'REQUEST_URI'));
define('FOTOPRODUTO_PATH', $_SERVER['DOCUMENT_ROOT'] . $diretorio . '/public_html/static/fotoproduto');
define('THUMBFOTOPRODUTO_PATH', $_SERVER['DOCUMENT_ROOT'] . $diretorio . '/public_html/static/fotoproduto/thumbs');
define('PATH_STATIC_DATA', $_SERVER['DOCUMENT_ROOT'] . $diretorio . '/public_html/static/data');

//ENVIO DE EMAILS DO SISTEMA
define('URL_IMG_MAIL', 'https://www.santosdumontcomp.com.br/admin/public_html/static/');

define('JS_URL', $diretorio . '/public_html/static/JavaScript');
define('CSS_URL', $diretorio . '/public_html/static/Css');
define('CSS_BOOTSTRAP_URL', $diretorio . '/public_html/static/Css/bootstrap');
define('CSS_DATAPICKER_URL', $diretorio . '/public_html/static/Css/datePicker');
define('CSS_FONT_ENTYPO_URL', $diretorio . '/public_html/static/Css/font-icons/entypo/css');
define('CSS_FONT_FA_URL', $diretorio . '/public_html/static/Css/font-icons/font-awesome/css');
define('CSS_GLOBAL_URL', $diretorio . '/public_html/static/Css/global');
define('CSS_JVECTORMAP_URL', $diretorio . '/public_html/static/Css/jvectormap');
define('CSS_RICKCHAW_URL', $diretorio . '/public_html/static/Css/rickshaw');
define('CSS_JQUERY_UI_URL', $diretorio . '/public_html/static/Css/jquery-ui');
define('URL_CSS_UIKIT_PATH', $diretorio . '/public_html/static/Css/Uikit');
define('URL_CSS_DATATABLE', $diretorio . '/public_html/static/Css/dataTable');
define('URL_CSS_PLUGIN_UPLOAD_URL', $diretorio . '/public_html/static/Css/pluginUpload');
define('JS_PLUGIN_UPLOAD_URL', $diretorio . '/public_html/static/JavaScript/pluginUpload');
define('JS_BOOTSTRAP_URL', $diretorio . '/public_html/static/JavaScript/bootstrap');
define('JS_ACESSO_URL', $diretorio . '/public_html/static/JavaScript/acesso');
define('JS_JQUERY_URL', $diretorio . '/public_html/static/JavaScript/jquery');
define('JS_JQUERY_UI_URL', $diretorio . '/public_html/static/JavaScript/jquery-ui');
define('JS_GLOBAL_URL', $diretorio . '/public_html/static/JavaScript/global');
define('JS_TINYMCE_URL', $diretorio . '/public_html/static/JavaScript/tinymce');
define('JS_GSAP_URL', $diretorio . '/public_html/static/JavaScript/gsap');
define('JS_DATAPICKER_URL', $diretorio . '/public_html/static/JavaScript/dataPicker');
define('JS_DATATABLE_URL', $diretorio . '/public_html/static/JavaScript/dataTable');
define('JS_SELECT2_URL', $diretorio . '/public_html/static/JavaScript/select2');
define('JS_JVECTORMAP_URL', $diretorio . '/public_html/static/JavaScript/jvectormap');
define('JS_RICKCHAW_URL', $diretorio . '/public_html/static/JavaScript/rickshaw');
define('JS_FULLCALENDAR_URL', $diretorio . '/public_html/static/JavaScript/fullcalendar');
define('URL_JS_UIKIT_PATH', $diretorio . '/public_html/static/JavaScript/Uikit');
define('CSS_LIGHTBOX', $diretorio . '/public_html/static/Css/lightbox');
define('JS_LIGHTBOX', $diretorio . '/public_html/static/JavaScript/lightbox');
define('JS_FANCYBOX', $diretorio . '/public_html/static/JavaScript/fancybox');
define('JS_TELAS', $diretorio . '/public_html/static/JavaScript/telas');
define('URL_ICONES', $diretorio . '/public_html/static/Icones');
define('URL_ICONES_ICONIC', $diretorio . '/public_html/static/Icones/iconic');
define('URL_IMG', $diretorio . '/public_html/static/img');
define('PASTAMODELOS_URL', $diretorio . '/public_html/static/modelos');
define('PATHSTATIC_URL', $diretorio . '/public_html/static');
define('PASTAS_CONTROLLER', 'Aparencia/Campanhas/Clientes/Common/Config/Estatisticas/Estoque/Regras/Relatorios/Vendas');

/*
define('CAPTCHA_KEY', '6LfU3ngUAAAAAMb7nA4LzadrapV_SHadNhAp3PS1');
define('GA_TRACKING_ID', 'UA-139154933-1');
define('GA_PERFIL_SITE_ID', '139154933');
define('US_GOOGLE', 'guitarpe@hotmail.com');
define('PASS_GOOGLE', 'Pez@112713');
key=API_KEY = AIzaSyBwiZP8L7_Y3QwMAjEDJMf-AmgRvpkzieU
GOOGLE MERCHANT=https://merchants.google.com/mc/overview?a=135912943
*/
