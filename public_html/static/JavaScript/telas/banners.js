jQuery(document).ready(function ($) {

    $('.form_submit_banner').click(function (e) {
        e.preventDefault();
        var form = $(this).closest('form').attr("id");
        var infInicialf = "O campo "
        var varPreencf = " deve ser preenchido!<br>";
        var msgf = "";

        $("form#" + form + " input, form#" + form + " select, form#" + form + " textarea").each(function () {
            if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {

                var input = $(this).attr('name');
                var val = $(this).val();
                var verificar = $(this).data('verif');
                var nomecampo = $(this).data('name');

                if (verificar != false) {
                    if (input != 'banner' && input != 'banner_md' && input != 'banner_sm') {
                        if ($(this).val() === "") {
                            msgf += infInicialf + nomecampo + varPreencf;
                        }
                    } else {
                        if ($('#id').val() == 0) {
                            if ($(this).val() === "") {
                                msgf += infInicialf + nomecampo + varPreencf;
                            }
                        }
                    }
                }
            }
        });

        if (msgf != "") {
            $dispararAlerta(msgf, 'warning');
            return false;
        } else {
            var post_url = $(this).closest('form').data("action");
            var request_method = $(this).closest('form').attr("method");

            var formdata = new FormData();
            var banner = $('#banner')[0].files[0];
            var banner_sm = $('#banner_sm')[0].files[0];
            var banner_md = $('#banner_md')[0].files[0];
            var submit = $('#submit');
            var id = $('#id').val();
            var descricao = $('#descricao').val();
            var link = $('#link').val();
            var local_link = $('#local_link').val();
            var posicao_textos = $('#posicao_textos').val();
            var texto_principal = $('#texto_principal').val();
            var efeito_texto_principal = $('#efeito_texto_principal').val();
            var cor_texto_principal = $('#pritxtcor').val();
            var segundo_texto = $('#segundo_texto').val();
            var efeito_segundo_texto = $('#efeito_segundo_texto').val();
            var cor_segundo_texto = $('#sectxtcor').val();
            var terceiro_texto = $('#terceiro_texto').val();
            var efeito_terceiro_texto = $('#efeito_terceiro_texto').val();
            var cor_terceiro_texto = $('#tertxtcor').val();
            var status = $('#status').val();
            var bttext = $('#bt_text').val();
            var btcor = $('#bt_cor').val();

            if (typeof (banner) !== "undefined") {
                formdata.append('banner', banner);
                formdata.append('ban1op', 1);
            } else {
                formdata.append('ban1op', 0);
            }

            if (typeof (banner_sm) !== "undefined") {
                formdata.append('banner_sm', banner_sm);
                formdata.append('ban2op', 1);
            } else {
                formdata.append('ban2op', 0);
            }

            if (typeof (banner_md) !== "undefined") {
                formdata.append('banner_md', banner_md);
                formdata.append('ban3op', 1);
            } else {
                formdata.append('ban3op', 0);
            }

            formdata.append('descricao', descricao);
            formdata.append('link', link);
            formdata.append('local_link', local_link);
            formdata.append('posicao_textos', posicao_textos);
            formdata.append('texto_principal', texto_principal);
            formdata.append('efeito_texto_principal', efeito_texto_principal);
            formdata.append('cor_texto_principal', cor_texto_principal);
            formdata.append('segundo_texto', segundo_texto);
            formdata.append('efeito_segundo_texto', efeito_segundo_texto);
            formdata.append('cor_segundo_texto', cor_segundo_texto);
            formdata.append('terceiro_texto', terceiro_texto);
            formdata.append('efeito_terceiro_texto', efeito_terceiro_texto);
            formdata.append('cor_terceiro_texto', cor_terceiro_texto);
            formdata.append('status', status);
            formdata.append('bttext', bttext);
            formdata.append('btcor', btcor);
            formdata.append('submit', submit);
            formdata.append('id', id);

            $.ajax({
                url: post_url,
                type: request_method,
                data: formdata,
                contentType: false,
                processData: false,
                success: function (response) {
                    var dataret = JSON.parse(response);

                    if (dataret.RET_COD == 0) {
                        retmsg = dataret.RET_MSG;
                        acao = 'success';

                        $("#img_1").attr("src", dataret.banner_1);
                        $("#img_2").attr("src", dataret.banner_2);
                        $("#img_3").attr("src", dataret.banner_3);
                        $(".preview img").show();

                    } else {
                        retmsg = dataret.RET_MSG;
                        acao = 'danger';

                        $("#img_1").attr("src", dataret.banner_1);
                        $("#img_2").attr("src", dataret.banner_2);
                        $("#img_3").attr("src", dataret.banner_3);
                        $(".preview img").hide();
                    }

                    $('#banner').val('');
                    $('#banner_sm').val('');
                    $('#banner_md').val('');

                    $('#submit').unbind();

                    $dispararAlerta(retmsg, acao);
                },
            }).done(function (response) {
                console.log(response);
            });
        }
    });

    $(document).on('click', '.deletar_banner', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', '.change_status', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        $.ajax({
            url: action,
            type: "POST",
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, 'success');
                    window.location.reload();
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $(document).on('change', '.file-foto', function (e) {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var prevw = $(this).data('preview');

            reader.onload = function (e) {
                $(prevw).attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    });
});
