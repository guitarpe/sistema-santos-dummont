jQuery(document).ready(function () {

    var GraficoBarraClientes = function () {

        var graficobarra;

        var graficoBarra = function () {

            var divcanvas = null;
            var titlegrafico = null;

            jQuery.ajax({
                url: caminho + "/Estatisticas/listAcessosLoginClientes",
                method: "POST",
                dataType: 'json',
                success: function (data) {

                    divcanvas = 'chart_cli_div';
                    titlegrafico = "Últimos 7 dias";

                    var dias = new Array();
                    var quantidade = new Array();
                    var series = new Array();

                    for (var h in data) {
                        dias.push(data[h].DIA_SEMANA + '<br/>' + data[h].DT_CAD);
                    }

                    for (var e in data) {
                        quantidade.push(Number(data[e].QTDE));
                    }

                    series.push({ "name": 'Quantidade de Acessos (Clientes)', "data": quantidade, "color": '#405f9f' });

                    var options = {
                        chart: {
                            renderTo: divcanvas,
                            type: 'column'
                        },
                        title: {
                            text: titlegrafico,
                            style: {
                                color: '#000',
                                fontWeight: 'bold'
                            }
                        },
                        xAxis: {
                            crosshair: true,
                            labels: {
                                style: {
                                    color: '#000',
                                    fontWeight: 'normal',
                                    fontSize: '9px'
                                }
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: ''
                            }
                        },
                        series: series
                    };

                    graficobarra = new Highcharts.Chart(options);

                    graficobarra.xAxis[0].setCategories(dias);
                    //graficobarra.xAxis[0].setStyle({color: '#008000', fontWeight: 'bold', fontSize: '10px'});
                },
                error: function (data) {
                    console.log(data);
                }
            });
        };

        return {
            init: function () {
                graficoBarra();
            },

            graficoBarra: function () {
                graficoBarra();
            }
        }
    }();

    var GraficoBarraUsuarios = function () {

        var graficobarraUsu;

        var graficoBarraUsu = function () {

            var divcanvasusu = null;
            var titlegraficousu = null;

            jQuery.ajax({
                url: caminho + "/Estatisticas/listAcessosLoginUsuariosSistema",
                method: "POST",
                dataType: 'json',
                success: function (data) {

                    divcanvasusu = 'chart_usu_div';
                    titlegraficousu = "Últimos 7 dias";

                    var diasusu = new Array();
                    var quantidadeusu = new Array();
                    var seriesusu = new Array();

                    for (var h in data) {
                        diasusu.push(data[h].DIA_SEMANA + '<br/>' + data[h].DT_CAD);
                    }

                    for (var e in data) {
                        quantidadeusu.push(Number(data[e].QTDE));
                    }

                    seriesusu.push({ "name": 'Quantidade de Acessos (Usuários de Sistema)', "data": quantidadeusu, "color": '#405f9f' });

                    var optionsusu = {
                        chart: {
                            renderTo: divcanvasusu,
                            type: 'column'
                        },
                        title: {
                            text: titlegraficousu,
                            style: {
                                color: '#000',
                                fontWeight: 'bold'
                            }
                        },
                        xAxis: {
                            crosshair: true,
                            labels: {
                                style: {
                                    color: '#000',
                                    fontWeight: 'normal',
                                    fontSize: '9px'
                                }
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: ''
                            }
                        },
                        series: seriesusu
                    };

                    graficobarraUsu = new Highcharts.Chart(optionsusu);
                    graficobarraUsu.xAxis[0].setCategories(diasusu);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        };

        return {
            init: function () {
                graficoBarraUsu();
            },

            graficoBarraUsu: function () {
                graficoBarraUsu();
            }
        }
    }();

    var GraficoBuscas = function () {

        var transacoes;

        var GraficoPalavrasBusca = function () {

            jQuery.ajax({
                url: caminho + "/Estatisticas/listUltimasBuscas",
                method: "POST",
                dataType: 'json',
                success: function (data) {

                    var categories = new Array();

                    for (var t in data) {
                        categories.push({ "name": data[t].PALAVRA, "y": Number(data[t].QTDE) });
                    }

                    transacoes = Highcharts.chart('chart_ult_busca', {
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45,
                                beta: 0
                            }
                        },
                        title: {
                            text: 'Últimos 7 dias',
                            style: {
                                color: '#000',
                                fontWeight: 'bold'
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            enabled: true,
                            y: 20,
                            align: 'right',
                            verticalAlign: 'top',
                            margin: 0,
                            width: 130,
                            borderWidth: 0,
                            itemMarginTop: 2,
                            itemMarginBottom: 2,
                            itemStyle: {
                                color: '#000',
                                fontWeight: 'normal',
                                fontSize: '10px;'
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                depth: 35,
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.percentage:.1f} %',
                                    distance: -50,
                                    filter: {
                                        property: 'percentage',
                                        operator: '>',
                                        value: 4
                                    }
                                },
                                showInLegend: true
                            }
                        },
                        series: [{
                            type: 'pie',
                            name: 'QTDE:',
                            data: categories,
                            size: '100%',
                            dataLabels: {
                                formatter: function () {
                                    return this.y;
                                },
                                color: '#ffffff',
                                distance: -30
                            }
                        }],
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 450
                                },
                                chartOptions: {
                                    series: [{
                                        id: 'versions',
                                        dataLabels: {
                                            enabled: false
                                        }
                                    }]
                                }
                            }]
                        }
                    });
                }
            });
        };

        return {
            init: function () {
                GraficoPalavrasBusca();
            }
        }
    }();

    var GraficoAcessos = function () {

        var acessos;

        var GraficoAcessosCidades = function () {

            jQuery.ajax({
                url: caminho + "/Estatisticas/listaAcessosCidades",
                method: "POST",
                dataType: 'json',
                success: function (data) {

                    var categorias = new Array();

                    for (var t in data) {
                        categorias.push({ "name": data[t].END_CIDADE + '-' + data[t].END_UF, "y": Number(data[t].QTDE) });
                    }

                    acessos = Highcharts.chart('chart_ace_cidades', {
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45,
                                beta: 0
                            }
                        },
                        title: {
                            text: 'Últimos 7 dias',
                            style: {
                                color: '#000',
                                fontWeight: 'bold'
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            enabled: true,
                            y: 20,
                            align: 'right',
                            verticalAlign: 'top',
                            margin: 0,
                            width: 130,
                            borderWidth: 0,
                            itemMarginTop: 2,
                            itemMarginBottom: 2,
                            itemStyle: {
                                color: '#000',
                                fontWeight: 'normal',
                                fontSize: '10px;'
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                depth: 35,
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.percentage:.1f} %',
                                    distance: -50,
                                    filter: {
                                        property: 'percentage',
                                        operator: '>',
                                        value: 4
                                    }
                                },
                                showInLegend: true
                            }
                        },
                        series: [{
                            type: 'pie',
                            name: 'QTDE:',
                            data: categorias,
                            size: '100%',
                            dataLabels: {
                                formatter: function () {
                                    return this.y;
                                },
                                color: '#ffffff',
                                distance: -30
                            }
                        }],
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 450
                                },
                                chartOptions: {
                                    series: [{
                                        id: 'versions',
                                        dataLabels: {
                                            enabled: false
                                        }
                                    }]
                                }
                            }]
                        }
                    });
                }
            });
        };

        return {
            init: function () {
                GraficoAcessosCidades();
            }
        }
    }();

    GraficoBarraClientes.init();
    GraficoBarraUsuarios.init();
    GraficoBuscas.init();
    GraficoAcessos.init();

});
