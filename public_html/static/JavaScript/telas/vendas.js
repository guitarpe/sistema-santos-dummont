jQuery(document).ready(function ($) {

    var table = $("#tb-pedidos").DataTable({
        "order": [
            [0, "desc"]
        ],
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        }
    });

    $(document).on('click', '#btn_processar', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var pedidos = [];

        table.rows('.selected').every(function (index, element) {
            pedidos.push(this.cell(this, 1).data());
        });

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "400px",
            height: "auto",
            ajax: {
                type: "POST",
                data: {
                    pedido: pedidos.toString()
                },
                complete: function (jqXHR, textStatus) {

                }
            }
        });
    });

    $(document).on('click', '.btn_process', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var pedido = $(this).data('id');

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "400px",
            height: "auto",
            ajax: {
                type: "POST",
                data: {
                    pedido: pedido
                },
                complete: function (jqXHR, textStatus) {

                }
            }
        });
    });

    $(document).on('click', '.btn_pross_sep_env', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var pedido = $(this).data('id');

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "900px",
            height: "auto",
            ajax: {
                type: "POST",
                data: {
                    pedido: pedido
                },
                complete: function (jqXHR, textStatus) {

                }
            }
        });
    });

    $(document).on('click', '.btn_pross', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var pedido = $(this).data('id');

        $.ajax({
            url: action,
            type: "POST",
            data: {
                pedido: pedido
            },
            success: function (data) {
                window.location.href = data;
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $(document).on('click', '.send_msg', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var pedido = $(this).data('id');

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "900px",
            height: "auto",
            ajax: {
                type: "POST",
                data: {
                    pedido: pedido
                },
                complete: function (jqXHR, textStatus) {

                }
            }
        });
    });

    $(document).on('click', '#btn_enviar', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        var pedidos = [];

        table.rows('.selected').every(function (index, element) {
            pedidos.push(this.cell(this, 1).data());
        });

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "900px",
            height: "auto",
            ajax: {
                type: "POST",
                data: {
                    pedido: pedidos.toString()
                },
                complete: function (jqXHR, textStatus) {

                }
            }
        });
    });

    $(document).on('click', '#btn_imprimir', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        var pedidos = [];

        table.rows('.selected').every(function (index, element) {
            pedidos.push(this.cell(this, 1).data());
        });

        bootbox.confirm({
            message: "<h3>Deseja realmente imprimir os pedidos selecionados?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if (result == true) {
                    window.open(action + '/' + pedidos.toString());
                }
            }
        });

    });

    $(document).on('click', '#btn_cancelar', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        var pedidos = [];

        table.rows('.selected').every(function (index, element) {
            pedidos.push(this.cell(this, 1).data());
        });

        bootbox.confirm({
            message: "<h3>Deseja realmente cancelar os pedidos selecionados?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if (result == true) {
                    $.ajax({
                        url: action + '/' + pedidos.toString(),
                        type: "GET",
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });

    });

    $(document).on('click', '.cancelar_pedido', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        bootbox.confirm({
            message: "<h3>Deseja realmente cancelar o pedido?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "GET",
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

});
