jQuery(document).ready(function ($) {

    $('.alert').delay(6000).fadeOut();

    $('.numbers').keyup(function () {
        this.value = this.value.replace(/[^0-9\/]/g, '');
    });

    var behavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00000';
    },
        options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(behavior.apply({}, arguments), options);
            }
        };

    $('.maskFone').mask(behavior, options);

    var infInicial = "O campo "
    var varPreenc = " deve ser preenchido!<br>";
    var msg = '';

    $('.form_submit').click(function () {

        var form = $(this).closest('form').attr("id");

        msg = "";

        $("#" + form + " input, select").each(function () {

            if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {
                var input = $(this).attr('name');

                if ($(this).val() === "" && input != 'foto') {
                    var nomecampo = $(this).attr('data-name');
                    msg += infInicial + nomecampo + varPreenc;
                }

                if (form == "form_login_esqueci") {

                }
            }
        });

        if (msg != "") {
            $dispararAlerta(msg, 'warning');
        } else {
            $('form[name=' + form + ']').submit();
        }

    });

    function dispararAlerta(msg, tipo) {

        $.UIkit.notify({
            message: msg,
            status: tipo,
            timeout: 15000,
            pos: 'top-right'
        });

        return false;
    }

    $('.form_submit').keydown(function (event) {
        if (event.keyCode == 13) {
            this.form.submit();
            return false;
        }
    });

});
