jQuery(document).ready(function ($) {

    var buttonCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    return data;
                }
            }
        }
    };

    var table = $('#tb-produtos').DataTable({
        "dom": '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"B>><"row"<"col-sm-12"tr>><"row"<"col-sm-6 nopad_r"i><"col-sm-6 nopad_l"p>>',
        //"dom": 'Bfrtip',
        "destroy": true,
        "processing": true,
        "serverSide": true,
        "order": [
            [0, "desc"]
        ],
        "language": {
            url: 'public_html/static/JavaScript/dataTable/lang/PT_BR.json'
        },
        buttons: [
            $.extend(true, {}, buttonCommon, {
                extend: 'copyHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'excelHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'pdfHtml5'
            })
        ],
        "ajax": {
            "url": "Integracao/ListaJsonProdutos",
            "type": "POST"
        },
        columns: [
            { searchable: false },
            null,
            null,
            null,
            { searchable: false, orderable: false },
            { searchable: true, orderable: false }
        ],
        "columnDefs": [
            {
                "targets": [2],
                "orderable": true,
                "render": function (data, type, row) {
                    if (row[2] == 1) {
                        return 'Ativo';
                    } else {
                        return 'Inativo';
                    }
                }
            },
            {
                "targets": [4],
                "class": "acoes",
                "orderable": false,
                "render": function (data, type, row) {
                    return '<a href="Produtos/SincronizarProdutos/' + row[7] + '/1" class="btn btn-xs btn-info prd_sync" data-toggle="tooltip" data-placement="top" title="Sincronizar - CETRUS"><i class="entypo-arrows-ccw"></i></a>';
                }
            }
        ],
        'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).attr("id", aData[0]);
            return nRow;
        },
        "stripeClasses": ['', '']
    });

    $('#btnbuscar').click(function () {

        table.column(1).search($('#busca_titulo').val().trim());
        table.column(2).search($('#busca_status').val());
        //table.column(5).search($('#em_estoque').val());
        //table.column(6).search($('#busca_categoria').val().trim());
        //table.column(7).search($('#busca_sku').val().trim());

        table.draw();
    });

    $(".dataTables_wrapper select").select2({
        minimunResultsForSearch: -1
    });

    tinymce.init({
        selector: '#descricao',
        height: 250,
        menubar: false,
        plugins: [
            'advlist autolink lists link charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime table contextmenu paste code wordcount'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
    });

    tinymce.init({
        selector: '#ficha_tecnica',
        height: 250,
        menubar: false,
        plugins: [
            'advlist autolink lists link charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime table contextmenu paste code wordcount'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
    });

    $(document).on('click', '.prd_sync', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        $.ajax({
            url: action,
            type: "GET",
            success: function (ret) {
                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, data.SITUACAO);
                    table.ajax.reload(null, false);
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, data.SITUACAO);
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $(document).on('click', '.prd_change_status', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        $.ajax({
            url: action,
            type: "POST",
            success: function (ret) {
                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, 'success');
                    table.ajax.reload(null, false);
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $(document).on('click', '.deletar_imagem', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');
        var prd = $(this).attr('data-prd');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover a imagem?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id,
                            prd: prd
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $verificarCampos = function (area) {
        var infoini = "O campo "
        var infofim = " deve ser preenchido!<br>";
        var mensagem = '';

        $("div#" + area + " input, div#" + area + " select, div#" + area + " textarea").each(function () {
            if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {

                var input = $(this).attr('name');

                var verificar = $(this).data('verif');
                var nomecampo = $(this).data('name');

                if (verificar != false) {
                    if (input == 'descricao') {
                        if (tinyMCE.get("descricao").getContent() == "") {
                            mensagem += infoini + nomecampo + infofim;
                        }
                    } else if (input == 'ficha_tecnica') {
                        if (tinyMCE.get("ficha_tecnica").getContent() == "") {
                            mensagem += infoini + nomecampo + infofim;
                        }
                    } else {
                        //if(input == )
                        if ($(this).val() === "" && input != 'foto') {
                            mensagem += infoini + nomecampo + infofim;
                        }
                    }
                }
            }
        });

        if (mensagem != "") {
            $dispararAlerta(mensagem, 'warning');
            return false;
        } else {
            return true;
        }
    };

    $('.navtabs').on('click', function (e) {
        e.preventDefault();

        var area = $(this).data('area');
        var href = $(this).attr("href");

        if (area != null) {
            if ($verificarCampos(area)) {
                $('.nav-tabs a[href="' + href + '"]').tab('show');
            }
        } else {
            $('.nav-tabs a[href="' + href + '"]').tab('show');
        }
    });

    $("#selecionar").on('click', function (e) {
        e.preventDefault();

        $('#multi-selecao option:selected').each(function () {
            $("#categorias").append('<option class="opt" value="' + $(this).val() + '" selected="selected">' + $(this).text() + '</option>');
        });
    });

    $("#limpartudo").on('click', function (e) {
        e.preventDefault();

        $('#categorias').html('');
    });

    $("#limparselects").on('click', function (e) {
        e.preventDefault();

        $('#categorias option:selected').each(function () {
            $(this).remove();
        });
    });

    $("#bt_buscar_cetrus").on('click', function (e) {
        e.preventDefault();

        var sku = $('#sku').val();

        $.ajax({
            url: 'BuscaBaseCetrus',
            type: "POST",
            data: {
                sku: sku
            },
            success: function (ret) {
                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, 'success');

                    $('#nome').val(data.DADOS.PRD_NOME);
                    $('#sku').val(data.DADOS.PRD_SKU);
                    $('#preco').val(data.DADOS.PRD_PRECO);
                    $('#marca').val(data.DADOS.PRD_MARCA);
                    $('#qtde_estoque').val(data.DADOS.PRD_QTDE_ESTOQUE);
                    $('#peso').val(data.DADOS.PRD_PESO);
                    $('#altura').val(data.DADOS.PRD_ALTURA);
                    $('#largura').val(data.DADOS.PRD_LARGURA);
                    $('#comprimento').val(data.DADOS.PRD_COMPRIMENTO);
                    $('#profundidade').val(data.DADOS.PRD_PROFUNFIDADE);

                } else {
                    $dispararAlerta(data.O_DESC_CURTO, 'warning');

                    $('#nome').val('');
                    $('#sku').val('');
                    $('#preco').val('');
                    $('#marca').val('');
                    $('#qtde_estoque').val('');
                    $('#peso').val('');
                    $('#altura').val('');
                    $('#largura').val('');
                    $('#comprimento').val('');
                    $('#profundidade').val('');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });

    });

    if ($("div.dropzone").length) {

        Dropzone.autoDiscover = false;

        var uploader = new Dropzone(".dropzone", {
            url: caminho + "/Produtos/UploadImagem",
            paramName: 'fotos',
            dictDefaultMessage: "Arraste as imagens aqui",
            dictResponseError: "Erro no upload dos arquivos",
            acceptedFiles: "image/jpg, image/jpeg, image/png",
            uploadMultiple: true,
            autoProcessQueue: false,
            addRemoveLinks: true,
            dictRemoveFile: 'Remover',
            maxFilesize: 2,
            parallelUploads: 4,
            init: function () {
                this.on("sending", function (file, xhr, data) {
                    var id = $("#id").val();

                    if (id != null) {
                        data.append("id", id);
                    }
                });
            }
        });

        uploader.on("successmultiple", function (file, response) {

            console.log(response);
            var data = JSON.parse(response);

            if (data.erros != '') {
                var err = data.erros.split(',');
                var imgerr = data.imgerr.split(',');
                var msg = '';

                $.each(err, function (index, value) {
                    msg += value + '<br/>';
                });

                $.each(imgerr, function (index, value) {
                    if (uploader.files.length) {
                        for (var i = 0; i < uploader.files.length; i++) {
                            if (uploader.files[i].name == value) {
                                uploader.removeFile(uploader.files[i]);
                            }
                        }
                    }
                });

                $.UIkit.notify({
                    message: msg,
                    status: 'danger',
                    timeout: 15000,
                    pos: 'top-right'
                });
            }

            var imagens = $("#imagens");

            if (imagens.val() != '') {
                imagens.val(imagens.val() + "," + data.sucesso);
            } else {
                imagens.val(data.sucesso);
            }

            $("#pasta_imagens").val(data.pasta);
            $("#imagens").val(imagens.val() + "," + data.sucesso);

            $(".dz-remove").hide();
        });

        uploader.on("error", function (file) {
            uploader.removeFile(file);
        });

        $('#btnupload').click(function () {
            uploader.processQueue();
        });
    }
});
