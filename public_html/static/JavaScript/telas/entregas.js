jQuery(document).ready(function ($) {
    $.getJSON(caminho + '/public_html/static/data/estados-cidades.json', function (data) {
        var items = [];
        var options = '';

        options = '<option value=""></option>';

        $.each(data.estados, function (key, val) {
            options += '<option value="' + val.sigla + '">' + val.sigla + '</option>';
        });
        $("#uf_abr").html(options);

        $("#uf_abr").change(function () {

            var options_cidades = '';
            var str = "";

            $("#uf_abr option:selected").each(function () {
                str += $(this).text();
            });

            $.each(data.estados, function (key, val) {
                if (val.sigla == str) {
                    $.each(val.cidades, function (key_city, val_city) {
                        options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                    });
                }
            });
            $("#data-cidades").html(options_cidades);

        }).change();
    });

    $('#btn_inserir_cidade').on('click', function () {
        var estado = $(this).closest('.area_abrang').find('#uf_abr').val();
        var cidade = $(this).closest('.area_abrang').find('#cidade_abr').val();

        if (estado != '' && cidade != '') {
            var linha = '<tr><td><input type="hidden" name="cidade[]" value="' + cidade + '" /><input type="hidden" name="estado[]" value="' + estado + '" />' + cidade + '</td><td>' + estado + '</td><td><a href="#" class="btn btn-xs btn-danger rem_cidade"><i class="entypo-trash"></i></a></td></tr>';
            $("#tb-cidades tbody").append(linha);

            $(this).closest('.area_abrang').find('#uf_abr').val('');
            $(this).closest('.area_abrang').find('#cidade_abr').val('');
        }
    });

    $('#restricao').on('change', function () {
        if ($(this).val() == 1) {
            $('.area_abrang').removeClass('disabled');
            $('.area_abrang :input').val('');
            $('.area_abrang :input').removeAttr('disabled');
            $('.area_abrang :button').removeAttr('disabled');
        } else {
            $('.area_abrang').addClass('disabled');
            $('.area_abrang :input').val('');
            $('.area_abrang :input').attr('disabled', true);
            $('.area_abrang :button').attr('disabled', true);
        }
    });

    $(document).on('click', '.rem_cidade', function (e) {
        e.preventDefault();
        $(this).closest('tr').remove();
    });
});
