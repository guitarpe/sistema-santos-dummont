jQuery(document).ready(function ($) {

    var buttonCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    return data;
                }
            }
        }
    };

    $('#tb-usuarios').DataTable({
        "dom": '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"B>><"row"<"col-sm-12"tr>><"row"<"col-sm-6 nopad_r"i><"col-sm-6 nopad_l"p>>',
        //"dom": 'Bfrtip',
        "destroy": true,
        "processing": true,
        "serverSide": false,
        "language": {
            url: 'public_html/static/JavaScript/dataTable/lang/PT_BR.json'
        },
        buttons: [
            $.extend(true, {}, buttonCommon, {
                extend: 'copyHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'excelHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'pdfHtml5'
            })
        ],
        'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).attr("id", aData[0]);
            return nRow;
        },
        "stripeClasses": ['', '']
    });

    $(".dataTables_wrapper select").select2({
        minimunResultsForSearch: -1
    });

    $(document).on('click', '.deletar_usuario', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            //window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', '.change_status', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        $.ajax({
            url: action,
            type: "POST",
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, 'success');
                    window.location.reload();
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });
});
