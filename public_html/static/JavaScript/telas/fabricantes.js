jQuery(document).ready(function ($) {

    $(document).on('click', '.deletar_parceiro', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', '.par_change_status', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        bootbox.confirm({
            message: "<h3>Ativando ou Inativando este Parceiro seus produtos ficarão disponíveis ou não no site, deseja realmente continuar?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        success: function (ret) {
                            var data = JSON.parse(ret);

                            if (data.O_COD_RETORNO == 0) {
                                $dispararAlerta(data.O_DESC_CURTO, 'success', 1);
                            } else {
                                $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, 'danger');
                            }
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });
});
