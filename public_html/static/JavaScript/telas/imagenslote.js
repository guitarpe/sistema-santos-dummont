jQuery(document).ready(function ($) {

    if ($("div.dropzone").length) {

        Dropzone.autoDiscover = false;

        var uploader = new Dropzone(".dropzone", {
            url: caminho + "/ImagensLote/UploadImagems",
            paramName: 'fotos',
            dictDefaultMessage: "Arraste as imagens aqui",
            dictResponseError: "Erro no upload dos arquivos",
            acceptedFiles: "image/jpg, image/jpeg, image/png",
            uploadMultiple: true,
            autoProcessQueue: false,
            addRemoveLinks: true,
            dictRemoveFile: 'Remover',
            maxFilesize: 4,
            parallelUploads: 20
        });

        uploader.on("successmultiple", function (file, response) {

            var data = JSON.parse(response);
            console.log(data);

            if (data.erros != '') {
                var err = data.erros.split(',');
                var imgerr = data.imgerr.split(',');
                var msg = '';

                $.each(err, function (index, value) {
                    msg += value + '<br/>';
                });

                $.each(imgerr, function (index, value) {
                    if (uploader.files.length) {
                        for (var i = 0; i < uploader.files.length; i++) {
                            if (uploader.files[i].name == value) {
                                uploader.removeFile(uploader.files[i]);
                            }
                        }
                    }
                });

                $.UIkit.notify({
                    message: msg,
                    status: 'danger',
                    timeout: 15000,
                    pos: 'top-right'
                });
            } else {
                var sucesso = data.sucesso.split(',');
                var msg = 'Imagens Carregadas com sucesso<br/>';

                $.each(sucesso, function (index, value) {
                    msg += value + '<br/>';
                });

                $.UIkit.notify({
                    message: msg,
                    status: 'success',
                    timeout: 15000,
                    pos: 'top-right'
                });
            }

            $(".dz-remove").hide();
        });

        uploader.on("error", function (file) {
            uploader.removeFile(file);
        });

        $('#btnupload').click(function () {
            uploader.processQueue();
        });
    }
});
