jQuery(document).ready(function ($) {

    $(document).on('click', '.deletar_especial', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $('#tipo').on('change', function () {
        $('.multselect').html('');
        $("#pesquisar").val('')
    });

    $('#btnpesquisar').on('click', function (e) {
        e.preventDefault();

        var tipo = $("#tipo").val();
        var pesquisar = $("#pesquisar").val();
        var action = $("#pesquisar").attr("data-url");

        if (tipo != null && tipo != '') {

            $("#multi-selecao").html("");

            $.ajax({
                url: action,
                type: "POST",
                data: {
                    tipo: tipo,
                    pesquisar: pesquisar
                },
                success: function (data) {

                    data = JSON.parse(data);

                    $.each(data, function (key, value) {
                        $("#multi-selecao").append('<option value="' + value['ID'] + '">' + value['ID'] + ' - ' + value['NOME'] + '</option>');
                    });
                },
                error: function (jqXHR, textStatus, errorMessage) {
                    console.log(errorMessage);
                }
            });
        } else {
            bootbox.alert("<h4>É necessário escolher o tipo!</h4>");
            $(this).val("");
        }
    });

    $("#selecionar").on('click', function (e) {
        e.preventDefault();

        $('#multi-selecao option:selected').each(function () {
            $("#selecionados").append('<option class="opt" value="' + $(this).val() + '" selected="selected">' + $(this).text() + '</option>');
        });

        $('#selecionados option').prop('selected', true);
    });

    $("#limpartudo").on('click', function (e) {
        e.preventDefault();

        $('#selecionados').html('');
    });

    $("#limparselects").on('click', function (e) {
        e.preventDefault();

        $('#selecionados option:selected').each(function () {
            $(this).remove();
        });

        $('#selecionados option').prop('selected', true);
    });
});
