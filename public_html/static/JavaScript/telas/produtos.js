jQuery(document).ready(function ($) {

    var buttonCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    return data;
                }
            }
        }
    };

    var table = $('#tb-produtos').DataTable({
        "dom": '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"B>><"row"<"col-sm-12"tr>><"row"<"col-sm-6 nopad_r"i><"col-sm-6 nopad_l"p>>',
        //"dom": 'Bfrtip',
        "destroy": true,
        "processing": true,
        "serverSide": true,
        "order": [
            [0, "desc"]
        ],
        "language": {
            url: 'public_html/static/JavaScript/dataTable/lang/PT_BR.json'
        },
        buttons: [
            $.extend(true, {}, buttonCommon, {
                extend: 'copyHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'excelHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'pdfHtml5'
            })
        ],
        "ajax": {
            "url": "Produtos/ListaProdutos",
            "type": "POST"
        },
        columns: [
            { searchable: false },
            null,
            null,
            null,
            { searchable: true, orderable: false },
            { searchable: false, orderable: false },
            { searchable: true, orderable: false, visible: false },
            { searchable: true, orderable: false, visible: false }
        ],
        "columnDefs": [
            {
                "targets": [2],
                "orderable": true,
                "render": function (data, type, row) {
                    if (row[2] == 1) {
                        return 'Ativo';
                    } else {
                        return 'Inativo';
                    }
                }
            },
            {
                "targets": [5],
                "class": "acoes",
                "orderable": false,
                "render": function (data, type, row) {
                    return '<a href="Produtos/Produto/' + row[0] + '" class="btn btn-xs btn-orange" data-toggle="tooltip" data-placement="top" title="Editar"><i class="entypo-pencil"></i></a> <a href="Produtos/Excluir" class="btn btn-xs btn-red deletar" data-id="' + row[0] + '" data-toggle="tooltip" data-placement="top" title="Excluir"><i class="entypo-trash"></i></a> <a href="Produtos/AtivaInativa/' + row[0] + '/2" class="btn btn-xs btn-black prd_change_status" data-toggle="tooltip" data-placement="top" title="Inativar"><i class="entypo-lock"></i></a> <a href="Produtos/AtivaInativa/' + row[0] + '/1" class="btn btn-xs btn-green prd_change_status" data-toggle="tooltip" data-placement="top" title="Ativar"><i class="entypo-lock-open"></i></a> <a href="Produtos/SincronizarProdutos/' + row[7] + '/1" class="btn btn-xs btn-info prd_sync" data-toggle="tooltip" data-placement="top" title="Sincronizar - CETRUS"><i class="entypo-arrows-ccw"></i></a>';
                }
            }
        ],
        'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).attr("id", aData[0]);
            return nRow;
        },
        "stripeClasses": ['', '']
    });

    $('#btnbuscar').click(function () {

        table.column(1).search($('#busca_titulo').val().trim());
        table.column(2).search($('#busca_status').val());
        table.column(4).search($('#busca_sku').val().trim());
        table.column(5).search($('#em_estoque').val());
        table.column(6).search($('#busca_categoria').val().trim());

        table.draw();
    });

    $(".dataTables_wrapper select").select2({
        minimunResultsForSearch: -1
    });

    tinymce.init({
        selector: '#descricao',
        height: 250,
        menubar: false,
        plugins: [
            'advlist autolink lists link charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime table contextmenu paste code wordcount'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
    });

    tinymce.init({
        selector: '#ficha_tecnica',
        height: 250,
        menubar: false,
        plugins: [
            'advlist autolink lists link charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime table contextmenu paste code wordcount'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
    });

    $(document).on('click', '.prd_sync', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        $.ajax({
            url: action,
            type: "GET",
            success: function (ret) {
                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, data.SITUACAO);
                    table.ajax.reload(null, false);
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, data.SITUACAO);
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $(document).on('click', '.prd_change_status', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        $.ajax({
            url: action,
            type: "POST",
            success: function (ret) {
                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, 'success');
                    table.ajax.reload(null, false);
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $(document).on('click', '.deletar_imagem', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var img = $(this).attr('data-img');
        var prd = $(this).attr('data-prd');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover a imagem?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            img: img,
                            prd: prd
                        },
                        success: function (data) {
                            console.log(data);
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $verificarCampos = function (area) {
        var infoini = "O campo "
        var infofim = " deve ser preenchido!<br>";
        var mensagem = '';

        $("div#" + area + " input, div#" + area + " select, div#" + area + " textarea").each(function () {
            if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {

                var input = $(this).attr('name');

                var verificar = $(this).data('verif');
                var nomecampo = $(this).data('name');

                if (verificar != false) {
                    if (input == 'descricao') {
                        if (tinyMCE.get("descricao").getContent() == "") {
                            mensagem += infoini + nomecampo + infofim;
                        }
                    } else if (input == 'ficha_tecnica') {
                        if (tinyMCE.get("ficha_tecnica").getContent() == "") {
                            mensagem += infoini + nomecampo + infofim;
                        }
                    } else {
                        //if(input == )
                        if ($(this).val() === "" && input != 'foto') {
                            mensagem += infoini + nomecampo + infofim;
                        }
                    }
                }
            }
        });

        if (mensagem != "") {
            $dispararAlerta(mensagem, 'warning');
            return false;
        } else {
            return true;
        }
    };

    $('.navtabs').on('click', function (e) {
        e.preventDefault();

        var area = $(this).data('area');
        var href = $(this).attr("href");

        if (area != null) {
            if ($verificarCampos(area)) {
                $('.nav-tabs a[href="' + href + '"]').tab('show');
            }
        } else {
            $('.nav-tabs a[href="' + href + '"]').tab('show');
        }
    });

    $("#selecionar").on('click', function (e) {
        e.preventDefault();

        $('#multi-selecao option:selected').each(function () {
            $("#categorias").append('<option class="opt" value="' + $(this).val() + '" selected="selected">' + $(this).text() + '</option>');
        });
    });

    $("#limpartudo").on('click', function (e) {
        e.preventDefault();

        $('#categorias').html('');
    });

    $("#limparselects").on('click', function (e) {
        e.preventDefault();

        $('#categorias option:selected').each(function () {
            $(this).remove();
        });
    });

    $("#bt_buscar_cetrus").on('click', function (e) {
        e.preventDefault();

        var sku = $('#sku').val();

        $.ajax({
            url: 'BuscaBaseCetrus',
            type: "POST",
            data: {
                sku: sku
            },
            success: function (ret) {
                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, 'success');

                    $('#nome').val(data.DADOS.PRD_NOME);
                    $('#sku').val(data.DADOS.PRD_SKU);
                    $('#unidade').val(data.DADOS.PRD_UNIDADE);
                    $('#preco').val(data.DADOS.PRD_PRECO);
                    $('#marca').val(data.DADOS.PRD_MARCA);
                    $('#qtde_estoque').val(data.DADOS.PRD_QTDE_ESTOQUE);
                    $('#peso').val(data.DADOS.PRD_PESO);
                    $('#altura').val(data.DADOS.PRD_ALTURA);
                    $('#largura').val(data.DADOS.PRD_LARGURA);
                    $('#status').val(data.PRD_STATUS);
                    $('#comprimento').val(data.DADOS.PRD_COMPRIMENTO);
                    $('#profundidade').val(data.DADOS.PRD_PROFUNFIDADE);

                } else {
                    $dispararAlerta(data.O_DESC_CURTO, 'warning');

                    $('#nome').val('');
                    $('#sku').val('');
                    $('#unidade').val('');
                    $('#preco').val('');
                    $('#marca').val('');
                    $('#qtde_estoque').val('');
                    $('#peso').val('');
                    $('#altura').val('');
                    $('#largura').val('');
                    $('#status').val(2);
                    $('#comprimento').val('');
                    $('#profundidade').val('');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });

    });

    $(".set_imag_princ").on('change', function (e) {

        var action = $(this).data("action");
        var prd = $(this).data("prd");
        var img = $(this).val();

        $.ajax({
            url: action,
            type: "POST",
            data: {
                img: img,
                prd: prd
            },
            success: function (data) {
                window.location.href = data;
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    //cadastro de produtos
    if ($("div.dropcad").length) {

        var action = $("div.dropzone").closest('form').attr('action');

        Dropzone.autoDiscover = false;

        var uploader = new Dropzone(".dropzone", {
            url: action,
            paramName: 'fotos',
            dictDefaultMessage: "Arraste as imagens aqui",
            dictResponseError: "Erro no upload dos arquivos",
            acceptedFiles: "image/jpg, image/jpeg, image/png",
            uploadMultiple: true,
            autoProcessQueue: false,
            addRemoveLinks: true,
            dictRemoveFile: 'Remover',
            maxFilesize: 2,
            parallelUploads: 4,
            init: function () {
                uploader = this;

                document.getElementById("submit-all").addEventListener("click", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var form = $(this).closest('form').attr("id");

                    var infInicial = "O campo "
                    var varPreenc = " deve ser preenchido!<br>";
                    var msg = '';

                    $("form#" + form + " input, form#" + form + " select, form#" + form + " textarea").each(function () {
                        if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {

                            var input = $(this).attr('name');
                            var verificar = $(this).data('verif');
                            var nomecampo = $(this).data('name');

                            if (verificar != false) {
                                if ($(this).val() === "" && input != 'foto') {
                                    msg += infInicial + nomecampo + varPreenc;
                                }
                            }
                        }
                    });

                    if (msg != "") {
                        $dispararAlerta(msg, 'warning');
                        return false;
                    } else {
                        uploader.processQueue();
                    }
                });

                this.on("sendingmultiple", function (data, xhr, formData) {

                    formData.append("submit", $("#submit-all").val());
                    formData.append("nome", $("input[name='nome']").val());
                    formData.append("sku", $("input[name='sku']").val());
                    formData.append("unidade", $("input[name='unidade']").val());
                    formData.append("status", $("input[name='status']").val());
                    formData.append("categorias", $("input[name='categorias']").val());
                    formData.append("preco", $("input[name='preco']").val());
                    formData.append("preco_anterior", $("input[name='preco_anterior']").val());
                    formData.append("taxa", $("input[name='taxa']").val());
                    formData.append("destaque", $("input[name='destaque']").val());
                    formData.append("qtde_estoque", $("input[name='qtde_estoque']").val());
                    formData.append("qtde_minima", $("input[name='qtde_minima']").val());
                    formData.append("nome_tecnico", $("input[name='nome_tecnico']").val());
                    formData.append("modelo", $("input[name='modelo']").val());
                    formData.append("descricao", $("input[name='descricao']").val());
                    formData.append("ficha_tecnica", $("input[name='ficha_tecnica']").val());
                    formData.append("marca", $("input[name='marca']").val());
                    formData.append("peso", $("input[name='peso']").val());
                    formData.append("comprimento", $("input[name='comprimento']").val());
                    formData.append("profundidade", $("input[name='profundidade']").val());
                    formData.append("largura", $("input[name='largura']").val());
                    formData.append("altura", $("input[name='altura']").val());
                    formData.append("voltagem", $("input[name='voltagem']").val());
                    formData.append("cor", $("input[name='cor']").val());
                    formData.append("tags", $("input[name='tags']").val());
                    formData.append("parceiro", $("input[name='parceiro']").val());
                    formData.append("imagens", $("input[name='imagens']").val());
                    formData.append("pasta_imagens", $("input[name='pasta_imagens']").val());
                });
            }
        });

        uploader.on('thumbnail', function (file) {
            if (file.accepted !== false) {
                $('.form_submit_prd').prop('disabled', false);
            }
        });

        uploader.on("successmultiple", function (file, response) {

            var data = JSON.parse(response);

            if (data.erro == 1) {
                var imgerr = data.imgerr.split(',');
                var retorno = data.retorno;
                var msg = retorno + '<br/>';

                $.each(imgerr, function (index, value) {
                    msg += value + '<br/>';

                    if (uploader.files.length) {
                        for (var i = 0; i < uploader.files.length; i++) {
                            if (uploader.files[i].name == value) {
                                uploader.removeFile(uploader.files[i]);
                            }
                        }
                    }
                });

                $.UIkit.notify({
                    message: msg,
                    status: 'danger',
                    timeout: 5000,
                    pos: 'top-right',
                    onClose: function () {
                        window.location.href = data.page;
                    }
                });
            } else {
                $.UIkit.notify({
                    message: data.retorno,
                    status: 'success',
                    timeout: 5000,
                    pos: 'top-right',
                    onClose: function () {
                        window.location.href = data.page;
                    }
                });
            }

            $(".dz-remove").hide();
        });

        uploader.on("error", function (file) {
            uploader.removeFile(file);
        });
    }

    //edição de produtos
    if ($("div.dropedit").length) {

        var action = $("div.dropzone").closest('form').attr('action');

        Dropzone.autoDiscover = false;

        var uploader = new Dropzone(".dropzone", {
            url: action,
            paramName: 'fotos',
            dictDefaultMessage: "Arraste as imagens aqui",
            dictResponseError: "Erro no upload dos arquivos",
            acceptedFiles: "image/jpg, image/jpeg, image/png",
            uploadMultiple: true,
            autoProcessQueue: false,
            addRemoveLinks: true,
            dictRemoveFile: 'Remover',
            maxFilesize: 2,
            parallelUploads: 4,
            init: function () {
                uploader = this;

                document.getElementById("submit-all").addEventListener("click", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    uploader.processQueue();
                });

                this.on("sendingmultiple", function (data, xhr, formData) {

                    formData.append("submit", $("#submit-all").val());
                    if ($("#imagens").val() == undefined) {
                        formData.append("imagens", '');
                    } else {
                        formData.append("imagens", $("#imagens").val());
                    }
                    if ($("#pasta_imagens").val() == undefined) {
                        formData.append("pasta_imagens", '');
                    } else {
                        formData.append("pasta_imagens", $("#pasta_imagens").val());
                    }
                    if ($("#imagem_principal").val() == undefined) {
                        formData.append("imagem_principal", 0);
                    } else {
                        formData.append("imagem_principal", $("#imagem_principal").val());
                    }
                    formData.append("form_tipo", 3);
                    formData.append("id", $("#id").val());
                });
            }
        });

        uploader.on('thumbnail', function (file) {
            if (file.accepted !== false) {
                $('.form_submit_prd').prop('disabled', false);
            }
        });

        uploader.on("successmultiple", function (file, response) {

            var data = JSON.parse(response);

            if (data.erro == 1) {
                var imgerr = data.imgerr.split(',');
                var retorno = data.retorno;
                var msg = retorno + '<br/>';

                $.each(imgerr, function (index, value) {
                    msg += value + '<br/>';

                    if (uploader.files.length) {
                        for (var i = 0; i < uploader.files.length; i++) {
                            if (uploader.files[i].name == value) {
                                uploader.removeFile(uploader.files[i]);
                            }
                        }
                    }
                });

                $.UIkit.notify({
                    message: msg,
                    status: 'danger',
                    timeout: 5000,
                    pos: 'top-right',
                    onClose: function () {
                        window.location.href = data.page;
                    }
                });
            } else {
                $.UIkit.notify({
                    message: data.retorno,
                    status: 'success',
                    timeout: 5000,
                    pos: 'top-right',
                    onClose: function () {
                        window.location.href = data.page;
                    }
                });
            }

            $(".dz-remove").hide();
        });

        uploader.on("error", function (file) {
            uploader.removeFile(file);
        });

        $('.form_submit_prd').click(function (e) {
            var form = $(this).closest('form').attr("id");

            var infInicial = "O campo "
            var varPreenc = " deve ser preenchido!<br>";
            var msg = '';

            $("form#" + form + " input, form#" + form + " select, form#" + form + " textarea").each(function () {
                if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {

                    var input = $(this).attr('name');
                    var verificar = $(this).data('verif');
                    var nomecampo = $(this).data('name');

                    if (verificar != false) {
                        if ($(this).val() === "" && input != 'foto') {
                            msg += infInicial + nomecampo + varPreenc;
                        }
                    }
                }
            });

            if (msg != "") {
                $dispararAlerta(msg, 'warning');
                return false;
            } else {
                uploader.processQueue();
            }

        });
    } else {
        $('.form_submit_prd').click(function (e) {
            var form = $(this).closest('form').attr("id");

            var infInicial = "O campo "
            var varPreenc = " deve ser preenchido!<br>";
            var msg = '';

            $("form#" + form + " input, form#" + form + " select, form#" + form + " textarea").each(function () {
                if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {

                    var input = $(this).attr('name');
                    var verificar = $(this).data('verif');
                    var nomecampo = $(this).data('name');

                    if (verificar != false) {
                        if ($(this).val() === "" && input != 'foto') {
                            msg += infInicial + nomecampo + varPreenc;
                        }
                    }
                }
            });

            if (msg != "") {
                $dispararAlerta(msg, 'warning');
                return false;
            } else {
                $('form[name=' + form + ']').submit();
            }

        });
    }
});
