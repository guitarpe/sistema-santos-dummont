jQuery(document).ready(function ($) {

    var buttonCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    return data;
                }
            }
        }
    };

    var table = $('#tb-clientes').DataTable({
        "dom": '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"B>><"row"<"col-sm-12"tr>><"row"<"col-sm-6 nopad_r"i><"col-sm-6 nopad_l"p>>',
        "destroy": true,
        "processing": true,
        "serverSide": true,
        "order": [
            [0, "desc"]
        ],
        "language": {
            url: 'public_html/static/JavaScript/dataTable/lang/PT_BR.json'
        },
        buttons: [
            $.extend(true, {}, buttonCommon, {
                extend: 'copyHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'excelHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'pdfHtml5'
            })
        ],
        "ajax": {
            "url": "Clientes/ListaClientes",
            "type": "POST"
        },
        columns: [
            { searchable: false },
            null,
            null,
            null,
            null,
            { searchable: false, orderable: false }
        ],
        "columnDefs": [
            {
                "targets": [5],
                "class": "acoes",
                "orderable": false,
                "render": function (data, type, row) {
                    return '<a href="#" data-toggle="modal" data-target="#modal-lg" data-load-url="Clientes/Compras/' + row[0] + '" class="btn btn-xs btn-info" data-toggle="tooltip" data-load-title="Compras Realizadas pelo Cliente: ' + row[1] + '" data-placement="top" title="Compras Realizadas"><i class="entypo-info"></i></a> <a href="Clientes/Cliente/' + row[0] + '" class="btn btn-xs btn-orange" data-toggle="tooltip" data-placement="top" title="Editar Cadastro"><i class="entypo-pencil"></i></a> <a href="Clientes/Resetar" data-id="' + row[0] + '" class="btn btn-xs btn-black resetar_senha" data-toggle="tooltip" data-placement="top" title="Resetar Senha"><i class="entypo-erase"></i></a> <a href="Clientes/Inativar/' + row[0] + '" class="btn btn-xs btn-black change_status" data-toggle="tooltip" data-placement="top" title="Inativar"><i class="entypo-lock"></i></a> <a href="Clientes/Ativar/' + row[0] + '" class="btn btn-xs btn-green change_status" data-toggle="tooltip" data-placement="top" title="Ativar"><i class="entypo-lock-open"></i></a> <a href="Clientes/Excluir" data-id="' + row[0] + '" class="btn btn-xs btn-red deletar_cliente" data-toggle="tooltip" data-placement="top" title="Excluir Cliente"><i class="entypo-trash"></i></a>';
                }
            }
        ],
        'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).attr("id", aData[0]);
            return nRow;
        },
        "stripeClasses": ['', '']
    });

    $('#btnbuscar').click(function () {

        table.column(1).search($('#busca_nome').val().trim());
        table.column(2).search($('#busca_data').val().trim());
        table.column(3).search($('#busca_email').val().trim());

        table.draw();
    });

    $(".dataTables_wrapper select").select2({
        minimunResultsForSearch: -1
    });

    $(document).on('click', '.change_status_end', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        $.ajax({
            url: action,
            type: "POST",
            success: function (ret) {
                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, 'success');
                    table.ajax.reload(null, false);
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $(document).on('click', '.change_status', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        $.ajax({
            url: action,
            type: "POST",
            success: function (ret) {
                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, 'success');
                    table.ajax.reload(null, false);
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $(document).on('click', '.deletar_endereco', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', '.deletar_cliente', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', '.resetar_senha', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');

        bootbox.confirm({
            message: "<h3>Deseja realmente resetar a senha de acesso deste cliente?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $(document).on("blur", ".validcpfcliente", function () {

        var field = $(this);

        if (field.val() !== "") {
            $.ajax({
                type: "POST",
                url: "/Clientes/VerificaCPFCliente",
                data: {
                    cpf: field.val()
                },
                success: function (result) {
                    if (result > 0) {
                        $dispararAlerta("Documento já cadastrado", 'warning');
                        field.val("");
                        field.focus();
                    }
                }
            });
        }
    });

    $(document).on('blur', '.verificaemailcliente', function () {

        var field = $(this);

        if (field.val() !== "") {
            $.ajax({
                type: "POST",
                url: "/Clientes/VerificaEmailCliente",
                data: {
                    email: field.val()
                },
                success: function (result) {
                    if (result > 0) {
                        $dispararAlerta("E-mail já cadastrado", 'warning');
                        field.val("");
                        field.focus();
                    }
                }
            });
        }
    });

    $(document).on('blur', '.cpf', function () {

        var numdoc = $(this);

        if (numdoc.val() !== "") {
            $.ajax({
                type: "POST",
                url: "/Clientes/ValidaDoc",
                data: {
                    tipo: 1,
                    numdoc: numdoc.val()
                },
                success: function (result) {

                    if (result == 0) {
                        $dispararAlerta("CPF Inválido", 'warning');
                        numdoc.val("");
                        numdoc.focus();
                    }
                }
            });
        }
    });

    $(document).on('blur', '.cnpj', function () {

        var numdoc = $(this);

        if (numdoc.val() !== "") {
            $.ajax({
                type: "POST",
                url: "/Clientes/ValidaDoc",
                data: {
                    tipo: 2,
                    numdoc: numdoc.val()
                },
                success: function (result) {

                    if (result == 0) {
                        $dispararAlerta("CNPJ Inválido", 'warning');
                        numdoc.val("");
                        numdoc.focus();
                    }
                }
            });
        }
    });

    $('#tipo_doc').on('change', function () {
        if ($(this).val() == 'CPF') {
            $("#lbl_doc").text("CPF");
            $('#cpf').attr("placeholder", "CPF");
            $('#cpf').removeClass('cnpj');
            $('#cpf').addClass('cpf');
            $("input[name='genero']").attr("disabled", false);
        } else {
            $("#lbl_doc").text("CNPJ");
            $('#cpf').attr("placeholder", "CNPJ");
            $('#cpf').removeClass('cpf');
            $('#cpf').addClass('cnpj');
            $("input[name='genero']").attr("disabled", true);
        }
    });

});
