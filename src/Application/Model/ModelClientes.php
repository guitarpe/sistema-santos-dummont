<?php

namespace Application\Model;

use santosdummont\Model;

class ModelClientes extends Model
{

    public function ListaClientesCadastrados($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['token'],
            'I_ORDER' => $dados['order'],
            'I_DIR' => $dados['dir'],
            'I_START' => $dados['start'],
            'I_LENGTH' => $dados['length'],
            'I_NOME' => $dados['nome'],
            'I_DATA' => $dados['data'],
            'I_EMAIL' => $dados['email']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaClientes', $parametros), true);
    }

    public function CadastrarEditarCliente($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID'],
            'I_NOME_COMPLETO' => $dados['I_NOME_COMPLETO'],
            'I_TEL_RESID' => $dados['I_TEL_RESID'],
            'I_TEL_COM' => $dados['I_TEL_COM'],
            'I_TEL_CEL' => $dados['I_TEL_CEL'],
            'I_TIPO_DOC' => $dados['I_TIPO_DOC'],
            'I_NUM_DOC' => $dados['I_NUM_DOC'],
            'I_GENERO' => $dados['I_GENERO'],
            'I_DT_NASC' => $dados['I_DT_NASC'],
            'I_EMAIL' => $dados['I_EMAIL'],
            'I_OCUPACAO' => $dados['I_OCUPACAO']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarCliente', $parametros), true);
    }

    public function EditarEnderecoCliente($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID'],
            'I_END_ID' => $dados['I_END_ID'],
            'I_END_CEP' => $dados['I_END_CEP'],
            'I_END_LOGRADOURO' => $dados['I_END_LOGRADOURO'],
            'I_END_BAIRRO' => $dados['I_END_BAIRRO'],
            'I_END_CIDADE' => $dados['I_END_CIDADE'],
            'I_END_NUMERO' => $dados['I_END_NUMERO'],
            'I_END_UF' => $dados['I_END_UF'],
            'I_END_PAIS' => $dados['I_END_PAIS'],
            'I_END_COMPLEMENTO' => $dados['I_END_COMPLEMENTO'],
            'I_END_TIPO' => $dados['I_END_TIPO'],
            'I_END_ENVIAR' => $dados['I_END_ENVIAR'],
            'I_END_STATUS' => $dados['I_END_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDEditarEnderecoCliente', $parametros), true);
    }

    public function DadosCliente($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CLI_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosClientes', $parametros), true);
    }

    public function DadosEndereco($token, $cli, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CLI_ID' => $cli,
            'I_END_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosEnderecoClientes', $parametros), true);
    }

    public function DadosComprasCliente($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CLI_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosCompraClientes', $parametros), true);
    }

    public function EnderecosCliente($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CLI_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDEnderecosClientes', $parametros), true);
    }

    public function ExcluirCliente($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_US_ID' => $dados['ClI_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirClientes', $parametros), true);
    }

    public function ExcluirEndCliente($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CLI_ID' => $dados['ClI_ID'],
            'I_END_ID' => $dados['END_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirEnderecoClientes', $parametros), true);
    }

    public function AtivarInativarCliente($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['CLI_ID'],
            'I_CLI_STATUS' => $dados['CLI_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtivarInativarCliente', $parametros), true);
    }

    public function AtivarInativarEnderecoCliente($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['CLI_ID'],
            'I_END_ID' => $dados['END_ID'],
            'I_END_STATUS' => $dados['END_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtivarInativarEnderecoCliente', $parametros), true);
    }

    public function ResetarSenha($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ClI_ID' => $dados['ClI_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDResetarSenhaClientes', $parametros), true);
    }

    public function VerificaEmailClienteExiste($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_EMAIL' => $dados['I_EMAIL'],
            'I_ClI_ID' => $dados['I_ClI_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDVerificaEmailClienteExiste', $parametros), true);
    }

    public function VerificaCPFClienteExiste($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CPF' => $dados['I_CPF'],
            'I_ClI_ID' => $dados['I_ClI_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDVerificaCPFClienteExiste', $parametros), true);
    }
}
