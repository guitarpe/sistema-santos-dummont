<?php

namespace Application\Model;

use santosdummont\Model,
    santosdummont\Common;

class ModelConfig extends Model
{

    public function ListaCuponsCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDListaCupons', $parametros), true);
    }

    public function DadosCupon($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CUP_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosCupons', $parametros), true);
    }

    public function CadastrarEditarCupon($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CUP_ID' => $dados['CUP_ID'],
            'I_CUP_TOKEN' => $dados['CUP_TOKEN'],
            'I_DT_INI' => $dados['DT_INI'],
            'I_DT_FIM' => $dados['DT_FIM'],
            'I_PRD_ID' => $dados['PRD_ID'],
            'I_CUP_DESCONTO' => $dados['CUP_DESCONTO'],
            'I_CUP_TIPO' => $dados['CUP_TIPO'],
            'I_CUP_STATUS' => $dados['CUP_STATUS'],
            'I_CUP_DESCRICAO' => $dados['CUP_DESCRICAO']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarCupons', $parametros), true);
    }

    public function ExcluirCupon($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CUP_ID' => $dados['CUP_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirCupons', $parametros), true);
    }

    public function AtivaInativaCupon($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CUP_ID' => $dados['CUP_ID'],
            'I_CUP_STATUS' => $dados['CUP_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtivaInativaCupon', $parametros), true);
    }

    public function DadosFabricante($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PAR_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosFabricante', $parametros), true);
    }

    public function CadastrarEditarFabricante($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PAR_ID' => $dados['PAR_ID'],
            'I_PAR_NOME' => $dados['PAR_NOME'],
            'I_PAR_URL' => $dados['PAR_URL'],
            'I_PAR_STATUS' => $dados['PAR_STATUS'],
            'I_PAR_DESCRICAO' => $dados['PAR_DESCRICAO'],
            'I_PAR_INFO' => $dados['PAR_INFO'],
            'I_PAR_EXIBIR_SITE' => $dados['PAR_EXIBIR_SITE'],
            'I_PASTA_IMAGENS' => $dados['PASTA_IMAGENS'],
            'I_IMAGEM' => $dados['IMAGEM']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarFabricante', $parametros), true);
    }

    public function AtivaInativaFabricante($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PAR_ID' => $dados['PAR_ID'],
            'I_PAR_STATUS' => $dados['PAR_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtivaInativaFabricante', $parametros), true);
    }

    public function ExcluirFabricante($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PAR_ID' => $dados['PAR_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirFabricante', $parametros), true);
    }

    public function DadosMenu($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_MN_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosMenu', $parametros), true);
    }

    public function CadastrarEditarMenus($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MN_ID' => $dados['MN_ID'],
            'I_MN_NOME' => $dados['MN_NOME'],
            'I_MN_DESC' => $dados['MN_DESC'],
            'I_MN_LINK' => $dados['MN_LINK'],
            'I_MN_PAI' => $dados['MN_PAI'],
            'I_MN_IMG' => $dados['MN_IMG']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarMenus', $parametros), true);
    }

    public function ExcluirMenu($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MN_ID' => $dados['MN_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirMenu', $parametros), true);
    }

    public function ListaRedesCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaRedesCadastrados', $parametros), true);
    }

    public function DadosRede($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_RS_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosRede', $parametros), true);
    }

    public function CadastrarEditarRedes($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_RS_ID' => $dados['RS_ID'],
            'I_RS_NOME' => $dados['RS_NOME'],
            'I_RS_DESC' => $dados['RS_DESC'],
            'I_RS_LINK' => $dados['RS_LINK'],
            'I_RS_ICO' => $dados['RS_ICO'],
            'I_RE_EXEC_LOGIN' => $dados['RE_EXEC_LOGIN']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarRedes', $parametros), true);
    }

    public function ExcluirRede($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_RS_ID' => $dados['RS_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirRede', $parametros), true);
    }

    public function ConfiguracoesADM($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDConfiguracoesADM', $parametros), true);
    }

    public function SalvarSisInfoContato($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID'],
            'I_ENDERECO' => $dados['ENDERECO'],
            'I_BAIRRO' => $dados['BAIRRO'],
            'I_NUMERO' => $dados['NUMERO'],
            'I_CIDADE' => $dados['CIDADE'],
            'I_UF' => $dados['UF'],
            'I_CEP' => $dados['CEP'],
            'I_COMPLEMENTO' => $dados['COMPLEMENTO'],
            'I_TELEFONE' => $dados['TELEFONE'],
            'I_EMAIL_TECNICO' => $dados['EMAIL_TECNICO']
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDSalvarInfoContato', $parametros), true);
    }

    public function SalvarSisInfoSMTP($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID'],
            'I_SMTP_HOST' => Common::encrypt_decrypt('encrypt', $dados['SMTP_HOST']),
            'I_SMTP_PORTA' => Common::encrypt_decrypt('encrypt', $dados['SMTP_PORTA']),
            'I_SMTP_USER' => Common::encrypt_decrypt('encrypt', $dados['SMTP_USER']),
            'I_SMTP_SENHA' => Common::encrypt_decrypt('encrypt', $dados['SMTP_SENHA']),
            'I_MAIL_CONTATO_DEFAULT' => Common::encrypt_decrypt('encrypt', $dados['MAIL_CONTATO_DEFAULT']),
            'I_NOME_CONTATO_DEFAULT' => $dados['NOME_CONTATO_DEFAULT']
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDSalvarInfoSMTP', $parametros), true);
    }

    public function SalvarSisInformacoes($dados, $op)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID'],
            'I_INFO' => $dados['INFO'],
            'I_TIPO' => $op
        ];


        return json_decode(self::autenticacaoWebServices('servicoSDSalvarInformacoes', $parametros), true);
    }

    public function SalvarSisIntegracoes($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID'],
            'I_IP_INTEGRACAO' => $dados['IP_INTEGRACAO'],
            'I_EMAIL_NOTIFICACAO' => $dados['EMAIL_NOTIFICACAO'],
            'I_UNI_MED_PESO' => $dados['UNI_MED_PESO'],
            'I_UNI_MED_TAM' => $dados['UNI_MED_TAM'],
            'I_CORREIO_USUARIO' => Common::encrypt_decrypt('encrypt', $dados['CORREIO_USUARIO']),
            'I_CORREIO_SENHA' => Common::encrypt_decrypt('encrypt', $dados['CORREIO_SENHA']),
            'I_TRACKING_ID' => Common::encrypt_decrypt('encrypt', $dados['TRACKING_ID']),
            'I_SITE_ID' => Common::encrypt_decrypt('encrypt', $dados['SITE_ID']),
            'I_CHAVE_CAPTCHA' => Common::encrypt_decrypt('encrypt', $dados['CHAVE_CAPTCHA']),
            'I_FACEBOOK_ID' => Common::encrypt_decrypt('encrypt', $dados['FACEBOOK_ID']),
            'I_APP_SECRET_ID' => Common::encrypt_decrypt('encrypt', $dados['APP_SECRET_ID']),
            'I_CIELO_MECHANT_ID' => Common::encrypt_decrypt('encrypt', $dados['CIELO_MECHANT_ID']),
            'I_CIELO_MECHANT_KEY' => Common::encrypt_decrypt('encrypt', $dados['CIELO_MECHANT_KEY']),
            'I_CIELO_CLIENT_ID' => Common::encrypt_decrypt('encrypt', $dados['CIELO_CLIENT_ID']),
            'I_PAGSEG_EMAIL' => Common::encrypt_decrypt('encrypt', $dados['PAGSEG_EMAIL']),
            'I_PAGSEG_TIPO_ENVIO' => $dados['PAGSEG_TIPO_ENVIO'],
            'I_PAGSEG_SAND_TOKEN' => Common::encrypt_decrypt('encrypt', $dados['PAGSEG_SAND_TOKEN']),
            'I_PAGSEG_SAND_APP_ID' => Common::encrypt_decrypt('encrypt', $dados['PAGSEG_SAND_APP_ID']),
            'I_PAGSEG_SAND_APP_KEY' => Common::encrypt_decrypt('encrypt', $dados['PAGSEG_SAND_APP_KEY']),
            'I_PAGSEG_PRD_TOKEN' => Common::encrypt_decrypt('encrypt', $dados['PAGSEG_PRD_TOKEN']),
            'I_PAGSEG_PRD_APP_ID' => Common::encrypt_decrypt('encrypt', $dados['PAGSEG_PRD_APP_ID']),
            'I_PAGSEG_PRD_APP_KEY' => Common::encrypt_decrypt('encrypt', $dados['PAGSEG_PRD_APP_KEY']),
            'I_TAG_MANAGER_ID' => Common::encrypt_decrypt('encrypt', $dados['TAG_MANAGER_ID']),
            'I_GOOGLE_USUARIO' => Common::encrypt_decrypt('encrypt', $dados['GOOGLE_USUARIO']),
            'I_GOOGLE_SENHA' => $dados['GOOGLE_SENHA']
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDSalvarIntegracoes', $parametros), true);
    }

    public function SalvarSisImagens($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID'],
            'I_LOGOTIPO' => $dados['LOGOTIPO'],
            'I_FAVICON' => $dados['FAVICON']
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDSalvarImagens', $parametros), true);
    }

    public function SalvarSisVideos($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID'],
            'I_TIPO_VIDEO' => $dados['TIPO_VIDEO'],
            'I_VIDEO_BOASVINDAS' => $dados['VIDEO_BOASVINDAS']
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDSalvarVideos', $parametros), true);
    }

    public function ListaMeiosCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDListaMeiosCadastrados', $parametros), true);
    }

    public function DadosMeioPagamento($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_MEIO_ID' => $id
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDDadosMeioPagamento', $parametros), true);
    }

    public function CadastrarEditarMeioPagamento($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MEIO_ID' => $dados['MEIO_ID'],
            'I_NOME_MEIO' => $dados['NOME_MEIO'],
            'I_DESCRICAO_MEIO' => $dados['DESCRICAO_MEIO'],
            'I_MP_STATUS' => $dados['MP_STATUS'],
            'I_IMAGEM' => $dados['IMAGEM']
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDCadastrarEditarMeioPagamento', $parametros), true);
    }

    public function ExcluirMeioPagamento($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MEIO_ID' => $dados['MEIO_ID']
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDExcluirMeioPagamento', $parametros), true);
    }

    public function ListaMeiosEntregaCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDListaMeiosEntregaCadastrados', $parametros), true);
    }

    public function DadosMeioEntrega($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_MEIO_ID' => $id
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDDadosMeioEntrega', $parametros), true);
    }

    public function ListaCidadesAbrangencia($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_MEIO_ID' => $id
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDListaCidadesAbrangencia', $parametros), true);
    }

    public function CadastrarEditarMeioEntrega($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MEIO_ID' => $dados['MEIO_ID'],
            'I_NOME_MEIO' => $dados['NOME_MEIO'],
            'I_DESCRICAO_MEIO' => $dados['DESCRICAO_MEIO'],
            'I_CODIGO_MEIO' => $dados['CODIGO_MEIO'],
            'I_EMPRESA' => $dados['EMPRESA'],
            'I_RESTRICAO' => $dados['RESTRICAO'],
            'I_VALOR_COBRADO' => $dados['VALOR_COBRADO'],
            'I_PRAZO_MAXIMO' => $dados['PRAZO_MAXIMO'],
            'I_ME_STATUS' => $dados['ME_STATUS'],
            'I_CALC_FRETE' => $dados['CALC_FRETE'],
            'I_IMAGEM' => $dados['IMAGEM']
        ];

        $meioentrega = json_decode(self::autenticacaoWebServices('servicoSDCadastrarEditarMeioEntrega', $parametros), true);

        $return = null;

        if ($meioentrega['list'][0]['O_COD_RETORNO'] != 0) {
            $return['list'][0] = [
                'O_COD_RETORNO' => 0,
                'O_DESC_CURTO' => $meioentrega['list'][0]['O_DESC_CURTO'],
                'O_TOKEN_INVALIDO' => $meioentrega['list'][0]['O_TOKEN_INVALIDO']
            ];
        } else {
            $cidades = explode(',', $dados['CIDADES']);
            $estados = explode(',', $dados['ESTADOS']);

            $loop = 0;
            $ret = null;
            foreach ($cidades as $key => $value) {
                $info = [
                    'TOKEN' => $dados['TOKEN'],
                    'MEIO_ID' => $meioentrega['list'][0]['MEIO_ID'],
                    'CIDADE' => $cidades[$key],
                    'ESTADO' => $estados[$key]
                ];

                $ret = $this->CadastrarAbrangencia($info);

                if ($ret['list'][0]['O_COD_RETORNO'] == 0) {
                    $loop++;
                } else {
                    break;
                }
            }

            if (($loop == count($cidades))) {
                $return['list'][0] = [
                    'O_COD_RETORNO' => 0,
                    'O_DESC_CURTO' => $meioentrega['list'][0]['O_DESC_CURTO'],
                    'O_TOKEN_INVALIDO' => $meioentrega['list'][0]['O_TOKEN_INVALIDO']
                ];
            } else {
                $return['list'][0] = [
                    'O_COD_RETORNO' => 0,
                    'O_DESC_CURTO' => $ret['list'][0]['O_DESC_CURTO'],
                    'O_TOKEN_INVALIDO' => $ret['list'][0]['O_TOKEN_INVALIDO']
                ];
            }
        }

        return $return;
    }

    private function CadastrarAbrangencia($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MEIO_ID' => $dados['MEIO_ID'],
            'I_CIDADE' => $dados['CIDADE'],
            'I_ESTADO' => $dados['ESTADO']
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDCadastrarAbrangencia', $parametros), true);
    }

    public function ExcluirMeioEntrega($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MEIO_ID' => $dados['MEIO_ID']
        ];

        return json_decode(self::autenticacaoWebServices('servicoSDExcluirMeioEntrega', $parametros), true);
    }
}
