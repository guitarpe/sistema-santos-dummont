<?php

namespace Application\Model;

use santosdummont\Model;

class ModelCampanhas extends Model
{

    public function ListaBannersCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaBanners', $parametros), true);
    }

    public function DadosBanner($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_BAN_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosBanner', $parametros), true);
    }

    public function EditarConfigBanners($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID_CONFIG' => $dados['ID'],
            'I_BANNER_SENTIDO' => $dados['BANNER_SENTIDO'],
            'I_BANNER_TEMPO' => $dados['BANNER_TEMPO']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDEditarConfigBanners', $parametros), true);
    }

    public function CadastrarEditarBanner($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_BAN_ID' => $dados['BAN_ID'],
            'I_BAN_DESCRICAO' => $dados['BAN_DESCRICAO'],
            'I_LINK' => $dados['LINK'],
            'I_LOCAL_LINK' => $dados['LOCAL_LINK'],
            'I_BAN_POS_TEXTOS' => $dados['BAN_POS_TEXTOS'],
            'I_BAN_TEXTO_PRINCIPAL' => $dados['BAN_TEXTO_PRINCIPAL'],
            'I_BAN_EFEITO_TXT_PRINC' => $dados['BAN_EFEITO_TXT_PRINC'],
            'I_BAN_COR_TXT_PRI' => $dados['BAN_COR_TXT_PRI'],
            'I_BAN_SEG_TEXTO' => $dados['BAN_SEG_TEXTO'],
            'I_BAN_EFEITO_SEG_TEXTO' => $dados['BAN_EFEITO_SEG_TEXTO'],
            'I_BAN_COR_TXT_SEC' => $dados['BAN_COR_TXT_SEC'],
            'I_BAN_TER_TEXTO' => $dados['BAN_TER_TEXTO'],
            'I_BAN_EFEITO_TER_TEXTO' => $dados['BAN_EFEITO_TER_TEXTO'],
            'I_BAN_COR_TXT_TER' => $dados['BAN_COR_TXT_TER'],
            'I_BAN_STATUS' => $dados['BAN_STATUS'],
            'I_BAN_BT_TEXTO' => $dados['BAN_BT_TEXTO'],
            'I_BAN_BT_COR' => $dados['BAN_BT_COR'],
            'I_IMAGEM' => $dados['IMAGEM'],
            'I_PASTA_IMAGENS' => $dados['PASTA_IMAGENS']
        ];

        $alias = 'servicoSDCadastrarEditarBanner';

        return json_decode(parent::autenticacaoWebServices($alias, $parametros), true);
    }

    public function ExcluirBanner($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_BAN_ID' => $dados['BAN_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirBanner', $parametros), true);
    }

    public function AtivarInativarBanner($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_BAN_ID' => $dados['BAN_ID'],
            'I_BAN_STATUS' => $dados['BAN_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtivarInativarBanner', $parametros), true);
    }

    public function ListaDestaquesCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaDestaquesCadastrados', $parametros), true);
    }

    public function DadosDestaque($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_DES_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosDestaque', $parametros), true);
    }

    public function CadastrarEditarDestaque($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_DES_ID' => $dados['DES_ID'],
            'I_DES_DESCRICAO' => $dados['DES_DESCRICAO'],
            'I_DES_TIPO' => $dados['DES_TIPO'],
            'I_DES_DATA_INI' => $dados['DES_DATA_INI'],
            'I_DES_DATA_FIM' => $dados['DES_DATA_FIM'],
            'I_DES_STATUS' => $dados['DES_STATUS'],
            'I_DES_SELECIONADOS' => $dados['DES_SELECIONADOS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarDestaque', $parametros), true);
    }

    public function ExcluirDestaque($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_DES_ID' => $dados['DES_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirDestaque', $parametros), true);
    }

    public function Pesquisa($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_TIPO' => $dados['TIPO'],
            'I_INFO' => $dados['INFO']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDPesquisaLista', $parametros), true);
    }

    public function ListaProdutosDestaque($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_DES_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaProdutosDestaque', $parametros), true);
    }

    public function ListaNovidadesCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaNovidadesCadastrados', $parametros), true);
    }

    public function DadosNovidade($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_NOV_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosNovidade', $parametros), true);
    }

    public function CadastrarEditarNovidade($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_NOV_ID' => $dados['NOV_ID'],
            'I_NOV_DESCRICAO' => $dados['NOV_DESCRICAO'],
            'I_NOV_TIPO' => $dados['NOV_TIPO'],
            'I_NOV_DATA_INI' => $dados['NOV_DATA_INI'],
            'I_NOV_DATA_FIM' => $dados['NOV_DATA_FIM'],
            'I_NOV_STATUS' => $dados['NOV_STATUS'],
            'I_NOV_SELECIONADOS' => $dados['NOV_SELECIONADOS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarNovidade', $parametros), true);
    }

    public function ExcluirNovidade($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_NOV_ID' => $dados['NOV_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirNovidade', $parametros), true);
    }

    public function PesquisaNov($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_TIPO' => $dados['TIPO'],
            'I_INFO' => $dados['INFO']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDPesquisaLista', $parametros), true);
    }

    public function ListaProdutosNovidade($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_NOV_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaProdutosNovidade', $parametros), true);
    }

    public function ListaPromocoesCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaPromocoesCadastrados', $parametros), true);
    }

    public function DadosPromocao($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRO_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosPromocao', $parametros), true);
    }

    public function CadastrarEditarPromocao($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PRO_ID' => $dados['PRO_ID'],
            'I_PRO_DESCRICAO' => $dados['PRO_DESCRICAO'],
            'I_PRO_TIPO' => $dados['PRO_TIPO'],
            'I_PRO_DATA_INI' => $dados['PRO_DATA_INI'],
            'I_PRO_DATA_FIM' => $dados['PRO_DATA_FIM'],
            'I_PRO_STATUS' => $dados['PRO_STATUS'],
            'I_PRO_SELECIONADOS' => $dados['PRO_SELECIONADOS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarPromocao', $parametros), true);
    }

    public function ExcluirPromocao($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PRO_ID' => $dados['PRO_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirPromocao', $parametros), true);
    }

    public function PesquisaPro($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_TIPO' => $dados['TIPO'],
            'I_INFO' => $dados['INFO']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDPesquisaLista', $parametros), true);
    }

    public function ListaProdutosPromocao($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRO_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaProdutosPromocao', $parametros), true);
    }

    public function ListaProdutosEspeciais($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaProdutosEspeciaisAdm', $parametros), true);
    }

    public function DadosProdutoEspecial($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRO_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDProdutoEspecial', $parametros), true);
    }

    public function CadastrarEditarProdutoEsp($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PRO_ID' => $dados['PRO_ID'],
            'I_PRO_DESCRICAO' => $dados['PRO_DESCRICAO'],
            'I_PRO_TIPO' => $dados['PRO_TIPO'],
            'I_PRO_DATA_INI' => $dados['PRO_DATA_INI'],
            'I_PRO_DATA_FIM' => $dados['PRO_DATA_FIM'],
            'I_PRO_STATUS' => $dados['PRO_STATUS'],
            'I_PRO_SELECIONADOS' => $dados['PRO_SELECIONADOS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarProdutoEsp', $parametros), true);
    }

    public function ExcluirProdutoEsp($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PRO_ID' => $dados['PRO_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirProdutoEsp', $parametros), true);
    }

    public function PesquisaEsp($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_TIPO' => $dados['TIPO'],
            'I_INFO' => $dados['INFO']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDPesquisaEsp', $parametros), true);
    }

    public function ListaProdutosEspeciaisCadastrados($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRO_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaProdutosEspeciaisCadastrados', $parametros), true);
    }

    public function ListaEmailsNewsletter($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaEmailsNewsletter', $parametros), true);
    }

    public function ListaContatoWhats($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaContatoWhats', $parametros), true);
    }

    public function ListaModaisCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaModaisCadastrados', $parametros), true);
    }

    public function DadosModal($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_MOD_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosModal', $parametros), true);
    }

    public function CadastrarEditarModal($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MOD_ID' => $dados['MOD_ID'],
            'I_MOD_DESCRICAO' => $dados['MOD_DESCRICAO'],
            'I_MOD_LINK' => $dados['MOD_LINK'],
            'I_MOD_DATA_INI' => $dados['MOD_DATA_INI'],
            'I_MOD_DATA_FIM' => $dados['MOD_DATA_FIM'],
            'I_MOD_STATUS' => $dados['MOD_STATUS'],
            'I_MOD_IMAGEM' => $dados['MOD_IMAGEM'],
            'I_MOD_PASTA_IMAGENS' => $dados['MOD_PASTA_IMAGENS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarModal', $parametros), true);
    }

    public function ExcluirModal($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MOD_ID' => $dados['MOD_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirModal', $parametros), true);
    }
}
