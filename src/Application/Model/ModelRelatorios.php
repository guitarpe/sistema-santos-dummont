<?php

namespace Application\Model;

use santosdummont\Model;

class ModelRelatorios extends Model
{

    public function ListaRelatorioUsuarios($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_TIPO' => 1
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaRelatorios', $parametros), true);
    }

    public function ListaRelatorioClientes($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_TIPO' => 2
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaRelatorios', $parametros), true);
    }

    public function ListaRelatorioAcessos($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_TIPO' => 3
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaRelatorios', $parametros), true);
    }

    public function ListaPedidosCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaPedidosCadastrados', $parametros), true);
    }

    public function ListaProdutosCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaProdutosCadastrados', $parametros), true);
    }

    public function ListaLoginsClientes($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaLoginsClientes', $parametros), true);
    }

    public function ListaLoginUsuariosSistema($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaLoginsUsuariosSistema', $parametros), true);
    }

    public function ListaUltimasBuscas($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaUltimasBuscas', $parametros), true);
    }

    public function ListaAcessosCidades($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaAcessosCidades', $parametros), true);
    }
}
