<?php
namespace Application\Model;

use santosdummont\Model;

class ModelVenda extends Model
{

    public function ListaPedidosRealizados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaPedidosCadastrados', $parametros), true);
    }

    public function ListaDetalhePedido($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PED_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaDetalhePedido', $parametros), true);
    }

    public function ListaItensDetalhePedido($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PED_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDItensPedido', $parametros), true);
    }

    public function CancelarPedido($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PED_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCancelaPedido', $parametros), true);
    }

    public function SepararDoEstoque($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PED_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDSepararDoEstoque', $parametros), true);
    }

    public function SepararEnvioCliente($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PED_ID' => $dados['I_PED_ID'],
            'I_LOCALIZADOR' => $dados['I_LOCALIZADOR'],
            'I_OBSERVACAO' => $dados['I_OBSERVACAO']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDSepararEnvioCliente', $parametros), true);
    }

    public function ReverterPedido($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PED_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDReverterPedido', $parametros), true);
    }
}
