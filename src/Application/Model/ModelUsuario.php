<?php

namespace Application\Model;

use santosdummont\Model;

class ModelUsuario extends Model
{

    public function ListaPerfisCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaPerfisCadastrados', $parametros), true);
    }

    public function DadosPerfil($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PER_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosPerfis', $parametros), true);
    }

    public function CadastrarEditarPerfis($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PER_ID' => $dados['PER_ID'],
            'I_PER_SIGLA' => $dados['PER_SIGLA'],
            'I_PER_DESC' => $dados['PER_DESC'],
            'I_PER_PERMISSIONS' => $dados['PER_PERMISSIONS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarPerfis', $parametros), true);
    }

    public function ExcluirPerfil($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PER_ID' => $dados['PER_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirPerfis', $parametros), true);
    }

    public function ListaUsuariosCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaUsuariosCadastrados', $parametros), true);
    }

    public function DadosUsuario($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_US_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosUsuarios', $parametros), true);
    }

    public function AtivarInativarUsuario($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_US_ID' => $dados['US_ID'],
            'I_US_STATUS' => $dados['US_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtivarInativarUsuario', $parametros), true);
    }

    public function CadastrarEditarUsuarios($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_US_ID' => $dados['US_ID'],
            'I_PER_ID' => $dados['PER_ID'],
            'I_US_LOGIN' => $dados['US_LOGIN'],
            'I_US_PASS' => $dados['US_PASS'],
            'I_US_STATUS' => $dados['US_STATUS'],
            'I_US_IMAGE' => $dados['US_IMAGEM'],
            'I_NOME_USER' => $dados['NOME_USER'],
            'I_EMAIL_USER' => $dados['EMAIL_USER'],
            'I_GENERO_USER' => $dados['GENERO_USER'],
            'I_TEL_USER' => $dados['TEL_USER'],
            'I_PASTA_USER' => $dados['PASTA_USER']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarUsuarios', $parametros), true);
    }

    public function ExcluirUsuario($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_US_ID' => $dados['US_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirUsuarios', $parametros), true);
    }
    #METODOS DA TELA DE LOGIN

    public function ValidaLogin($login, $senha)
    {
        $pass = strtoupper(md5($login . $senha));

        $parametros = [
            'I_US_LOGIN' => $login,
            'I_US_SENHA' => $pass
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDValidaLogin', $parametros), true);
    }

    public function TrocarSenhaReset($dados)
    {
        $parametros = [
            'I_US_ID' => $dados['US_ID'],
            'I_US_LOGIN' => $dados['US_LOGIN'],
            'I_US_SENHA' => $dados['US_SENHA'],
            'I_US_STATUS' => $dados['US_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDTrocarSenha', $parametros), true);
    }

    public function UsuarioPorID($id)
    {
        $parametros = [
            'I_US_ID' => $id,
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDUsuarioPorID', $parametros), true);
    }

    public function UsuarioPorEmail($email)
    {
        $parametros = [
            'I_MAIL_ID' => $email,
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDUsuarioPorEmail', $parametros), true);
    }
}
