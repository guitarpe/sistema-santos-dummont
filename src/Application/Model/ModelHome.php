<?php

namespace Application\Model;

use santosdummont\Model;

class ModelHome extends Model
{

    public function DadosHome($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosHome', $parametros), true);
    }

    public function getNotificacoes($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDLerNotificacoes', $parametros), true);
    }
}
