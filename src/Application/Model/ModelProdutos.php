<?php

namespace Application\Model;

use santosdummont\Model;

class ModelProdutos extends Model
{

    function __construct()
    {
        parent::__construct('SantosDummont');
    }

    public function ListaProdutosCadastrados($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['token'],
            'I_ORDER' => $dados['order'],
            'I_DIR' => $dados['dir'],
            'I_START' => $dados['start'],
            'I_LENGTH' => $dados['length'],
            'I_NOME' => !empty($dados['nome']) ? $dados['nome'] : null,
            'I_SKU' => !empty($dados['sku']) ? $dados['sku'] : null,
            'I_CATNOME' => !empty($dados['catnome']) ? $dados['catnome'] : null,
            'I_STATUS' => !empty($dados['status']) ? $dados['status'] : 2,
            'I_ESTOQUE' => !empty($dados['estoque']) ? $dados['estoque'] : 2
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaProdutos', $parametros), true);
    }

    public function ProdutoExisteSKU($token, $sku)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRD_SKU' => $sku
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDProdutoExisteSKU', $parametros), true);
    }

    public function ListaProdutosAPI($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaProdutosAPI', $parametros), true);
    }

    public function ListaProdutosAPICron()
    {
        $parametros = [];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaProdutosAPICron', $parametros), true);
    }

    public function ListaCategoriasBuscaCadastrados($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['token'],
            'I_ORDER' => $dados['order'],
            'I_DIR' => $dados['dir'],
            'I_START' => $dados['start'],
            'I_LENGTH' => $dados['length'],
            'I_NOME' => $dados['nome'],
            'I_PAI' => $dados['pai'],
            'I_STATUS' => $dados['status']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaCategoriasBuscaCadastrados', $parametros), true);
    }

    public function ListaCategoriasCadastradas($token, $cat = null)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CATEGORIA' => $cat
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaCategorias', $parametros), true);
    }

    public function DadosCategoria($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosCategoria', $parametros), true);
    }

    public function CadastrarEditarCategoria($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CAT_ID' => $dados['CAT_ID'],
            'I_CAT_NOME' => $dados['CAT_NOME'],
            'I_CAT_STATUS' => $dados['CAT_STATUS'],
            'I_CAT_PAI' => $dados['CAT_PAI'],
            'I_CAT_POSICAO' => $dados['CAT_POSICAO'],
            'I_CAT_NIVEL' => $dados['CAT_NIVEL'],
            'I_CAT_URL' => $dados['CAT_URL'],
            'I_CAT_IMAGEM' => $dados['CAT_IMAGEM']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarCategoria', $parametros), true);
    }

    public function ExcluirCategoria($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirCategoria', $parametros), true);
    }

    public function ExcluirImagemCategoria($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirImagemCategoria', $parametros), true);
    }

    public function ListaCategoriasProdutos($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaCategoriasProdutos', $parametros), true);
    }

    public function ListaCategoriasdoProduto($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRD_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaCategoriasdoProdutos', $parametros), true);
    }

    public function ListaFabricantesCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaFabricantes', $parametros), true);
    }

    public function DadosProduto($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosProduto', $parametros), true);
    }

    public function ListaCategoriasInfo($token, $prd)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRD_ID' => $prd
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaCategoriasInfo', $parametros), true);
    }

    public function FotosProduto($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ID' => $id
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDFotosProduto', $parametros), true);
    }

    public function ImagemProduto($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PRD_ID' => $dados['PRD_ID'],
            'I_IMG' => $dados['IMG']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDImagemProduto', $parametros), true);
    }

    public function CadastrarEditarProduto($dados, $tipo = null)
    {
        if ($tipo == 1) {
            $parametros = [
                'I_TOKEN' => $dados['TOKEN'],
                'I_TIPO' => $tipo,
                'I_PRD_ID' => $dados['PRD_ID'],
                'I_PRD_DESTAQUE' => $dados['PRD_DESTAQUE'],
                'I_CATEGORIAS' => $dados['CATEGORIAS'],
                'I_PRD_NOME' => $dados['PRD_NOME'],
                'I_PRD_PATH' => $dados['PRD_PATH'],
                'I_PRD_STATUS' => $dados['PRD_STATUS'],
                'I_PRD_QTDE_ESTOQUE' => $dados['PRD_QTDE_ESTOQUE'],
                'I_PRD_QTDE_MINIMA' => $dados['PRD_QTDE_MINIMA'],
                'I_PRD_SKU' => $dados['PRD_SKU'],
                'I_PRD_UNIDADE' => $dados['PRD_UNIDADE'],
                'I_PRD_PRECO' => $dados['PRD_PRECO'],
                'I_PRD_PRECO_ANTERIOR' => $dados['PRD_PRECO_ANTERIOR'],
                'I_PRD_TAXA' => $dados['PRD_TAXA'],
                'I_PRD_MODELO' => null,
                'I_PRD_NOME_TECNICO' => null,
                'I_PRD_FICHA_TECNICA' => null,
                'I_PRD_DESCRICAO_MINIMA' => null,
                'I_PRD_MARCA' => null,
                'I_PRD_PESO' => null,
                'I_PRD_LARGURA' => null,
                'I_PRD_ALTURA' => null,
                'I_PRD_COMPRIMENTO' => null,
                'I_PRD_PROFUNDIDADE' => null,
                'I_PRD_VOLTAGEM' => null,
                'I_PRD_COR' => null,
                'I_PRD_TAGS' => null,
                'I_PAR_ID' => 0,
                'I_IMAGENS' => null,
                'I_PASTA_IMAGENS' => null
            ];
        }

        if ($tipo == 2) {
            $parametros = [
                'I_TOKEN' => $dados['TOKEN'],
                'I_TIPO' => $tipo,
                'I_PRD_ID' => $dados['PRD_ID'],
                'I_PRD_DESTAQUE' => null,
                'I_CATEGORIAS' => null,
                'I_PRD_NOME' => null,
                'I_PRD_PATH' => null,
                'I_PRD_STATUS' => null,
                'I_PRD_QTDE_ESTOQUE' => null,
                'I_PRD_QTDE_MINIMA' => null,
                'I_PRD_SKU' => null,
                'I_PRD_UNIDADE' => null,
                'I_PRD_PRECO' => 0,
                'I_PRD_PRECO_ANTERIOR' => 0,
                'I_PRD_TAXA' => 0,
                'I_PRD_MODELO' => $dados['PRD_MODELO'],
                'I_PRD_NOME_TECNICO' => $dados['PRD_NOME_TECNICO'],
                'I_PRD_FICHA_TECNICA' => $dados['PRD_FICHA_TECNICA'],
                'I_PRD_DESCRICAO_MINIMA' => $dados['PRD_DESCRICAO_MINIMA'],
                'I_PRD_MARCA' => $dados['PRD_MARCA'],
                'I_PRD_PESO' => $dados['PRD_PESO'],
                'I_PRD_LARGURA' => $dados['PRD_LARGURA'],
                'I_PRD_ALTURA' => $dados['PRD_ALTURA'],
                'I_PRD_COMPRIMENTO' => $dados['PRD_COMPRIMENTO'],
                'I_PRD_PROFUNDIDADE' => $dados['PRD_PROFUNDIDADE'],
                'I_PRD_VOLTAGEM' => $dados['PRD_VOLTAGEM'],
                'I_PRD_COR' => $dados['PRD_COR'],
                'I_PRD_TAGS' => $dados['PRD_TAGS'],
                'I_PAR_ID' => $dados['PAR_ID'],
                'I_IMAGENS' => null,
                'I_PASTA_IMAGENS' => null
            ];
        }

        if ($tipo == 3) {
            $parametros = [
                'I_TOKEN' => $dados['TOKEN'],
                'I_TIPO' => $tipo,
                'I_PRD_ID' => $dados['PRD_ID'],
                'I_PRD_DESTAQUE' => null,
                'I_CATEGORIAS' => null,
                'I_PRD_NOME' => null,
                'I_PRD_PATH' => null,
                'I_PRD_STATUS' => null,
                'I_PRD_QTDE_ESTOQUE' => null,
                'I_PRD_QTDE_MINIMA' => null,
                'I_PRD_SKU' => null,
                'I_PRD_UNIDADE' => null,
                'I_PRD_PRECO' => 0,
                'I_PRD_PRECO_ANTERIOR' => 0,
                'I_PRD_TAXA' => 0,
                'I_PRD_MODELO' => null,
                'I_PRD_NOME_TECNICO' => null,
                'I_PRD_FICHA_TECNICA' => null,
                'I_PRD_DESCRICAO_MINIMA' => null,
                'I_PRD_MARCA' => null,
                'I_PRD_PESO' => null,
                'I_PRD_LARGURA' => null,
                'I_PRD_ALTURA' => null,
                'I_PRD_COMPRIMENTO' => null,
                'I_PRD_PROFUNDIDADE' => null,
                'I_PRD_VOLTAGEM' => null,
                'I_PRD_COR' => null,
                'I_PRD_TAGS' => null,
                'I_PAR_ID' => 0,
                'I_IMAGENS' => $dados['IMAGENS'],
                'I_PASTA_IMAGENS' => $dados['PASTA_IMAGENS']
            ];
        }

        if ($tipo == 4) {
            $parametros = [
                'I_TOKEN' => $dados['I_TOKEN'],
                'I_TIPO' => $tipo,
                'I_PRD_ID' => 0,
                'I_PRD_DESTAQUE' => $dados['I_PRD_DESTAQUE'],
                'I_CATEGORIAS' => null,
                'I_PRD_NOME' => $dados['I_PRD_NOME'],
                'I_PRD_PATH' => $dados['I_PRD_PATH'],
                'I_PRD_STATUS' => $dados['I_PRD_STATUS'],
                'I_PRD_QTDE_ESTOQUE' => $dados['I_PRD_QTDE_ESTOQUE'],
                'I_PRD_QTDE_MINIMA' => $dados['I_PRD_QTDE_MINIMA'],
                'I_PRD_SKU' => $dados['I_PRD_SKU'],
                'I_PRD_UNIDADE' => $dados['I_PRD_UNIDADE'],
                'I_PRD_PRECO' => $dados['I_PRD_PRECO'],
                'I_PRD_PRECO_ANTERIOR' => $dados['I_PRD_PRECO_ANTERIOR'],
                'I_PRD_TAXA' => $dados['I_PRD_TAXA'],
                'I_PRD_MODELO' => $dados['I_PRD_MODELO'],
                'I_PRD_NOME_TECNICO' => $dados['I_PRD_NOME_TECNICO'],
                'I_PRD_FICHA_TECNICA' => null,
                'I_PRD_DESCRICAO_MINIMA' => null,
                'I_PRD_MARCA' => $dados['I_PRD_MARCA'],
                'I_PRD_PESO' => $dados['I_PRD_PESO'],
                'I_PRD_LARGURA' => $dados['I_PRD_LARGURA'],
                'I_PRD_ALTURA' => $dados['I_PRD_ALTURA'],
                'I_PRD_COMPRIMENTO' => $dados['I_PRD_COMPRIMENTO'],
                'I_PRD_PROFUNDIDADE' => $dados['I_PRD_PROFUNDIDADE'],
                'I_PRD_VOLTAGEM' => $dados['I_PRD_VOLTAGEM'],
                'I_PRD_COR' => $dados['I_PRD_COR'],
                'I_PRD_TAGS' => $dados['I_PRD_TAGS'],
                'I_PAR_ID' => 0,
                'I_IMAGENS' => null,
                'I_PASTA_IMAGENS' => null
            ];
        }

        if (empty($tipo)) {
            $parametros = [
                'I_TOKEN' => $dados['TOKEN'],
                'I_TIPO' => 0,
                'I_PRD_ID' => $dados['PRD_ID'],
                'I_PRD_DESTAQUE' => $dados['PRD_DESTAQUE'],
                'I_CATEGORIAS' => $dados['CATEGORIAS'],
                'I_PRD_NOME' => $dados['PRD_NOME'],
                'I_PRD_PATH' => $dados['PRD_PATH'],
                'I_PRD_STATUS' => $dados['PRD_STATUS'],
                'I_PRD_QTDE_ESTOQUE' => $dados['PRD_QTDE_ESTOQUE'],
                'I_PRD_QTDE_MINIMA' => $dados['PRD_QTDE_MINIMA'],
                'I_PRD_SKU' => $dados['PRD_SKU'],
                'I_PRD_UNIDADE' => $dados['PRD_UNIDADE'],
                'I_PRD_PRECO' => $dados['PRD_PRECO'],
                'I_PRD_PRECO_ANTERIOR' => $dados['PRD_PRECO_ANTERIOR'],
                'I_PRD_TAXA' => $dados['PRD_TAXA'],
                'I_PRD_MODELO' => $dados['PRD_MODELO'],
                'I_PRD_NOME_TECNICO' => $dados['PRD_NOME_TECNICO'],
                'I_PRD_FICHA_TECNICA' => $dados['PRD_FICHA_TECNICA'],
                'I_PRD_DESCRICAO_MINIMA' => $dados['PRD_DESCRICAO_MINIMA'],
                'I_PRD_MARCA' => $dados['PRD_MARCA'],
                'I_PRD_PESO' => $dados['PRD_PESO'],
                'I_PRD_LARGURA' => $dados['PRD_LARGURA'],
                'I_PRD_ALTURA' => $dados['PRD_ALTURA'],
                'I_PRD_COMPRIMENTO' => $dados['PRD_COMPRIMENTO'],
                'I_PRD_PROFUNDIDADE' => $dados['PRD_PROFUNDIDADE'],
                'I_PRD_VOLTAGEM' => $dados['PRD_VOLTAGEM'],
                'I_PRD_COR' => $dados['PRD_COR'],
                'I_PRD_TAGS' => $dados['PRD_TAGS'],
                'I_PAR_ID' => $dados['PAR_ID'],
                'I_PASTA_IMAGENS' => '',
                'I_IMAGENS' => $dados['IMAGENS']
            ];
        }

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarEditarProduto', $parametros), true);
    }

    public function AtivaInativaProduto($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PRD_ID' => $dados['PRD_ID'],
            'I_PRD_STATUS' => $dados['PRD_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtivaInativaProduto', $parametros), true);
    }

    public function AtualizaAPIProduto($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_SKU' => $dados['I_PRD_SKU'],
            'I_PRD_UNIDADE' => $dados['I_PRD_UNIDADE'],
            'I_PRD_NOME' => $dados['I_PRD_NOME'],
            'I_PRD_URL' => $dados['I_PRD_URL'],
            'I_PRD_QTDE_ESTOQUE' => $dados['I_PRD_QTDE_ESTOQUE'],
            'I_PRD_PRECO' => $dados['I_PRD_PRECO'],
            'I_PRD_COD_CETRUS' => $dados['I_PRD_COD_CETRUS'],
            'I_PRD_MARCA' => $dados['I_PRD_MARCA'],
            'I_PRD_MARCA_URL' => $dados['I_PRD_MARCA_URL'],
            'I_PRD_PESO' => $dados['I_PRD_PESO'],
            'I_PRD_ALTURA' => $dados['I_PRD_ALTURA'],
            'I_PRD_LARGURA' => $dados['I_PRD_LARGURA'],
            'I_PRD_STATUS' => $dados['I_PRD_STATUS'],
            'I_PRD_COMPRIMENTO' => $dados['I_PRD_COMPRIMENTO'],
            'I_PRD_PROFUNDIDADE' => $dados['I_PRD_PROFUNDIDADE']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtualizaAPIProduto', $parametros), true);
    }

    public function AtualizaAPIProdutoCron($dados)
    {
        $parametros = [
            'I_PRD_SKU' => $dados['I_PRD_SKU'],
            'I_PRD_UNIDADE' => $dados['I_PRD_UNIDADE'],
            'I_PRD_NOME' => $dados['I_PRD_NOME'],
            'I_PRD_URL' => $dados['I_PRD_URL'],
            'I_PRD_QTDE_ESTOQUE' => $dados['I_PRD_QTDE_ESTOQUE'],
            'I_PRD_PRECO' => $dados['I_PRD_PRECO'],
            'I_PRD_COD_CETRUS' => $dados['I_PRD_COD_CETRUS'],
            'I_PRD_MARCA' => $dados['I_PRD_MARCA'],
            'I_PRD_MARCA_URL' => $dados['I_PRD_MARCA_URL'],
            'I_PRD_PESO' => $dados['I_PRD_PESO'],
            'I_PRD_ALTURA' => $dados['I_PRD_ALTURA'],
            'I_PRD_LARGURA' => $dados['I_PRD_LARGURA'],
            'I_PRD_STATUS' => $dados['I_PRD_STATUS'],
            'I_PRD_COMPRIMENTO' => $dados['I_PRD_COMPRIMENTO'],
            'I_PRD_PROFUNDIDADE' => $dados['I_PRD_PROFUNDIDADE']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtualizaAPIProdutoCron', $parametros), true);
    }

    public function ExcluirProduto($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirProduto', $parametros), true);
    }

    public function ExcluirImageProduto($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_IMG' => $dados['IMG'],
            'I_PRD_ID' => $dados['PRD_ID']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDExcluirImagem', $parametros), true);
    }

    public function SetarImagemPrincipal($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PRD_ID' => $dados['PRD_ID'],
            'I_IMG' => $dados['IMG']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDSetarImagemPrincipal', $parametros), true);
    }

    public function CategoriaExiste($token, $categoria)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CAT_NOME' => $categoria,
            'I_CAT_STATUS' => 1,
            'I_CAT_PAI' => 0
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCadastrarCategoriaImportacao', $parametros), true);
    }

    public function AtivaInativaCategoria($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CAT_ID' => $dados['CAT_ID'],
            'I_CAT_STATUS' => $dados['CAT_STATUS']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtivaInativaCategoria', $parametros), true);
    }

    public function InativarTokensExpirados()
    {
        $parametros = [];

        return json_decode(parent::autenticacaoWebServices('servicoSDInativaTokensExpirados', $parametros), true);
    }

    public function GravaProdutoPorSKU($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_SKU' => $dados['I_PRD_SKU'],
            'I_IMAGEM' => $dados['I_IMAGEM']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDInfoProdutosPorSKU', $parametros), true);
    }

    public function DadosProdutoPorSKU($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_SKU' => $dados['I_PRD_SKU']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDDadosProdutoPorSKU', $parametros), true);
    }

    public function ListaPedidosCron()
    {
        $parametros = [];

        return json_decode(parent::autenticacaoWebServices('servicoSDListaPedidosCron', $parametros), true);
    }

    public function AtualizaAPIPedidoCron($dados)
    {
        $parametros = [
            'I_PED_ID' => $dados['PED_ID'],
            'I_PED_STATUS' => $dados['PED_STATUS'],
            'I_PED_DATE' => $dados['PED_DATE']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDAtualizaAPIPedidoCron', $parametros), true);
    }
}
