<?php
namespace Application\Model;

use santosdummont\Model;

class ModelCron extends Model
{

    function __construct()
    {
        parent::__construct('SantosDummont');
    }

    public function CronListaProdutos($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCronListaProdutos', $parametros), true);
    }

    public function CronUpdateProdutos($dados)
    {
        $parametros = [
            'I_PRD_ID' => $dados['PRD_ID'],
            'I_PASTA_IMAGENS' => $dados['PASTA_IMAGENS'],
            'I_PRD_IMAGEM_PRINCIPAL' => $dados['PRD_IMAGEM_PRINCIPAL']
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDCronUpdateProduto', $parametros), true);
    }

    public function TesteImportacaoImagens($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDTesteImportacaoImagens', $parametros), true);
    }

    public function TesteSelecionarImagens($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        return json_decode(parent::autenticacaoWebServices('servicoSDTesteSelecionarImagens', $parametros), true);
    }
}
