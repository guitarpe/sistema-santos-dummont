<?php

namespace Application\Controller\Clientes;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

require_once SYSTEM_PATH . 'vendor/santosdummont/vendor/autoload.php';

class Clientes extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelClientes', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    public function main()
    {
        $dados['titulopagina'] = "Clientes Cadastrados";
        $dados['acaoimportar'] = SITE_URL . "/Clientes/Importar";
        parent::prepararView("Clientes/pag_clientes", $dados);
    }

    function ListaClientes()
    {
        $token = Session::get('token');

        $data = [];
        $params = $_REQUEST;

        $columns = array(
            0 => 'CLI_ID',
            1 => 'NOME_COMPLETO',
            2 => 'DATA_CADASTRO',
            3 => 'EMAIL'
        );

        $parametros['nome'] = null;
        $parametros['data'] = null;
        $parametros['email'] = null;

        if (!empty($params['columns'][1]['search']['value'])) {
            $parametros['nome'] = $params['columns'][1]['search']['value'];
        }

        if (!empty($params['columns'][2]['search']['value'])) {
            $parametros['data'] = $params['columns'][2]['search']['value'];
        }

        if (!empty($params['columns'][3]['search']['value'])) {
            $parametros['email'] = $params['columns'][3]['search']['value'];
        }

        $parametros['token'] = $token;
        $parametros['order'] = $columns[$params['order'][0]['column']];
        $parametros['dir'] = $params['order'][0]['dir'];
        $parametros['start'] = $params['start'];
        $parametros['length'] = $params['length'];

        $list = Common::retornoWSLista($this->model->ListaClientesCadastrados($parametros));

        foreach ($list as $prod) {
            $row = array();
            $row[] = $prod['CLI_ID'];
            $row[] = $prod['NOME_COMPLETO'];
            $row[] = $prod['DATA_CADASTRO'];
            $row[] = $prod['EMAIL'];
            $row[] = intval($prod['CLI_STATUS']) == 1 ? 'Ativo' : 'Inativo';
            $row[] = null;

            $data[] = $row;
        }

        $output = [
            "draw" => intval($params['draw']),
            "recordsTotal" => !empty($list[0]['TOTALGERAL']) ? $list[0]['TOTALGERAL'] : 0,
            "recordsFiltered" => !empty($list[0]['TOTAL']) ? $list[0]['TOTAL'] : 0,
            "data" => $data
        ];

        echo json_encode($output);
    }

    public function Cliente($id = null)
    {
        $token = Session::get('token');

        if (empty($id)) {
            $dados['titulopagina'] = "Novo Cliente";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Clientes/Inserir";
            $dados['cliente'] = null;
        } else {
            $dados['titulopagina'] = "Editar Cliente";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Clientes/Editar";
            $dados['cliente'] = Common::retornoWSLista($this->model->DadosCliente($token, $id))[0];
            $dados['listaenderecos'] = Common::retornoWSLista($this->model->EnderecosCliente($token, $id));
        }

        parent::prepararView("Clientes/cad_cliente", $dados);
    }

    public function EnderecoCliente($cli, $id)
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Editar Endereço";
        $dados['titleaction'] = "Salvar Edição";
        $dados['urlaction'] = SITE_URL . "/Clientes/EditarEndereco";
        $dados['endereco'] = Common::retornoWSLista($this->model->DadosEndereco($token, $cli, $id))[0];
        $dados['cliente'] = Common::retornoWSLista($this->model->DadosCliente($token, $cli))[0];

        parent::prepararView("Clientes/end_cliente", $dados);
    }

    public function Compras($id)
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Compras Realizadas Pelo Cliente";
        $dados['cliente'] = Common::retornoWSLista($this->model->DadosCliente($token, $id))[0];
        $dados['listacompras'] = Common::retornoWSLista($this->model->DadosComprasCliente($token, $id));
        parent::prepararBasicView("Clientes/compras_cliente", $dados);
    }

    public function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');
        $cliente = Common::retornoWSLista($this->model->DadosUsuario($token, $id))[0];

        $dados = [
            'TOKEN' => $token,
            'ClI_ID' => intval($id)
        ];

        $deletar = $this->model->ExcluirCliente($dados);

        if ($deletar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Clientes';
        } else {
            $msg = 'Cliente deletado com sucesso!';
            $situacao = 'success';
            self::removerImagem($cliente['CLI_IMAGEM']);

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Clientes';
        }
    }

    public function Resetar()
    {
        $config = Common::retornoWSLista($this->model->ConfigEnvioEmail())[0];
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'ClI_ID' => $id
        ];

        $retorno = $this->model->ResetarSenha($dados);

        if ($retorno['list']['O_COD_RETORNO'] != 0) {
            $msg = $retorno['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Clientes';
        } else {
            $infomail = [
                'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                'fromname' => $config['NOME_CONTATO_DEFAULT'],
                'url' => $config['URL_SISTEMA'],
                'logotipo' => $config['LOGOTIPO']
            ];

            $html = '<html>
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                            <title>Santos Dumont - Confirmar Registro</title>
                        </head>
                        <body>
                            <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                    <img src="' . URL . PATHSTATIC_URL . '/' . $config['LOGOTIPO'] . '">
                                </div>
                                <div class="container" style="padding:20px">
                                    <table>
                                        <tr>
                                            <td>
                                                <h2><b></b></h2>
                                                <br/>Olá, sua senha foi resetada!
                                                <br/>
                                                Para acessar nossa área do cliente no site, basta confirmar que seu e-mail está correto clicando no link a seguir:
                                                <br/>
                                                <br/>
                                                <a href="' . URL . SITE_URL . '/Usuarios/AtivarRegistro/' . Common::encrypt_decrypt('encrypt', $retorno['list'][0]['O_TOKEN']) . '">Clique aqui</a>
                                                <br/>
                                                <br/>
                                                Boas Compras!
                                                <br/>
                                                <br/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </body>
                    </html>';

            $enviado = Common::dispararEmailPersonalizado('Santos Dumont - Reset de senha', $html, $dados['I_EMAIL'], $infomail);

            if ($enviado) {
                $msg = 'Senha resetada com sucesso!<br/>O cliente receberá um e-mail para gerar uma nova senha.';
                $situacao = 'success';
            } else {
                $msg = 'Ocorreu um erro ao enviar o e-mail!<br/>Entre em contato com o administrador.';
                $situacao = 'danger';
            }

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Clientes';
        }
    }

    public function Inserir()
    {
        $config = Common::retornoWSLista($this->model->ConfigEnvioEmail())[0];
        $this->validarCamposObrigatorio();

        $dados['I_TOKEN'] = Session::get('token');
        $dados['I_CLI_ID'] = 0;
        $dados['I_NOME_COMPLETO'] = filter_input(INPUT_POST, "nome");
        $dados['I_TEL_RESID'] = filter_input(INPUT_POST, "tel_resid");
        $dados['I_TEL_COM'] = filter_input(INPUT_POST, "tel_com");
        $dados['I_TEL_CEL'] = filter_input(INPUT_POST, "tel_cel");
        $dados['I_TIPO_DOC'] = filter_input(INPUT_POST, "tipo_doc");
        $dados['I_NUM_DOC'] = filter_input(INPUT_POST, "cpf");
        $dados['I_GENERO'] = filter_input(INPUT_POST, "genero");
        $dados['I_DT_NASC'] = implode('/', array_reverse(explode('-', filter_input(INPUT_POST, "dt_nasc"))));
        $dados['I_EMAIL'] = filter_input(INPUT_POST, "email");
        $dados['I_OCUPACAO'] = filter_input(INPUT_POST, "ocupacao");
        $submit = filter_input(INPUT_POST, "submit");

        if (isset($submit)) {
            $retorno = $this->model->CadastrarEditarCliente($dados);

            if ($retorno['list'][0]['O_COD_RETORNO'] != 0) {
                $msg = $retorno['list'][0]['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Clientes');
            } else {
                $infomail = [
                    'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                    'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                    'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                    'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                    'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                    'fromname' => $config['NOME_CONTATO_DEFAULT'],
                    'url' => $config['URL_SISTEMA'],
                    'logotipo' => $config['LOGOTIPO']
                ];

                $html = '<html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <title>Santos Dumont - Confirmar Registro</title>
                            </head>
                            <body>
                                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                    <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                        <img src="' . URL . PATHSTATIC_URL . '/' . $config['LOGOTIPO'] . '">
                                    </div>
                                    <div class="container" style="padding:20px">
                                        <table>
                                            <tr>
                                                <td>
                                                    <h2><b></b></h2>
                                                    <br/>Olá, você está prestes a se tornar membro da família Santos Dumont Componentes Eletrônicos!
                                                    <br/>
                                                    Para acessar nossa área do cliente no site, basta confirmar que seu e-mail está correto clicando no link a seguir:
                                                    <br/>
                                                    <br/>
                                                    <a href="' . URL . SITE_URL . '/Usuarios/AtivarRegistro/' . Common::encrypt_decrypt('encrypt', $retorno['list'][0]['O_TOKEN']) . '">Clique aqui</a>
                                                    <br/>
                                                    <br/>
                                                    Boas Compras!
                                                    <br/>
                                                    <br/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>';

                $enviado = Common::dispararEmailPersonalizado('Santos Dumont - Confirmar Registro', $html, $dados['I_EMAIL'], $infomail);

                if ($enviado) {
                    $msg = 'Cliente inserido com sucesso!<br/>O cliente receberá um e-mail para gerar uma nova senha.';
                    $situacao = 'success';
                } else {
                    $msg = 'Ocorreu um erro ao enviar o e-mail!<br/>Entre em contato com o administrador.';
                    $situacao = 'danger';
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Clientes');
            }
        }
    }

    public function Editar()
    {

        $this->validarCamposObrigatorio();

        $dados['I_TOKEN'] = Session::get('acesso');
        $dados['I_CLI_ID'] = filter_input(INPUT_POST, "id");
        $dados['I_NOME_COMPLETO'] = filter_input(INPUT_POST, "nome");
        $dados['I_TEL_RESID'] = filter_input(INPUT_POST, "tel_resid");
        $dados['I_TEL_COM'] = filter_input(INPUT_POST, "tel_com");
        $dados['I_TEL_CEL'] = filter_input(INPUT_POST, "tel_cel");
        $dados['I_TIPO_DOC'] = filter_input(INPUT_POST, "tipo_doc");
        $dados['I_NUM_DOC'] = filter_input(INPUT_POST, "cpf");
        $dados['I_GENERO'] = filter_input(INPUT_POST, "genero");
        $dados['I_DT_NASC'] = implode('/', array_reverse(explode('-', filter_input(INPUT_POST, "dt_nasc"))));
        $dados['I_EMAIL'] = filter_input(INPUT_POST, "email");
        $dados['I_OCUPACAO'] = filter_input(INPUT_POST, "ocupacao");

        $submit = filter_input(INPUT_POST, "submit");

        if (isset($submit)) {
            $retorno = $this->model->CadastrarEditarCliente($dados);

            if ($retorno['list'][0]['O_COD_RETORNO'] != 0) {
                $msg = $retorno['list'][0]['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Clientes');
            } else {
                if ($retorno['list'][0]['O_TROCA_EMAIL'] != 0) {

                    $config = Common::retornoWSLista($this->model->ConfigEnvioEmail())[0];

                    $infomail = [
                        'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                        'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                        'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                        'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                        'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                        'fromname' => $config['NOME_CONTATO_DEFAULT'],
                        'url' => $config['URL_SISTEMA'],
                        'logotipo' => $config['LOGOTIPO']
                    ];

                    $html = '<html>
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                            <title>Santos Dumont - Confirmar Registro</title>
                        </head>
                        <body>
                            <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                    <img src="' . URL . PATHSTATIC_URL . '/' . $config['LOGOTIPO'] . '">
                                </div>
                                <div class="container" style="padding:20px">
                                    <table>
                                        <tr>
                                            <td>
                                                <h2><b></b></h2>
                                                <br/>Olá, seu e-mail foi modificado, dessa forma por medida de segurança resetamos sua senha para que voc~e confirme seu acesso!
                                                <br/>
                                                Para acessar nossa área do cliente no site, basta confirmar que seu e-mail está correto clicando no link a seguir:
                                                <br/>
                                                <br/>
                                                <a href="' . URL . SITE_URL . '/Usuarios/AtivarRegistro/' . Common::encrypt_decrypt('encrypt', $retorno['list'][0]['O_TOKEN']) . '">Clique aqui</a>
                                                <br/>
                                                <br/>
                                                Boas Compras!
                                                <br/>
                                                <br/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </body>
                    </html>';

                    $enviado = Common::dispararEmailPersonalizado('Santos Dumont - Alteração de cadastro', $html, $dados['I_EMAIL'], $infomail);

                    if ($enviado) {
                        $msg = 'Cliente editado com sucesso!<br/>O cliente receberá um e-mail para gerar uma nova senha.';
                        $situacao = 'success';
                    } else {
                        $msg = 'Ocorreu um erro ao enviar o e-mail!<br/>Entre em contato com o administrador.';
                        $situacao = 'danger';
                    }

                    Common::alert($msg, $situacao, 'acao');
                    Common::redir('Clientes');
                } else {
                    $msg = 'Cliente editado com sucesso.';
                    $situacao = 'success';
                    Common::alert($msg, $situacao, 'acao');
                    Common::redir('Clientes');
                }
            }
        }
    }

    public function EditarEndereco()
    {
        $this->validarCamposObrigatorioEnd();

        $dados['I_TOKEN'] = Session::get('acesso');
        $dados['I_CLI_ID'] = filter_input(INPUT_POST, "cliente");
        $dados['I_END_ID'] = filter_input(INPUT_POST, "id");
        $dados['I_END_CEP'] = filter_input(INPUT_POST, "cep");
        $dados['I_END_LOGRADOURO'] = filter_input(INPUT_POST, "logradouro");
        $dados['I_END_BAIRRO'] = filter_input(INPUT_POST, "bairro");
        $dados['I_END_CIDADE'] = filter_input(INPUT_POST, "cidade");
        $dados['I_END_NUMERO'] = filter_input(INPUT_POST, "num");
        $dados['I_END_UF'] = filter_input(INPUT_POST, "uf");
        $dados['I_END_PAIS'] = filter_input(INPUT_POST, "pais");
        $dados['I_END_COMPLEMENTO'] = filter_input(INPUT_POST, "complemento");
        $dados['I_END_TIPO'] = filter_input(INPUT_POST, "tipo");
        $dados['I_END_ENVIAR'] = filter_input(INPUT_POST, "enviar");
        $dados['I_END_STATUS'] = filter_input(INPUT_POST, "status");

        $submit = filter_input(INPUT_POST, "submit");

        if (isset($submit)) {
            $retorno = $this->model->EditarEnderecoCliente($dados);

            if ($retorno['list'][0]['O_COD_RETORNO'] != 0) {
                $msg = $retorno['list'][0]['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Clientes');
            } else {
                $msg = 'Endereço editado com sucesso.';
                $situacao = 'success';
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Clientes');
            }
        }
    }

    function Importar()
    {
        $token = Session::get('token');

        if (empty($_FILES['arquivo_importacao']['name'][0])) {
            $msg = 'É necessário selecionar um arquivo!';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Produtos');
        } else {

            $ret = $this->salvarArquivo($_FILES['arquivo_importacao']);
            $file = $ret['list']['documento'];

            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load(PASTAARQUIVOS_URL . '/' . $file);

            $campos = [];
            $importados = 0;
            $linhasimpo = 0;

            $naoimportados = [];

            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

                $linhas = [];

                foreach ($worksheet->getRowIterator() as $row) {
                    if ($row->getRowIndex() == 1) {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        foreach ($cellIterator as $cell) {
                            if (!is_null($cell)) {
                                array_push($campos, $cell->getCalculatedValue());
                            }
                        }
                    } else {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        $valores = [];
                        foreach ($cellIterator as $cell) {
                            $valor = $cell->getCalculatedValue();
                            array_push($valores, $valor);
                        }

                        if (!empty($valores[0])) {
                            $linhas['TOKEN'] = $token;

                            foreach ($valores as $key => $value) {
                                $linhas[$campos[$key]] = $value;
                            }

                            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarCliente($linhas));

                            if (intval($cadastrar[0]['O_COD_RETORNO']) == 0) {
                                $importados++;
                            } else {
                                array_push($naoimportados, $linhas['NOME_USER']);
                            }
                        }

                        $linhasimpo++;
                    }
                }
            }

            $this->removerArquivo($file);

            if ($importados > 0) {
                $msg = 'Foram importados ' . $importados . ' clientes com sucesso!';

                if (count($naoimportados) > 0) {
                    $msg .= '\nOs clientes ' . implode('\n', $naoimportados) . ' não foram importados.';
                }

                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Clientes');
            } else {
                $msg = 'Ocorreu algum erro ao importar os clientes';
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Clientes');
            }
        }
    }

    function Inativar($id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'CLI_ID' => $id,
            'CLI_STATUS' => 0
        ];

        $status = $this->model->AtivarInativarCliente($parametros);

        if ($status['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $status['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Inativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function Ativar($id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'CLI_ID' => $id,
            'CLI_STATUS' => 1
        ];

        $status = $this->model->AtivarInativarCliente($parametros);

        if ($status['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $status['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Ativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function InativarEndereco($cli, $id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'CLI_ID' => $cli,
            'END_ID' => $id,
            'END_STATUS' => '0'
        ];

        $status = $this->model->AtivarInativarEnderecoCliente($parametros);

        if ($status['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $status['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Inativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function AtivarEndereco($cli, $id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'CLI_ID' => $cli,
            'END_ID' => $id,
            'END_STATUS' => '1'
        ];

        $status = $this->model->AtivarInativarEnderecoCliente($parametros);

        if ($status['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $status['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Ativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function ExcluirEndereco($cli)
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'ClI_ID' => $cli,
            'END_ID' => intval($id)
        ];

        $deletar = $this->model->ExcluirEndCliente($dados);

        if ($deletar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Clientes/Cliente/' . $cli;
        } else {
            $msg = 'Endereço deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Clientes/Cliente/' . $cli;
        }
    }

    public function ValidaDoc()
    {
        $tipo = filter_input(INPUT_POST, "tipo");
        $numdoc = filter_input(INPUT_POST, "numdoc");

        if (intval($tipo) == 1) {
            echo parent::validar_cpf($numdoc);
        } else if (intval($tipo) == 2) {
            echo parent::validar_cnpj($numdoc);
        }
    }

    public function VerificaEmailCliente()
    {
        $dados['I_TOKEN'] = Session::get('token');
        $dados['I_CLI_ID'] = filter_input(INPUT_POST, "id");
        $dados['I_EMAIL'] = filter_input(INPUT_POST, "email");

        $email = Common::retornoWSLista($this->modelsistema->VerificaEmailClienteExiste($dados))[0];

        echo intval($email['O_EXISTE']);
    }

    public function VerificaCPFCliente()
    {
        $dados['I_TOKEN'] = Session::get('token');
        $dados['I_CLI_ID'] = filter_input(INPUT_POST, "id");
        $dados['I_CPF'] = filter_input(INPUT_POST, "cpf");

        $cpf = Common::retornoWSLista($this->modelsistema->VerificaCPFClienteExiste($dados))[0];

        echo intval($cpf['O_EXISTE']);
    }

    private function validarCamposObrigatorio()
    {
        $tipodoc = filter_input(INPUT_POST, "tipo_doc");
        $dados['Nome Completo'] = filter_input(INPUT_POST, "nome");
        $dados['Telefone Celular'] = filter_input(INPUT_POST, "tel_cel");
        $dados['E-Mail'] = filter_input(INPUT_POST, "email");
        if ($tipodoc == 'CPF') {
            $dados['CPF'] = filter_input(INPUT_POST, "cpf");
            $dados['Gênero'] = filter_input(INPUT_POST, "genero");
        } else {
            $dados['CNPJ'] = filter_input(INPUT_POST, "cpf");
        }
    }

    private function validarCamposObrigatorioEnd()
    {
        $dados['Cep'] = filter_input(INPUT_POST, "cep");
        $dados['Logradouro'] = filter_input(INPUT_POST, "logradouro");
        $dados['Bairro'] = filter_input(INPUT_POST, "bairro");
        $dados['Cidade'] = filter_input(INPUT_POST, "cidade");
        $dados['Número'] = filter_input(INPUT_POST, "num");
        $dados['UF'] = filter_input(INPUT_POST, "uf");
    }

    private function salvarArquivo($arquivo)
    {
        $arquivos = new \Application\Controller\Common\Arquivos;

        return $arquivos->executar(PASTAARQUIVOS_URL, $arquivo);
    }

    private function removerArquivo($nome_arquivo)
    {
        $arquivos = new \Application\Controller\Common\Arquivos;

        $arquivos->deletarDocumentoDiretorio(PASTAARQUIVOS_URL, $nome_arquivo);
    }

    private function removerImagem($nome_imagem)
    {
        $caminho_imagem = FOTOCLIENTES_URL . "/" . $nome_imagem;
        if (file_exists($caminho_imagem)) {
            @unlink($caminho_imagem);
        }
        clearstatcache(TRUE, $caminho_imagem);
    }
}
