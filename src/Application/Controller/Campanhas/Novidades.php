<?php

namespace Application\Controller\Campanhas;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Novidades extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCampanhas', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Campanhas de Novidades Cadastradas";
        $dados['listanovidades'] = Common::retornoWSLista($this->model->ListaNovidadesCadastrados($token));
        parent::prepararView("Campanhas/pag_novidades", $dados);
    }

    function Novidade($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['novidade'] = Common::retornoWSLista($this->model->DadosNovidade($token, $id))[0];
            $dados['listaprodnovidade'] = Common::retornoWSLista($this->model->ListaProdutosNovidade($token, $id));
            $dados['urlaction'] = SITE_URL . "/Novidades/Editar";
            $dados['titulopagina'] = "Editar Campanha - Novidades";
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Novidades/Inserir";
            $dados['titulopagina'] = "Nova Campanha - Novidades";
        }
        $dados['urlbusca'] = SITE_URL . "/Novidades/Pesquisar";
        parent::prepararView("Campanhas/cad_novidades", $dados);
    }

    public function Pesquisar()
    {
        $token = Session::get('token');
        $tipo = filter_input(INPUT_POST, "tipo");
        $info = filter_input(INPUT_POST, "pesquisar");

        $dados = [
            'TOKEN' => $token,
            'TIPO' => $tipo,
            'INFO' => $info
        ];

        $pesquisa = $this->model->PesquisaNov($dados);

        echo json_encode(Common::retornoWSLista($pesquisa));
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $tipo = filter_input(INPUT_POST, "tipo");
        $data_ini = filter_input(INPUT_POST, "data_ini");
        $data_fim = filter_input(INPUT_POST, "data_fim");
        $status = filter_input(INPUT_POST, "status");
        $selecionados = filter_input(INPUT_POST, 'selecionados', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (isset($submit)) {

            $dados = [
                'TOKEN' => Session::get('token'),
                'NOV_ID' => 0,
                'NOV_DESCRICAO' => $descricao,
                'NOV_TIPO' => $tipo,
                'NOV_DATA_INI' => $data_ini,
                'NOV_DATA_FIM' => $data_fim,
                'NOV_STATUS' => $status,
                'NOV_SELECIONADOS' => implode(',', $selecionados)
            ];

            $cadastrar = $this->model->CadastrarEditarNovidade($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Novidades');
            } else {
                $msg = 'Novidade cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Novidades');
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $tipo = filter_input(INPUT_POST, "tipo");
        $data_ini = filter_input(INPUT_POST, "data_ini");
        $data_fim = filter_input(INPUT_POST, "data_fim");
        $status = filter_input(INPUT_POST, "status");
        $selecionados = filter_input(INPUT_POST, 'selecionados', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (isset($submit)) {

            $dados = [
                'TOKEN' => Session::get('token'),
                'NOV_ID' => $id,
                'NOV_DESCRICAO' => $descricao,
                'NOV_TIPO' => $tipo,
                'NOV_DATA_INI' => $data_ini,
                'NOV_DATA_FIM' => $data_fim,
                'NOV_STATUS' => $status,
                'NOV_SELECIONADOS' => implode(',', $selecionados)
            ];

            $cadastrar = $this->model->CadastrarEditarNovidade($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Novidades');
            } else {
                $msg = 'Novidade editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Novidades');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'NOV_ID' => $id
        ];

        $deletar = $this->model->ExcluirNovidade($dados);

        if ($deletar['list']['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Novidades';
        } else {
            $msg = 'Novidade deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Novidades';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {
        $dados['Descrição'] = filter_input(INPUT_POST, "descricao");
        $dados['Tipo'] = filter_input(INPUT_POST, "tipo");
        $dados['Data de Início'] = filter_input(INPUT_POST, "data_ini");
        $dados['Data Final'] = filter_input(INPUT_POST, "data_fim");
        $dados['Status'] = filter_input(INPUT_POST, "status");
        $dados['Selecionados'] = filter_input(INPUT_POST, 'selecionados', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (count($dados['Selecionados']) > 1) {
            unset($dados['Selecionados']);
        }

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Novidades/Novidade/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Novidades/Novidade');
        }
    }
}
