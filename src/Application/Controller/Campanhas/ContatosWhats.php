<?php
namespace Application\Controller\Campanhas;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class ContatosWhats extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCampanhas', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = 'Conatatos Registrados - Whatsapp';
        $dados['listaemails'] = Common::retornoWSLista($this->model->ListaContatoWhats($token));
        parent::prepararView("Campanhas/pag_whats", $dados);
    }

    function Excluir()
    {

    }
}
