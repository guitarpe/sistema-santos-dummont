<?php

namespace Application\Controller\Campanhas;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Promocoes extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCampanhas', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Campanhas de Promoções Cadastradas";
        $dados['listapromocoes'] = Common::retornoWSLista($this->model->ListaPromocoesCadastrados($token));
        parent::prepararView("Campanhas/pag_promocoes", $dados);
    }

    function Promocao($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['promocao'] = Common::retornoWSLista($this->model->DadosPromocao($token, $id))[0];
            $dados['listaprodpromocao'] = Common::retornoWSLista($this->model->ListaProdutosPromocao($token, $id));
            $dados['urlaction'] = SITE_URL . "/Promocoes/Editar";
            $dados['titulopagina'] = "Editar Campanha - Promoções";
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Promocoes/Inserir";
            $dados['titulopagina'] = "Nova Campanha - Promoções";
        }
        $dados['urlbusca'] = SITE_URL . "/Promocoes/Pesquisar";
        parent::prepararView("Campanhas/cad_promocoes", $dados);
    }

    public function Pesquisar()
    {
        $token = Session::get('token');
        $tipo = filter_input(INPUT_POST, "tipo");
        $info = filter_input(INPUT_POST, "pesquisar");

        $dados = [
            'TOKEN' => $token,
            'TIPO' => $tipo,
            'INFO' => $info
        ];

        $pesquisa = $this->model->PesquisaPro($dados);

        echo json_encode(Common::retornoWSLista($pesquisa));
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $tipo = filter_input(INPUT_POST, "tipo");
        $data_ini = filter_input(INPUT_POST, "data_ini");
        $data_fim = filter_input(INPUT_POST, "data_fim");
        $status = filter_input(INPUT_POST, "status");
        $selecionados = filter_input(INPUT_POST, 'selecionados', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (isset($submit)) {

            $dados = [
                'TOKEN' => Session::get('token'),
                'PRO_ID' => 0,
                'PRO_DESCRICAO' => $descricao,
                'PRO_TIPO' => $tipo,
                'PRO_DATA_INI' => $data_ini,
                'PRO_DATA_FIM' => $data_fim,
                'PRO_STATUS' => $status,
                'PRO_SELECIONADOS' => implode(',', $selecionados)
            ];

            $cadastrar = $this->model->CadastrarEditarPromocao($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Promocoes');
            } else {
                $msg = 'Promoção cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Promocoes');
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $tipo = filter_input(INPUT_POST, "tipo");
        $data_ini = filter_input(INPUT_POST, "data_ini");
        $data_fim = filter_input(INPUT_POST, "data_fim");
        $status = filter_input(INPUT_POST, "status");
        $selecionados = filter_input(INPUT_POST, 'selecionados', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (isset($submit)) {

            $dados = [
                'TOKEN' => Session::get('token'),
                'PRO_ID' => $id,
                'PRO_DESCRICAO' => $descricao,
                'PRO_TIPO' => $tipo,
                'PRO_DATA_INI' => $data_ini,
                'PRO_DATA_FIM' => $data_fim,
                'PRO_STATUS' => $status,
                'PRO_SELECIONADOS' => implode(',', $selecionados)
            ];

            $cadastrar = $this->model->CadastrarEditarPromocao($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Promocoes');
            } else {
                $msg = 'Promoção editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Promocoes');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'PRO_ID' => $id
        ];

        $deletar = $this->model->ExcluirPromocao($dados);

        if ($deletar['list']['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Promocoes';
        } else {
            $msg = 'Promoção deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Promocoes';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {
        $dados['Descrição'] = filter_input(INPUT_POST, "descricao");
        $dados['Tipo'] = filter_input(INPUT_POST, "tipo");
        $dados['Data de Início'] = filter_input(INPUT_POST, "data_ini");
        $dados['Data Final'] = filter_input(INPUT_POST, "data_fim");
        $dados['Status'] = filter_input(INPUT_POST, "status");
        $dados['Selecionados'] = filter_input(INPUT_POST, 'selecionados', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (count($dados['Selecionados']) > 1) {
            unset($dados['Selecionados']);
        }

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Promocoes/Promocao/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Promocoes/Promocao');
        }
    }
}
