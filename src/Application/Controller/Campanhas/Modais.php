<?php

namespace Application\Controller\Campanhas;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Modais extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCampanhas', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Modais Promocionais Cadastrados";
        $dados['listamodais'] = Common::retornoWSLista($this->model->ListaModaisCadastrados($token));
        parent::prepararView("Campanhas/pag_modais", $dados);
    }

    function Modal($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['modal'] = Common::retornoWSLista($this->model->DadosModal($token, $id))[0];
            $dados['urlaction'] = SITE_URL . "/Modais/Editar";
            $dados['titulopagina'] = "Editar Modal - Promoções";
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Modais/Inserir";
            $dados['titulopagina'] = "Novo Modal - Promoções";
        }
        parent::prepararView("Campanhas/cad_modais", $dados);
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $link = filter_input(INPUT_POST, "link");
        $data_ini = filter_input(INPUT_POST, "data_ini");
        $data_fim = filter_input(INPUT_POST, "data_fim");
        $status = filter_input(INPUT_POST, "status");

        $now = new \DateTime();

        $pasta = $now->format('Y_m_d_H_i_s');

        if (isset($submit)) {

            $imagens = [];

            if (!empty($_FILES['imagem']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 700, 'altura' => 500);

                $ret = Common::salvarImagem($_FILES['imagem'], MODAIS_URL . '/' .$pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'MOD_ID' => 0,
                'MOD_DESCRICAO' => $descricao,
                'MOD_LINK' => $link,
                'MOD_DATA_INI' => $data_ini,
                'MOD_DATA_FIM' => $data_fim,
                'MOD_STATUS' => $status,
                'MOD_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : null,
                'MOD_PASTA_IMAGENS' => $pasta
            ];

            $cadastrar = $this->model->CadastrarEditarModal($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Modais');
            } else {
                $msg = 'Modal cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Modais');
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");

        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $link = filter_input(INPUT_POST, "link");
        $data_ini = filter_input(INPUT_POST, "data_ini");
        $data_fim = filter_input(INPUT_POST, "data_fim");
        $status = filter_input(INPUT_POST, "status");

        $now = new \DateTime();

        $token = Session::get('token');
        $modal = Common::retornoWSLista($this->model->DadosModal($token, $id))[0];

        $this->validarCamposObrigatorio($id, $modal['MOD_IMAGEM']);

        $pasta = empty($modal['PASTA_IMAGENS']) ? $now->format('Y_m_d_H_i_s') : $modal['PASTA_IMAGENS'];

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['imagem']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 700, 'altura' => 500);

                $ret = Common::salvarImagem($_FILES['imagem'], MODAIS_URL . '/' .$pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'MOD_ID' => $id,
                'MOD_DESCRICAO' => $descricao,
                'MOD_LINK' => $link,
                'MOD_DATA_INI' => $data_ini,
                'MOD_DATA_FIM' => $data_fim,
                'MOD_STATUS' => $status,
                'MOD_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : $modal['MOD_IMAGEM'],
                'MOD_PASTA_IMAGENS' => $pasta
            ];

            $cadastrar = $this->model->CadastrarEditarModal($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Modais');
            } else {
                $msg = 'Modal editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Modais');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'MOD_ID' => $id
        ];

        $modal = Common::retornoWSLista($this->model->DadosBanner($token, $id))[0];
        Common::removerImagem($modal['IMAGEM']);

        //remove a pasta
        Common::remoteDir(MODAIS_URL . '/' .$modal['PASTA_IMAGENS']);

        $deletar = $this->model->ExcluirModal($dados);

        if ($deletar['list']['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Modais';
        } else {
            $msg = 'Modal deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Modais';
        }
    }

    private function validarCamposObrigatorio($id = null, $img = null)
    {
        $dados['Descrição'] = filter_input(INPUT_POST, "descricao");
        $dados['Link'] = filter_input(INPUT_POST, "link");
        $dados['Data de Início'] = filter_input(INPUT_POST, "data_ini");
        $dados['Data Final'] = filter_input(INPUT_POST, "data_fim");
        $dados['Status'] = filter_input(INPUT_POST, "status");

        if (empty($id)) {
            $dados['Imagem'] = $_FILES['imagem']['name'];
        } else {
            if (empty($img)) {
                $dados['Imagem'] = $_FILES['imagem']['name'];
            }
        }

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Modais/Modal/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Modais/Modal');
        }
    }
}
