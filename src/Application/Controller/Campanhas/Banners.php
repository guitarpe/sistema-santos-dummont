<?php

namespace Application\Controller\Campanhas;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Banners extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCampanhas', 'model');
        parent::loadModel('Application\Model\ModelConfig', 'modelconfig');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Banners Cadastrados";
        $dados['listabanners'] = Common::retornoWSLista($this->model->ListaBannersCadastrados($token));
        $dados['urlactioncfgbanner'] = SITE_URL . "/Banners/ConfigBanner";
        Session::delete('id-banner');
        parent::prepararView("Campanhas/pag_banners", $dados);
    }

    function Banner($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();
        //$dados['config'] = Common::retornoWSLista($this->modelconfig->ConfiguracoesADM($token))[0];

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['banner'] = Common::retornoWSLista($this->model->DadosBanner($token, $id))[0];
            $dados['urlaction'] = SITE_URL . "/Banners/Editar";
            $dados['titulopagina'] = "Editar Banner";
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Banners/Inserir";
            $dados['titulopagina'] = "Novo Banner";
        }

        parent::prepararView("Campanhas/cad_banners", $dados);
    }

    function ConfigBanner()
    {
        $submit = filter_input(INPUT_POST, "submit");
        $id = filter_input(INPUT_POST, "id");
        //$direcao = filter_input(INPUT_POST, "direcao");
        $tempo = filter_input(INPUT_POST, "tempo");

        if (isset($submit)) {
            $dados = [
                'TOKEN' => Session::get('token'),
                'ID' => $id,
                'BANNER_SENTIDO' => 0, //$direcao,
                'BANNER_TEMPO' => $tempo
            ];

            $editar = Common::retornoWSLista($this->model->EditarConfigBanners($dados));

            if ($editar[0]['O_COD_RETORNO'] != 0) {
                $msg = $editar[0]['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Banners');
            } else {
                $msg = 'Configurações salvas com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Banners');
            }
        }
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();
        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $link = filter_input(INPUT_POST, "link");
        $local_link = filter_input(INPUT_POST, "local_link");
        $posicao_textos = filter_input(INPUT_POST, "posicao_textos");
        $texto_principal = filter_input(INPUT_POST, "texto_principal");
        $efeito_texto_principal = filter_input(INPUT_POST, "efeito_texto_principal");
        $cor_texto_principal = filter_input(INPUT_POST, "cor_texto_principal");
        $segundo_texto = filter_input(INPUT_POST, "segundo_texto");
        $efeito_segundo_texto = filter_input(INPUT_POST, "efeito_segundo_texto");
        $cor_segundo_texto = filter_input(INPUT_POST, "cor_segundo_texto");
        $terceiro_texto = filter_input(INPUT_POST, "terceiro_texto");
        $efeito_terceiro_texto = filter_input(INPUT_POST, "efeito_terceiro_texto");
        $cor_terceiro_texto = filter_input(INPUT_POST, "cor_terceiro_texto");
        $status = filter_input(INPUT_POST, "status");
        $bttext = "teste";//filter_input(INPUT_POST, "bttext");
        $btcor = "#fff";//filter_input(INPUT_POST, "btcor");

        if (isset($submit)) {
            $ret = [];

            $confg1 = [
                'tamanho' => 49999999,
                'largura' => 2100,
                'altura' => 450
            ];

            $confg2 = [
                'tamanho' => 49999999,
                'largura' => 1024,
                'altura' => 315
            ];

            $confg3 = [
                'tamanho' => 49999999,
                'largura' => 375,
                'altura' => 160
            ];

            $valida1 = Common::validarImagem($_FILES['banner'], $confg1);
            $valida2 = Common::validarImagem($_FILES['banner_md'], $confg2);
            $valida3 = Common::validarImagem($_FILES['banner_sm'], $confg3);

            if (!empty($valida1)) {
                $ret['RET_COD'] = 1;
                $ret['RET_MSG'] = $valida1;
            }

            if (!empty($valida2)) {
                $ret['RET_COD'] = 1;
                $ret['RET_MSG'] = $valida2;
            }

            if (!empty($valida3)) {
                $ret['RET_COD'] = 1;
                $ret['RET_MSG'] = $valida3;
            }

            if (empty($ret)) {
                $now = new \DateTime();
                $pasta = $now->format('Y_m_d_H_i_s');
                $imagens = [];

                $ret1 = Common::salvarImagem($_FILES['banner'], BANNERS_URL.'/'.$pasta, $confg1);
                $ret2 = Common::salvarImagem($_FILES['banner_md'], BANNERS_URL.'/'.$pasta, $confg2);
                $ret3 = Common::salvarImagem($_FILES['banner_sm'], BANNERS_URL.'/'.$pasta, $confg3);

                //imagens
                if(!$ret1['erro']){
                    array_push($imagens, $pasta . '/' . $ret1['list']['imagem']);
                }

                if(!$ret2['erro']){
                    array_push($imagens, $pasta . '/' . $ret2['list']['imagem']);
                }

                if(!$ret3['erro']){
                    array_push($imagens, $pasta . '/' . $ret3['list']['imagem']);
                }

                $dados = [
                    'TOKEN' => Session::get('token'),
                    'BAN_ID' => 0,
                    'BAN_DESCRICAO' => $descricao,
                    'LINK' => $link,
                    'LOCAL_LINK' => $local_link,
                    'BAN_POS_TEXTOS' => $posicao_textos,
                    'BAN_TEXTO_PRINCIPAL' => $texto_principal,
                    'BAN_EFEITO_TXT_PRINC' => $efeito_texto_principal,
                    'BAN_COR_TXT_PRI' => $cor_texto_principal,
                    'BAN_SEG_TEXTO' => $segundo_texto,
                    'BAN_EFEITO_SEG_TEXTO' => $efeito_segundo_texto,
                    'BAN_COR_TXT_SEC' => $cor_segundo_texto,
                    'BAN_TER_TEXTO' => $terceiro_texto,
                    'BAN_EFEITO_TER_TEXTO' => $efeito_terceiro_texto,
                    'BAN_COR_TXT_TER' => $cor_terceiro_texto,
                    'BAN_STATUS' => $status,
                    'BAN_TB_COR' => $btcor,
                    'BAN_TB_TEXTO' => $bttext,
                    'IMAGEM' => implode(',', $imagens),
                    'PASTA_IMAGENS' => $pasta,
                ];

                $cadastrar = $this->model->CadastrarEditarBanner($dados);

                if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
                    $msg = $cadastrar['list'][0]['O_DESC_CURTO'];
                    $ret['RET_COD'] = $cadastrar['list'][0]['O_COD_RETORNO'];
                    $ret['RET_MSG'] = $msg;
                    $ret['refresh'] = 1;
                    $ret['banner_1'] = '';
                    $ret['banner_2'] = '';
                    $ret['banner_3'] = '';
                } else {
                    $msg = 'Banner cadastrado com sucesso!';
                    $ret['RET_COD'] = $cadastrar['list'][0]['O_COD_RETORNO'];
                    $ret['RET_MSG'] = $msg;
                    $ret['refresh'] = 1;
                    $ret['banner_1'] = SITE_URL . '/public_html/static/banners/' . $imagens[0];
                    $ret['banner_2'] = SITE_URL . '/public_html/static/banners/' . $imagens[1];
                    $ret['banner_3'] = SITE_URL . '/public_html/static/banners/' . $imagens[2];
                }
            }

            echo json_encode($ret);
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $link = filter_input(INPUT_POST, "link");
        $local_link = filter_input(INPUT_POST, "local_link");
        $posicao_textos = filter_input(INPUT_POST, "posicao_textos");
        $texto_principal = filter_input(INPUT_POST, "texto_principal");
        $efeito_texto_principal = filter_input(INPUT_POST, "efeito_texto_principal");
        $cor_texto_principal = filter_input(INPUT_POST, "cor_texto_principal");
        $segundo_texto = filter_input(INPUT_POST, "segundo_texto");
        $efeito_segundo_texto = filter_input(INPUT_POST, "efeito_segundo_texto");
        $cor_segundo_texto = filter_input(INPUT_POST, "cor_segundo_texto");
        $terceiro_texto = filter_input(INPUT_POST, "terceiro_texto");
        $efeito_terceiro_texto = filter_input(INPUT_POST, "efeito_terceiro_texto");
        $cor_terceiro_texto = filter_input(INPUT_POST, "cor_terceiro_texto");
        $status = filter_input(INPUT_POST, "status");
        $bttext = "teste";//filter_input(INPUT_POST, "bttext");
        $btcor = "#fff";//filter_input(INPUT_POST, "btcor");

        $now = new \DateTime();

        $banner = Common::retornoWSLista($this->model->DadosBanner($token, $id))[0];
        $pasta = !empty($banner['PASTA_IMAGENS']) ? $banner['PASTA_IMAGENS'] : $now->format('Y_m_d_H_i_s');
        $imagensbanco = explode(',', $banner['IMAGEM']);

        if (isset($submit)) {
            $ret = [];
            $imagens = [];

            $confg1 = [
                'tamanho' => 49999999,
                'largura' => 2100,
                'altura' => 450
            ];

            $confg2 = [
                'tamanho' => 49999999,
                'largura' => 1024,
                'altura' => 315
            ];

            $confg3 = [
                'tamanho' => 49999999,
                'largura' => 375,
                'altura' => 160
            ];

            if (intval(filter_input(INPUT_POST, "ban1op")) == 1) {
                $valida1 = Common::validarImagem($_FILES['banner'], $confg1);
                if (!empty($valida1)) {
                    $ret['RET_COD'] = 1;
                    $ret['RET_MSG'] = $valida1;
                } else {
                    array_push($imagens, $pasta . '/' . $_FILES['banner']['name']);
                    Common::removerImagem(BANNERS_URL.'/'.$imagensbanco[0]);
                }
            } else {
                array_push($imagens, $imagensbanco[0]);
            }

            if (intval(filter_input(INPUT_POST, "ban2op")) == 1) {
                $valida2 = Common::validarImagem($_FILES['banner_md'], $confg2);
                if (!empty($valida2)) {
                    $ret['RET_COD'] = 1;
                    $ret['RET_MSG'] = $valida2;
                } else {
                    array_push($imagens, $pasta . '/' . $_FILES['banner_md']['name']);
                    Common::removerImagem(BANNERS_URL.'/'.$imagensbanco[1]);
                }
            } else {
                array_push($imagens, $imagensbanco[1]);
            }

            if (intval(filter_input(INPUT_POST, "ban3op")) == 1) {
                $valida3 = Common::validarImagem($_FILES['banner_sm'], $confg3);

                if (!empty($valida3)) {
                    $ret['RET_COD'] = 1;
                    $ret['RET_MSG'] = $valida3;
                } else {
                    array_push($imagens, $pasta . '/' . $_FILES['banner_sm']['name']);
                    Common::removerImagem(BANNERS_URL.'/'.$imagensbanco[2]);
                }
            } else {
                array_push($imagens, $imagensbanco[2]);
            }

            if (empty($ret)) {
                $dados = [
                    'TOKEN' => $token,
                    'BAN_ID' => $id,
                    'BAN_DESCRICAO' => $descricao,
                    'LINK' => $link,
                    'LOCAL_LINK' => $local_link,
                    'BAN_POS_TEXTOS' => $posicao_textos,
                    'BAN_TEXTO_PRINCIPAL' => $texto_principal,
                    'BAN_EFEITO_TXT_PRINC' => $efeito_texto_principal,
                    'BAN_COR_TXT_PRI' => $cor_texto_principal,
                    'BAN_SEG_TEXTO' => $segundo_texto,
                    'BAN_EFEITO_SEG_TEXTO' => $efeito_segundo_texto,
                    'BAN_COR_TXT_SEC' => $cor_segundo_texto,
                    'BAN_TER_TEXTO' => $terceiro_texto,
                    'BAN_EFEITO_TER_TEXTO' => $efeito_terceiro_texto,
                    'BAN_COR_TXT_TER' => $cor_terceiro_texto,
                    'BAN_STATUS' => $status,
                    'BAN_BT_TEXTO' => $bttext,
                    'BAN_BT_COR' => $btcor,
                    'IMAGEM' => implode(',', $imagens),
                    'PASTA_IMAGENS' => $pasta,
                ];

                $cadastrar = $this->model->CadastrarEditarBanner($dados);

                if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
                    $msg = $cadastrar['list'][0]['O_DESC_CURTO'];
                    $ret['RET_COD'] = $cadastrar['list'][0]['O_COD_RETORNO'];
                    $ret['RET_MSG'] = $msg;
                    $ret['refresh'] = 1;
                    $ret['banner_1'] = '';
                    $ret['banner_2'] = '';
                    $ret['banner_3'] = '';
                } else {
                    if (intval(filter_input(INPUT_POST, "ban1op")) == 1) {
                        Common::salvarImagem($_FILES['banner'], BANNERS_URL.'/'.$pasta, $confg1);
                    }
                    if (intval(filter_input(INPUT_POST, "ban2op")) == 1) {
                        Common::salvarImagem($_FILES['banner_md'], BANNERS_URL.'/'.$pasta, $confg2);
                    }
                    if (intval(filter_input(INPUT_POST, "ban3op")) == 1) {
                        Common::salvarImagem($_FILES['banner_sm'], BANNERS_URL.'/'.$pasta, $confg3);
                    }

                    $msg = 'Banner editado com sucesso!';
                    $ret['RET_COD'] = $cadastrar['list'][0]['O_COD_RETORNO'];
                    $ret['RET_MSG'] = $msg;
                    $ret['refresh'] = 1;
                    $ret['banner_1'] = SITE_URL . '/public_html/static/banners/' . $imagens[0];
                    $ret['banner_2'] = SITE_URL . '/public_html/static/banners/' . $imagens[1];
                    $ret['banner_3'] = SITE_URL . '/public_html/static/banners/' . $imagens[2];
                }
            }

            echo json_encode($ret);
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'BAN_ID' => $id
        ];

        $banner = Common::retornoWSLista($this->model->DadosBanner($token, $id))[0];
        $imagensbanco = explode(',', $banner['IMAGEM']);
        Common::removerImagem(BANNERS_URL.'/'.$imagensbanco[0]);
        Common::removerImagem(BANNERS_URL.'/'.$imagensbanco[1]);
        Common::removerImagem(BANNERS_URL.'/'.$imagensbanco[2]);

        $deletar = $this->model->ExcluirBanner($dados);

        if ($deletar['list']['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Banners';
        } else {
            $msg = 'Banner deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Banners';
        }
    }

    function Inativar($id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'BAN_ID' => $id,
            'BAN_STATUS' => 0
        ];

        $status = $this->model->AtivarInativarBanner($parametros);
        if ($status['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $status['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Inativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function Ativar($id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'BAN_ID' => $id,
            'BAN_STATUS' => 1
        ];

        $status = $this->model->AtivarInativarBanner($parametros);

        if ($status['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $status['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Ativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    private function validarCamposObrigatorio($id = null, $img = null)
    {
        $dados['Descrição'] = filter_input(INPUT_POST, "descricao");

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Banners/Banner/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Banners/Banner');
        }
    }
}
