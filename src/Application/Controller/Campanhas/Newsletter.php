<?php
namespace Application\Controller\Campanhas;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Newsletter extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCampanhas', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = 'E-mails Registrados - Newsletter';
        $dados['listaemails'] = Common::retornoWSLista($this->model->ListaEmailsNewsletter($token));
        parent::prepararView("Campanhas/pag_newsletter", $dados);
    }

    function Enviar()
    {

    }

    function Excluir()
    {

    }
}
