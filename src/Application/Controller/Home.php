<?php

namespace Application\Controller;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

require_once SYSTEM_PATH . 'vendor/santosdummont/vendor/autoload.php';

class Home extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelHome', 'model');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = 'Sistema Administrativo ';

        $analytics = self::initializeAnalytics();
        $profile = self::getFirstProfileId($analytics);
        $results = self::getResults($analytics, $profile);

        $dados['analytics'] = self::printResults($results);

        $home = $this->model->DadosHome($token);

        if ($home['list'][0]['O_COD_RETORNO'] != 0) {
            $dados['numusuarios'] = 0;
            $dados['numclientes'] = 0;
            $dados['numpedidos'] = 0;
        } else {
            $dados['numusuarios'] = $home['list'][0]['O_NUM_USUARIOS'];
            $dados['numclientes'] = $home['list'][0]['O_NUM_CLIENTES'];
            $dados['numpedidos'] = $home['list'][0]['O_NUM_PEDIDOS'];
        }

        $dados['visitasdia'] = !empty($dados['analytics']['visitas']) ? $dados['analytics']['visitas'] : 0;
        $dados['pageviewsdia'] = !empty($dados['analytics']['pageviews']) ? $dados['analytics']['pageviews'] : 0;
        $dados['visitasmes'] = !empty($dados['analytics']['visitas']) ? $dados['analytics']['visitas'] : 0;
        $dados['pageviewsmes'] = !empty($dados['analytics']['pageviews']) ? $dados['analytics']['pageviews'] : 0;

        parent::prepararView("Home/pag_home", $dados);
    }

    function Sair()
    {
        Session::destroy();
        Common::redir('Login');
    }

    function MenuClicado()
    {
        $menuclicado = filter_input(INPUT_POST, "id");

        if (!empty($menuclicado)) {
            Session::set("selecionado", $menuclicado);
        }
    }

    private function initializeAnalytics()
    {
        $KEY_FILE_LOCATION = __DIR__ . '/Common/admin-santos-dumont-433319e586f0.json';

        $client = new \Google_Client();
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new \Google_Service_Analytics($client);

        return $analytics;
    }

    private function getFirstProfileId($analytics)
    {
        $accounts = $analytics->management_accounts->listManagementAccounts();

        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();

            $properties = $analytics->management_webproperties
                ->listManagementWebproperties($firstAccountId);

            if (count($properties->getItems()) > 0) {
                $items = $properties->getItems();
                $firstPropertyId = $items[0]->getId();

                $profiles = $analytics->management_profiles
                    ->listManagementProfiles($firstAccountId, $firstPropertyId);

                if (count($profiles->getItems()) > 0) {
                    $items = $profiles->getItems();

                    return $items[0]->getId();
                } else {
                    throw new \Exception('No views (profiles) found for this user.');
                }
            } else {
                throw new \Exception('No properties found for this user.');
            }
        } else {
            throw new \Exception('No accounts found for this user.');
        }
    }

    private function getResults($analytics, $profileId)
    {
        return $analytics->data_ga->get('ga:' . $profileId, '7daysAgo', 'today', 'ga:sessions,ga:pageviews,ga:visitors');
    }

    private function printResults($results)
    {
        if (count($results->getRows()) > 0) {
            $rows = $results->getRows();
            $sessions = $rows[0][0];
            $pageviews = $rows[0][1];
            $visitas = $rows[0][2];

            $dados['sessoes'] = $sessions;
            $dados['visitas'] = $visitas;
            $dados['pageviews'] = $pageviews;

            return $dados;
        } else {
            return null;
        }
    }

    public function NotificacoesSistema()
    {

        $token = Session::get('token');

        $notificacoes = $this->model->getNotificacoes($token);
        $dados['O_COD_RETORNO'] = $notificacoes['list'][0]['O_COD_RETORNO'];

        if ($notificacoes['list'][0]['O_COD_RETORNO'] != 0) {
            $dados['O_CNT_IN_CLI'] = 0;
            $dados['O_CNT_IN_PED'] = 0;
            $dados['O_CNT_OUT_PED'] = 0;
        } else {
            $dados['O_CNT_IN_CLI'] = $notificacoes['list'][0]['O_CNT_IN_CLI'];
            $dados['O_CNT_IN_PED'] = $notificacoes['list'][0]['O_CNT_IN_PED'];
            $dados['O_CNT_OUT_PED'] = $notificacoes['list'][0]['O_CNT_OUT_PED'];

            if (intval($dados['O_CNT_IN_CLI']) > 0) {
                self::enviarEmailNotificacao(1);
            }

            if (intval($dados['O_CNT_IN_PED']) > 0) {
                self::enviarEmailNotificacao(2);
            }

            if (intval($dados['O_CNT_OUT_PED']) > 0) {
                self::enviarEmailNotificacao(3);
            }
        }

        echo json_encode($dados);
    }

    private function enviarEmailNotificacao($tipo)
    {
        $token = Session::get('token');
        $config = Common::retornoWSLista($this->model->ConfigEnvioEmail())[0];

        $info = array(
            'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
            'fromname' => $config['NOME_CONTATO_DEFAULT'],
            'url' => $config['URL_SISTEMA'],
            'logotipo' => $config['LOGOTIPO']
        );

        $email = $config['EMAIL_REC_NOTIFICACOES'];

        if ($tipo == 1) {
            $assunto = 'Santos Dumont - Novo Cliente Cadastrado';
            $sub = 'cliente cadastrado';
            $op = '/Clientes';
        } elseif ($tipo == 2) {
            $assunto = 'Santos Dumont - Novo Pedido Cadastrado';
            $sub = 'pedido cadastrado';
            $op = '/Vendas/Pedidos';
        } else {
            $assunto = 'Santos Dumont - Pedido Cancelado';
            $sub = 'pedido cancelado';
            $op = '/Clientes';
        }

        $html = '<html>
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                            <title>' . $assunto . '</title>
                        </head>
                        <body>
                            <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                    <img src="' . URL_IMG_MAIL . '/' . $config['LOGOTIPO'] . '">
                                </div>
                                <div class="container" style="padding:20px">
                                    <table>
                                        <tr>
                                            <td>
                                                <br/>Para verificar o ' . $sub . ' basta acesar o painel administrativo
                                                <br/>
                                                <br/>
                                                <h3><strong><a href="' . URL . '/admin/' . $op . '" target="_blank">Painel Administrativo</a></strong></h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </body>
                    </html>';

        if ($tipo == 1) {
            $enviado = Common::dispararEmailPersonalizado('Santos Dumont - Novo Cliente Cadastrado', $html, $email, $info);
        } elseif ($tipo == 2) {
            $enviado = Common::dispararEmailPersonalizado('Santos Dumont - Novo Pedido Cadastrado', $html, $email, $info);
        } else {
            $enviado = Common::dispararEmailPersonalizado('Santos Dumont - Pedido Cancelado', $html, $email, $info);
        }

        $this->model->LerNotificacao($token, $tipo, 2);
    }

    public function Notificacoes($op)
    {

        $token = Session::get('token');
        $this->model->LerNotificacao($token, $op, 1);

        switch ($op) {
            case 1:
                Common::redir('Clientes');
                break;
            case 2:
                Common::redir('Vendas/Pedidos');
                break;
            case 3:
                Common::redir('Vendas/Pedidos');
                break;
        }
    }
}
