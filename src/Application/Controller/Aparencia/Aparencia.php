<?php
namespace Application\Controller\Aparencia;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Aparencia extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function Imagens()
    {
        parent::prepararView("Aparencia/pag_imagens");
    }

    function Layout()
    {
        parent::prepararView("Aparencia/pag_layout");
    }
}
