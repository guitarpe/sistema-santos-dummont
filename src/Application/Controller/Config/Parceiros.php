<?php
namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Parceiros extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Parceiros Cadastrados";
        $dados['listaparceiros'] = Common::retornoWSLista($this->model->ListaParceirosCadastrados($token));
        parent::prepararView("Config/pag_parceiros", $dados);
    }

    function Parceiro($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['parceiro'] = Common::retornoWSLista($this->model->DadosParceiro($token, $id))[0];
            $dados['urlaction'] = SITE_URL . "/Parceiros/Editar";
            $dados['titulopagina'] = "Editar Parceiro";
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Parceiros/Inserir";
            $dados['titulopagina'] = "Novo Parceiro";
        }
        parent::prepararView("Config/cad_parceiro", $dados);
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $status = filter_input(INPUT_POST, "status");
        $descricao = filter_input(INPUT_POST, "descricao");

        $now = new \DateTime();

        $pasta = $now->format('Y_m_d_H_i_s');

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $ret = Common::salvarImagem($_FILES['foto'], FOTOPARCEIROS_URL . '/' . $pasta);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'PAR_ID' => 0,
                'PAR_NOME' => $nome,
                'PAR_STATUS' => $status,
                'PAR_DESCRICAO' => $descricao,
                'PASTA_IMAGENS' => $pasta,
                'IMAGENS' => count($imagens) > 0 ? implode(',', $imagens) : null
            ];

            $cadastrar = $this->model->CadastrarEditarParceiro($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Parceiros');
            } else {
                $msg = 'Parceiro cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Parceiros');
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $status = filter_input(INPUT_POST, "status");
        $descricao = filter_input(INPUT_POST, "descricao");

        $now = new \DateTime();

        $token = Session::get('token');
        $parceiro = Common::retornoWSLista($this->model->DadosParceiro($token, $id))[0];

        $this->validarCamposObrigatorio($id, $parceiro['IMAGENS']);

        $pasta = empty($parceiro['PASTA_IMAGENS']) ? $now->format('Y_m_d_H_i_s') : $parceiro['PASTA_IMAGENS'];

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $ret = Common::salvarImagem($_FILES['foto'], FOTOPARCEIROS_URL . '/' . $pasta);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'PAR_ID' => $id,
                'PAR_NOME' => $nome,
                'PAR_STATUS' => $status,
                'PAR_DESCRICAO' => $descricao,
                'PASTA_IMAGENS' => $pasta,
                'IMAGENS' => count($imagens) > 0 ? implode(',', $imagens) : $parceiro['IMAGENS']
            ];

            $cadastrar = $this->model->CadastrarEditarParceiro($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Parceiros');
            } else {
                $msg = 'Parceiro editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Parceiros');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'PAR_ID' => $id
        ];

        $deletar = $this->model->ExcluirParceiro($dados);

        if ($deletar['list']['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Parceiros';
        } else {
            $msg = 'Parceiro deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Parceiros';
        }
    }

    private function validarCamposObrigatorio($id = null, $img = null)
    {
        $dados['Nome do Parceiro'] = filter_input(INPUT_POST, "nome");
        $dados['Descrição'] = filter_input(INPUT_POST, "descricao");

        if (empty($id)) {
            $dados['Foto'] = $_FILES['foto']['name'];
        } else {
            if (empty($img)) {
                $dados['Foto'] = $_FILES['foto']['name'];
            }
        }

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Parceiros/Parceiro/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Parceiros/Parceiro');
        }
    }
}
