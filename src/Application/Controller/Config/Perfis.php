<?php
namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Perfis extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelUsuario', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Perfis Cadastrados";
        $dados['listaperfis'] = Common::retornoWSLista($this->model->ListaPerfisCadastrados($token));
        parent::prepararView("Config/pag_perfis", $dados);
    }

    function Perfil($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Cadastrar Novo Perfil";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Perfis/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Perfil";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Perfis/Editar";
            $dados['perfil'] = Common::retornoWSLista($this->model->DadosPerfil($token, $id))[0];
        }

        $dados['listamenus'] = Common::retornoWSLista($this->model->ListaMenusCadastrados($token));

        parent::prepararView("Config/cad_perfis", $dados);
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $sigla = filter_input(INPUT_POST, 'sigla');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $permissoes = filter_input(INPUT_POST, 'permissoes', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'PER_ID' => 0,
                'PER_SIGLA' => $sigla,
                'PER_DESC' => $descricao,
                'PER_PERMISSIONS' => implode(",", $permissoes)
            ];

            $cadastrar = $this->model->CadastrarEditarPerfis($parametros);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Perfis');
            } else {

                $msg = 'Perfil cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Perfis');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $sigla = filter_input(INPUT_POST, 'sigla');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $permissoes = filter_input(INPUT_POST, 'permissoes', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'PER_ID' => $id,
                'PER_SIGLA' => $sigla,
                'PER_DESC' => $descricao,
                'PER_PERMISSIONS' => implode(",", $permissoes)
            ];

            $editar = $this->model->CadastrarEditarPerfis($parametros);

            if ($editar['list'][0]['O_COD_RETORNO'] != 0) {
                $msg = $editar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Perfis');
            } else {

                $msg = 'Perfil editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Perfis');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'PER_ID' => $id
        ];

        $deletar = $this->model->ExcluirPerfil($dados);

        if ($deletar['list']['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Perfis';
        } else {
            $msg = 'Perfil deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Perfis';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {

        $dados['Sigla'] = filter_input(INPUT_POST, 'sigla');
        $dados['Descrição'] = filter_input(INPUT_POST, 'descricao');
        //$dados['Perfis'] = filter_input(INPUT_POST, 'permissoes', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (count($dados['Perfis']) > 1) {
            unset($dados['Perfis']);
        }

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Perfis/Perfil/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Perfis/Perfil');
        }
    }
}
