<?php
namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Redes extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Redes Sociais Cadastrados";
        $dados['listaredes'] = Common::retornoWSLista($this->model->ListaRedesCadastrados($token));

        parent::prepararView("Config/pag_redes", $dados);
    }

    function Rede($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Cadastrar Nova Rede Social";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Redes/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Rede Social";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Redes/Editar";
            $dados['rede'] = Common::retornoWSLista($this->model->DadosRede($token, $id))[0];
        }

        parent::prepararView("Config/cad_redes", $dados);
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");

        $nome = filter_input(INPUT_POST, 'nome');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $link = filter_input(INPUT_POST, 'link');
        $icone = filter_input(INPUT_POST, 'icone');
        $exec_login = filter_input(INPUT_POST, 'exec_login');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'RS_ID' => 0,
                'RS_NOME' => $nome,
                'RS_DESC' => $descricao,
                'RS_LINK' => $link,
                'RS_ICO' => $icone,
                'RE_EXEC_LOGIN' => $exec_login
            ];

            $cadastrar = $this->model->CadastrarEditarRedes($parametros);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Redes');
            } else {

                $msg = 'Rede social cadastrada com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Redes');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, 'nome');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $link = filter_input(INPUT_POST, 'link');
        $icone = filter_input(INPUT_POST, 'icone');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'RS_ID' => $id,
                'RS_NOME' => $nome,
                'RS_DESC' => $descricao,
                'RS_LINK' => $link,
                'RS_ICO' => $icone
            ];

            $editar = $this->model->CadastrarEditarRedes($parametros);

            if ($editar['list']['O_COD_RETORNO'] != 0) {
                $msg = $editar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Redes');
            } else {

                $msg = 'Rede social editada com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Redes');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'RS_ID' => $id
        ];

        $deletar = $this->model->ExcluirRede($dados);

        if ($deletar['list']['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Redes';
        } else {
            $msg = 'Rede social deletada com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Redes';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {

        $dados['Descrição'] = filter_input(INPUT_POST, 'descricao');
        $dados['Nome'] = filter_input(INPUT_POST, 'nome');
        $dados['Link'] = filter_input(INPUT_POST, 'link');
        $dados['Ícone'] = filter_input(INPUT_POST, 'icone');

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Redes/Rede/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Redes/Rede');
        }
    }
}
