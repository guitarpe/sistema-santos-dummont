<?php

namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Sistema extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');
        $tabativa = Session::get('tabativa');

        $dados['urlactioncontato'] = SITE_URL . "/Sistema/SalvarContato";
        $dados['urlactionsmtp'] = SITE_URL . "/Sistema/SalvarInfoSMTP";
        $dados['urlactioninfo'] = SITE_URL . "/Sistema/SalvarInformacoes";
        $dados['urlactionintegracao'] = SITE_URL . "/Sistema/SalvarInfoIntegracoes";
        $dados['urlactionimg'] = SITE_URL . "/Sistema/SalvarImagens";
        $dados['urlactionvideos'] = SITE_URL . "/Sistema/SalvarVideos";
        $dados['cfgcontato'] = Common::retornoWSLista($this->model->ConfiguracoesADM($token))[0];
        $dados['titulopagina'] = "Configurações e Informações gerais do Sistema";
        $dados['titleaction'] = 'Salvar';
        $dados['tabativa'] = empty($tabativa) ? 1 : $tabativa;

        parent::prepararView("Config/pag_config", $dados);
    }

    function SalvarContato()
    {
        $token = Session::get('token');

        $this->validarCamposObrigatorio(1);

        $submit = filter_input(INPUT_POST, "submit");

        $idcontato = filter_input(INPUT_POST, "idcontato");
        $endereco = filter_input(INPUT_POST, "endereco");
        $numero = filter_input(INPUT_POST, "numero");
        $complemento = filter_input(INPUT_POST, "complemento");
        $bairro = filter_input(INPUT_POST, "bairro");
        $cidade = filter_input(INPUT_POST, "cidade");
        $uf = filter_input(INPUT_POST, "uf");
        $telefone = filter_input(INPUT_POST, "telefone");
        $cep = filter_input(INPUT_POST, "cep");
        $email = filter_input(INPUT_POST, "email");

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'ID' => $idcontato,
                'ENDERECO' => $endereco,
                'BAIRRO' => $bairro,
                'NUMERO' => $numero,
                'CIDADE' => $cidade,
                'UF' => $uf,
                'CEP' => $cep,
                'COMPLEMENTO' => $complemento,
                'TELEFONE' => $telefone,
                'EMAIL_TECNICO' => $email
            ];

            $cadastrar = $this->model->SalvarSisInfoContato($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Session::set('tabativa', 1);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            } else {
                $msg = 'Informações de contato salvas com sucesso!';
                $situacao = 'success';

                Session::set('tabativa', 1);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            }
        }
    }

    function SalvarInfoSMTP()
    {
        $this->validarCamposObrigatorio(2);

        $token = Session::get('token');

        $idsmtp = filter_input(INPUT_POST, "idsmtp");
        $host = filter_input(INPUT_POST, "host");
        $usuario = filter_input(INPUT_POST, "usuario");
        $senha = filter_input(INPUT_POST, "senha");
        $porta = filter_input(INPUT_POST, "porta");
        $contatopadrao = filter_input(INPUT_POST, "contatopadrao");
        $emailcontatopadrao = filter_input(INPUT_POST, "emailcontatopadrao");
        $submit = filter_input(INPUT_POST, "submit");

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'ID' => $idsmtp,
                'SMTP_HOST' => $host,
                'SMTP_PORTA' => $porta,
                'SMTP_USER' => $usuario,
                'SMTP_SENHA' => $senha,
                'MAIL_CONTATO_DEFAULT' => $emailcontatopadrao,
                'NOME_CONTATO_DEFAULT' => $contatopadrao,
            ];

            $cadastrar = $this->model->SalvarSisInfoSMTP($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Session::set('tabativa', 3);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            } else {
                $msg = 'Informações salvas com sucesso!';
                $situacao = 'success';

                Session::set('tabativa', 3);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            }
        }
    }

    function SalvarInformacoes($op)
    {

        $this->validarCamposObrigatorio(3, $op);

        $token = Session::get('token');

        $idinfo = filter_input(INPUT_POST, "idinfo");

        if ($op == 1) {
            $info = filter_input(INPUT_POST, "sobrenos");
        }
        if ($op == 2) {
            $info = filter_input(INPUT_POST, "infoentrega");
        }
        if ($op == 3) {
            $info = filter_input(INPUT_POST, "servicocliente");
        }
        if ($op == 4) {
            $info = filter_input(INPUT_POST, "transpretorno");
        }
        if ($op == 5) {
            $info = filter_input(INPUT_POST, "metodospagamento");
        }
        if ($op == 6) {
            $info = filter_input(INPUT_POST, "termosuso");
        }
        if ($op == 7) {
            $info = filter_input(INPUT_POST, "politicaprivacidade");
        }

        $submit = filter_input(INPUT_POST, "submit");

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'ID' => $idinfo,
                'INFO' => $info
            ];

            $cadastrar = $this->model->SalvarSisInformacoes($dados, $op);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Session::set('tabativa', 2);

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            } else {
                $msg = 'Informações salvas com sucesso!';
                $situacao = 'success';

                Session::set('tabativa', 2);

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            }
        }
    }

    function SalvarImagens()
    {
        $token = Session::get('token');

        $submit = filter_input(INPUT_POST, "submit");
        $idimagens = filter_input(INPUT_POST, "idimagens");

        $cfgcontato = Common::retornoWSLista($this->model->ConfiguracoesADM($token));

        $logotipo = [];
        $favicon = [];
        $pasta = 'img';

        if (!empty($_FILES['logotipo']['name'])) {
            $config = array('tamanho' => 4999999, 'largura' => 250, 'altura' => 160);
            $ret = $this->salvarVideo($_FILES['logotipo'], $pasta, $config);
            array_push($logotipo, $pasta . "/" . $ret['list']['imagem']);
        }

        if (!empty($_FILES['favicon']['name'])) {
            $config = array('tamanho' => 4999999, 'largura' => 250, 'altura' => 160);
            $ret = $this->salvarVideo($_FILES['favicon'], $pasta, $config);
            array_push($favicon, $pasta . "/" . $ret['list']['imagem']);
        }

        $this->validarCamposObrigatorioImg($cfgcontato['LOGOTIPO'], $cfgcontato['FAVICON']);

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'ID' => $idimagens,
                'LOGOTIPO' => count($logotipo) > 0 ? implode(',', $logotipo) : $cfgcontato['LOGOTIPO'],
                'FAVICON' => count($favicon) > 0 ? implode(',', $favicon) : $cfgcontato['FAVICON']
            ];

            $cadastrar = $this->model->SalvarSisImagens($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                $imgl = implode(',', $logotipo);
                if (!empty($imgl)) {
                    $this->removerVideo($imgl);
                }

                $imgf = implode(',', $favicon);
                if (!empty($imgf)) {
                    $this->removerVideo($imgf);
                }

                Session::set('tabativa', 4);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            } else {
                $msg = 'Imagens salvas com sucesso!';
                $situacao = 'success';

                Session::set('tabativa', 4);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            }
        }
    }

    function SalvarVideos()
    {
        $token = Session::get('token');

        $submit = filter_input(INPUT_POST, "submit");
        $idvideos = filter_input(INPUT_POST, "idvideos");
        $tipovideos = intval(filter_input(INPUT_POST, "tipovideo"));

        $videos = null;

        $cfgcontato = Common::retornoWSLista($this->model->ConfiguracoesADM($token));

        if ($tipovideos == 1) {
            $videos = filter_input(INPUT_POST, "videobvlink");
        } else {
            $videosup = [];
            $pasta = 'videos';

            if (!empty($_FILES['videobv']['name'])) {
                $config = array('tamanho' => 4999999);

                $ret = $this->salvarVideo($_FILES['logotipo'], $pasta, $config);
                array_push($videosup, $pasta . "/" . $ret['list']['imagem']);
            }

            $videos = count($videosup) > 0 ? implode(',', $videosup) : "";
        }

        $this->validarCamposObrigatorioVideos($tipovideos, $cfgcontato['VIDEO_BOASVINDAS']);

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'ID' => $idvideos,
                'TIPO_VIDEO' => $tipovideos,
                'VIDEO_BOASVINDAS' => !empty($videos) ? $videos : $cfgcontato['VIDEO_BOASVINDAS']
            ];

            $cadastrar = $this->model->SalvarSisVideos($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                if ($tipovideos == 2) {
                    $imgl = implode(',', $videosup);
                    if (!empty($imgl)) {
                        $this->removerVideo($imgl);
                    }
                }

                Session::set('tabativa', 6);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            } else {
                $msg = 'Imagens salvas com sucesso!';
                $situacao = 'success';

                Session::set('tabativa', 6);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            }
        }
    }

    function SalvarInfoIntegracoes()
    {
        $this->validarCamposObrigatorio(4);

        $token = Session::get('token');

        $idinfo = filter_input(INPUT_POST, "idinfo");
        $ip_integracao = filter_input(INPUT_POST, "ip_integracao");
        $email_notificaco = filter_input(INPUT_POST, "email_notificaco");
        $medida_peso = filter_input(INPUT_POST, "medida_peso");
        $medida_tamanho = filter_input(INPUT_POST, "medida_tamanho");
        $correio_usuario = filter_input(INPUT_POST, "correio_usuario");
        $correio_senha = filter_input(INPUT_POST, "correio_senha");
        $tracking_id = filter_input(INPUT_POST, "tracking_id");
        $site_id = filter_input(INPUT_POST, "site_id");
        $chave_captcha = filter_input(INPUT_POST, "chave_captcha");
        $facebook_id = filter_input(INPUT_POST, "facebook_id");
        $app_secret_id = filter_input(INPUT_POST, "app_secret_id");
        $cielo_mechant_id = filter_input(INPUT_POST, "cielo_mechant_id");
        $cielo_mechant_key = filter_input(INPUT_POST, "cielo_mechant_key");
        $cielo_client_id = filter_input(INPUT_POST, "cielo_client_id");
        $pagseg_email = filter_input(INPUT_POST, "pagseg_email");
        $pagseg_tipo_envio = filter_input(INPUT_POST, "pagseg_tipo_envio");
        $pagseg_sand_token = filter_input(INPUT_POST, "pagseg_sand_token");
        $pagseg_sand_app_id = filter_input(INPUT_POST, "pagseg_sand_app_id");
        $pagseg_sand_app_key = filter_input(INPUT_POST, "pagseg_sand_app_key");
        $pagseg_prd_token = filter_input(INPUT_POST, "pagseg_prd_token");
        $pagseg_prd_app_id = filter_input(INPUT_POST, "pagseg_prd_app_id");
        $pagseg_prd_app_key = filter_input(INPUT_POST, "pagseg_prd_app_key");
        $tag_manager_id = filter_input(INPUT_POST, "tag_manager_id");
        $google_usuario = filter_input(INPUT_POST, "google_usuario");
        $google_senha = filter_input(INPUT_POST, "google_senha");


        $submit = filter_input(INPUT_POST, "submit");

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'ID' => $idinfo,
                'IP_INTEGRACAO' => $ip_integracao,
                'EMAIL_NOTIFICACAO' => $email_notificaco,
                'UNI_MED_PESO' => $medida_peso,
                'UNI_MED_TAM' => $medida_tamanho,
                'CORREIO_USUARIO' => $correio_usuario,
                'CORREIO_SENHA' => $correio_senha,
                'TRACKING_ID' => $tracking_id,
                'SITE_ID' => $site_id,
                'CHAVE_CAPTCHA' => $chave_captcha,
                'FACEBOOK_ID' => $facebook_id,
                'APP_SECRET_ID' => $app_secret_id,
                'CIELO_MECHANT_ID' => $cielo_mechant_id,
                'CIELO_MECHANT_KEY' => $cielo_mechant_key,
                'CIELO_CLIENT_ID' => $cielo_client_id,
                'PAGSEG_EMAIL' => $pagseg_email,
                'PAGSEG_TIPO_ENVIO' => $pagseg_tipo_envio,
                'PAGSEG_SAND_TOKEN' => $pagseg_sand_token,
                'PAGSEG_SAND_APP_ID' => $pagseg_sand_app_id,
                'PAGSEG_SAND_APP_KEY' => $pagseg_sand_app_key,
                'PAGSEG_PRD_TOKEN' => $pagseg_prd_token,
                'PAGSEG_PRD_APP_ID' => $pagseg_prd_app_id,
                'PAGSEG_PRD_APP_KEY' => $pagseg_prd_app_key,
                'TAG_MANAGER_ID' => $tag_manager_id,
                'GOOGLE_USUARIO' => $google_usuario,
                'GOOGLE_SENHA' => $google_senha
            ];

            $cadastrar = $this->model->SalvarSisIntegracoes($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Session::set('tabativa', 5);

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            } else {
                $msg = 'Informações salvas com sucesso!';
                $situacao = 'success';

                Session::set('tabativa', 5);

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            }
        }
    }

    private function salvarVideo($file, $pasta, $config)
    {
        $Video = new \Application\Controller\Common\Video($config);

        if (!file_exists(PATHSTATIC . '/' . $pasta)) {
            mkdir(PATHSTATIC . '/' . $pasta, 0777, true);
        }

        return $Video->executar(PATHSTATIC . '/' . $pasta, $file);
    }

    private function removerVideo($nome_imagem)
    {
        $caminho_imagem = PATHSTATIC . "/" . $nome_imagem;
        if (file_exists($caminho_imagem))
            @unlink($caminho_imagem);
        clearstatcache(TRUE, $caminho_imagem);
    }

    private function validarCamposObrigatorio($tipo = null, $op = null)
    {
        if ($tipo == 1) {
            $dados['Endereço'] = filter_input(INPUT_POST, "endereco");
            $dados['Número'] = filter_input(INPUT_POST, "numero");
            $dados['Complemento'] = filter_input(INPUT_POST, "complemento");
            $dados['Bairro'] = filter_input(INPUT_POST, "bairro");
            $dados['Cidade'] = filter_input(INPUT_POST, "cidade");
            $dados['UF'] = filter_input(INPUT_POST, "uf");
            $dados['Telefone'] = filter_input(INPUT_POST, "telefone");
            $dados['CEP'] = filter_input(INPUT_POST, "cep");
            $dados['E-mail'] = filter_input(INPUT_POST, "email");
        }

        if ($tipo == 2) {
            $dados['Host'] = filter_input(INPUT_POST, "host");
            $dados['Usuário'] = filter_input(INPUT_POST, "usuario");
            $dados['Senha'] = filter_input(INPUT_POST, "senha");
            $dados['Porta'] = filter_input(INPUT_POST, "porta");
            $dados['Contato Padrão'] = filter_input(INPUT_POST, "contatopadrao");
            $dados['Email do Contato Padrão'] = filter_input(INPUT_POST, "emailcontatopadrao");
        }

        if ($tipo == 3) {
            if ($op == 1) {
                $dados['Sobre Nós'] = filter_input(INPUT_POST, "sobrenos");
            }
            if ($op == 2) {
                $dados['Informação de entrega'] = filter_input(INPUT_POST, "infoentrega");
            }
            if ($op == 3) {
                $dados['Serviço ao Cliente'] = filter_input(INPUT_POST, "servicocliente");
            }
            if ($op == 4) {
                $dados['Transporte e Retornos'] = filter_input(INPUT_POST, "transpretorno");
            }
            if ($op == 5) {
                $dados['Métodos de Pagamento'] = filter_input(INPUT_POST, "metodospagamento");
            }
            if ($op == 6) {
                $dados['Termos de Uso'] = filter_input(INPUT_POST, "termosuso");
            }
            if ($op == 7) {
                $dados['Política de Privacidade'] = filter_input(INPUT_POST, "politicaprivacidade");
            }
        }

        if ($tipo == 4) {
            $dados['IP de Comunicação'] = filter_input(INPUT_POST, "ip_integracao");
        }

        Common::validarInputsObrigatorio($dados, 'Sistema');
    }

    private function validarCamposObrigatorioImg($logo, $favicon)
    {
        if (empty($logo)) {
            $dados['Logotipo'] = $_FILES['logotipo']['name'];
        }

        if (empty($favicon)) {
            $dados['Favicon'] = $_FILES['favicon']['name'];
        }

        Common::validarInputsObrigatorio($dados, 'Sistema');
    }

    private function validarCamposObrigatorioVideos($tipo, $video)
    {
        if ($tipo == 2) {
            if (empty($video)) {
                $dados['Vídeo'] = $_FILES['favicon']['name'];
            }
        } else {
            if (empty($video)) {
                $dados['Vídeo'] = filter_input(INPUT_POST, "videobvlink");
            }
        }

        Common::validarInputsObrigatorio($dados, 'Sistema');
    }
}
