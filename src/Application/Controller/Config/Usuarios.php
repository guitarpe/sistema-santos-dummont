<?php

namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session,
    santosdummont\PHPMailer;

require_once SYSTEM_PATH . 'vendor/santosdummont/vendor/autoload.php';

class Usuarios extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelUsuario', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Usuários Cadastrados";
        $dados['acaoimportar'] = SITE_URL . "/Usuarios/Importar";
        $dados['listausuarios'] = Common::retornoWSLista($this->model->ListaUsuariosCadastrados($token));
        parent::prepararView("Config/pag_usuarios", $dados);
    }

    function Usuario($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Novo Usuário";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Usuarios/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Usuário";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Usuarios/Editar";
            $dados['usuario'] = Common::retornoWSLista($this->model->DadosUsuario($token, $id))[0];
        }
        $dados['listaperfis'] = Common::retornoWSLista($this->model->ListaPerfisCadastrados($token));

        parent::prepararView("Config/cad_usuarios", $dados);
    }

    function ResertarSenha($id)
    {
        $token = Session::get('token');

        $dados = Common::retornoWSLista($this->model->DadosUsuario($token, $id))[0];
        $config = Common::retornoWSLista($this->model->ConfigEnvioEmail())[0];

        $parametros = [
            'US_ID' => $id,
            'US_LOGIN' => $dados['US_LOGIN'],
            'US_SENHA' => '',
            'US_STATUS' => '2'
        ];

        $resetar = $this->model->TrocarSenhaReset($parametros);

        if ($resetar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $resetar['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios');
        } else {

            $info = array(
                'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                'fromname' => $config['NOME_CONTATO_DEFAULT'],
                'url' => $config['URL_SISTEMA'],
                'logotipo' => $config['LOGOTIPO']
            );

            $enviado = Common::dispararEmail($dados, $dados['EMAIL'], 'Reset de Senha', $info, 1);

            if ($enviado) {
                $msg = 'Senha resetada com sucesso!<br/>O usuário receberá um e-mail para gerar uma nova senha.';
                $situacao = 'success';
            } else {
                $msg = 'Ocorreu um erro ao enviar o e-mail!<br/>Entre em contato com o administrador.';
                $situacao = 'danger';
            }

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios');
        }
    }

    function Inativar($id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'US_ID' => $id,
            'US_STATUS' => 0
        ];

        $status = $this->model->AtivarInativarUsuario($parametros);
        if ($status['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $status['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Inativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function Ativar($id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'US_ID' => $id,
            'US_STATUS' => 1
        ];

        $status = $this->model->AtivarInativarUsuario($parametros);

        if ($status['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $status['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Ativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function MeusDados()
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        $dados['titulopagina'] = "Meus Dados";
        $dados['titleaction'] = "Salvar Edição";
        $dados['urlaction'] = SITE_URL . "/Usuarios/Editar";
        $dados['usuario'] = Common::retornoWSLista($this->model->UsuarioLogado($token))[0];
        parent::prepararView("Config/cad_usuarios", $dados);
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio(1);

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $perfil = filter_input(INPUT_POST, "perfil");
        $nome = filter_input(INPUT_POST, "nome");
        $email = filter_input(INPUT_POST, "email");
        $telefone = filter_input(INPUT_POST, "telefone");
        $genero = filter_input(INPUT_POST, "genero");
        $login = filter_input(INPUT_POST, "login");

        $now = new \DateTime();

        $pasta = $now->format('Y_m_d_H_i_s');

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 200, 'altura' => 200);

                $ret = Common::salvarImagem($_FILES['foto'], FOTOUSUARIOS_URL . '/' . $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $parametros = [
                'TOKEN' => $token,
                'US_ID' => 0,
                'PER_ID' => $perfil,
                'US_LOGIN' => $login,
                'US_PASS' => '',
                'US_STATUS' => '2',
                'US_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : null,
                'NOME_USER' => $nome,
                'EMAIL_USER' => $email,
                'GENERO_USER' => $genero,
                'TEL_USER' => $telefone,
                'PASTA_USER' => $pasta
            ];

            $cadastrar = $this->model->CadastrarEditarUsuarios($parametros);

            if (intval($cadastrar['list'][0]['O_COD_RETORNO']) != 0) {
                $msg = $cadastrar['list'][0]['O_DESC_CURTO'];
                $situacao = 'danger';

                $img = implode(',', $imagens);
                if (!empty($img)) {
                    Common::removerImagem(FOTOUSUARIOS_URL . '/' . $img);
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios');
            } else {
                $dados = Common::retornoWSLista($this->model->UsuarioPorEmail($email))[0];
                $config = Common::retornoWSLista($this->model->ConfigEnvioEmail())[0];

                $info = array(
                    'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                    'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                    'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                    'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                    'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                    'fromname' => $config['NOME_CONTATO_DEFAULT'],
                    'url' => $config['URL_SISTEMA'],
                    'logotipo' => $config['LOGOTIPO']
                );

                Common::dispararEmail($dados, $email, 'Novo Usuário Cadastrado', $info, 2);

                $msg = 'Usuário cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios');
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');
        $usuario = Common::retornoWSLista($this->model->DadosUsuario($token, $id))[0];

        $submit = filter_input(INPUT_POST, "submit");

        $perfil = filter_input(INPUT_POST, "perfil");
        $nome = filter_input(INPUT_POST, "nome");
        $email = filter_input(INPUT_POST, "email");
        $telefone = filter_input(INPUT_POST, "telefone");
        $genero = filter_input(INPUT_POST, "genero");
        $login = filter_input(INPUT_POST, "login");
        $status = filter_input(INPUT_POST, "status");

        $this->validarCamposObrigatorio(2, $usuario['US_IMAGEM']);

        $now = new \DateTime();

        $imagemantiga = $usuario['US_IMAGEM'];

        $pasta = empty($usuario['PASTA_USER']) ? $now->format('Y_m_d_H_i_s') : $usuario['PASTA_USER'];

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 200, 'altura' => 200);

                $ret = Common::salvarImagem($_FILES['foto'], FOTOUSUARIOS_URL . '/' . $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => $token,
                'US_ID' => $id,
                'PER_ID' => $perfil,
                'US_LOGIN' => $login,
                'US_PASS' => '',
                'US_STATUS' => $status,
                'US_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : $usuario['US_IMAGEM'],
                'NOME_USER' => $nome,
                'EMAIL_USER' => $email,
                'GENERO_USER' => $genero,
                'TEL_USER' => $telefone,
                'PASTA_USER' => $pasta
            ];

            $editar = $this->model->CadastrarEditarUsuarios($dados);

            if ($editar['list']['O_COD_RETORNO'] != 0) {
                $msg = $editar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                if (!empty($imagens[0])) {
                    Common::removerImagem(FOTOUSUARIOS_URL . '/' . $imagens[0]);
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios/Usuario/' . $id);
            } else {

                if (count($imagens['name']) > 0) {
                    Common::removerImagem(FOTOUSUARIOS_URL . '/' . $imagemantiga);
                }

                $msg = 'Usuário Editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");

        $token = Session::get('token');
        $usuario = Common::retornoWSLista($this->model->DadosUsuario($token, $id))[0];

        $dados = [
            'TOKEN' => $token,
            'US_ID' => $id
        ];

        $deletar = $this->model->ExcluirUsuario($dados);

        if ($deletar['list']['O_COD_RETORNO'] != 0) {
            $msg = 'Erro ao deletar o usuário, tente novamente mais tarde! ' . $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Usuarios';
        } else {
            $msg = 'Usuário deletado com sucesso!';
            $situacao = 'success';

            Common::removerImagem(FOTOUSUARIOS_URL . '/' . $usuario['US_IMAGEM']);

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Usuarios';
        }
    }

    function Importar()
    {
        $token = Session::get('token');

        if (empty($_FILES['arquivo_importacao']['name'][0])) {
            $msg = 'É necessário selecionar um arquivo!';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Produtos');
        } else {

            $ret = Common::salvarArquivo($_FILES['arquivo_importacao']);
            $file = $ret['list']['documento'];

            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load(PASTAARQUIVOS_URL . '/' . $file);

            $campos = [];
            $importados = 0;
            $linhasimpo = 0;

            $naoimportados = [];

            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

                $linhas = [];

                foreach ($worksheet->getRowIterator() as $row) {
                    if ($row->getRowIndex() == 1) {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        foreach ($cellIterator as $cell) {
                            if (!is_null($cell)) {
                                array_push($campos, $cell->getCalculatedValue());
                            }
                        }
                    } else {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        $valores = [];
                        foreach ($cellIterator as $cell) {
                            $valor = $cell->getCalculatedValue();
                            array_push($valores, $valor);
                        }

                        if (!empty($valores[0])) {
                            $linhas['TOKEN'] = $token;
                            $linhas['US_ID'] = 0;
                            $linhas['PER_ID'] = 0;
                            $linhas['US_IMAGEM'] = "";
                            $linhas['PASTA_USER'] = "";
                            $linhas['US_PASS'] = '';
                            $linhas['US_STATUS'] = 2;

                            foreach ($valores as $key => $value) {
                                $linhas[$campos[$key]] = $value;
                            }

                            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarUsuarios($linhas));

                            if (intval($cadastrar[0]['O_COD_RETORNO']) == 0) {
                                $importados++;
                            } else {
                                array_push($naoimportados, $linhas['NOME_USER']);
                            }
                        }

                        $linhasimpo++;
                    }
                }
            }

            Common::removerArquivo($file);

            if ($importados > 0) {
                $msg = 'Foram importados ' . $importados . ' usuarios com sucesso!';

                if (count($naoimportados) > 0) {
                    $msg .= '\nOs usuarios ' . implode('\n', $naoimportados) . ' não foram importados.';
                }

                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios');
            } else {
                $msg = 'Ocorreu algum erro ao importar os usuarios';
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios');
            }
        }
    }

    private function validarCamposObrigatorio($tipo, $img = null)
    {
        if ($tipo == 1) {

            $dados['Perfil de Usuário'] = filter_input(INPUT_POST, "perfil");
            $dados['Nome Completo'] = filter_input(INPUT_POST, "nome");
            $dados['Sexo'] = filter_input(INPUT_POST, "genero");
            $dados['Telefone'] = filter_input(INPUT_POST, "telefone");
            $dados['Login'] = filter_input(INPUT_POST, "login");
            $dados['E-mail'] = filter_input(INPUT_POST, "email");

            $dados['Foto'] = $_FILES['foto']['name'];

            Common::validarInputsObrigatorio($dados, 'Usuarios/Usuario');
        }

        if ($tipo == 2) {

            $id = filter_input(INPUT_POST, "id");
            $dados['Perfil de Usuário'] = filter_input(INPUT_POST, "perfil");
            $dados['Nome Completo'] = filter_input(INPUT_POST, "nome");
            $dados['Sexo'] = filter_input(INPUT_POST, "genero");
            $dados['Telefone'] = filter_input(INPUT_POST, "telefone");
            $dados['Login'] = filter_input(INPUT_POST, "login");
            $dados['E-mail'] = filter_input(INPUT_POST, "email");
            $dados['Status'] = filter_input(INPUT_POST, "status");

            if (empty($img)) {
                $dados['Foto'] = $_FILES['foto']['name'];
            }

            Common::validarInputsObrigatorio($dados, 'Usuarios/Usuario/' . $id);
        }
    }
}
