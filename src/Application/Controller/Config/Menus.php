<?php

namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Menus extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Menus Cadastrados";
        $dados['listamenus'] = Common::retornoWSLista($this->model->ListaMenusCadastrados($token));
        parent::prepararView("Config/pag_menus", $dados);
    }

    function Menu($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Cadastrar Novo Menu";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Menus/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Menu";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Menus/Editar";
            $dados['dadosmenu'] = Common::retornoWSLista($this->model->DadosMenu($token, $id))[0];
        }

        $dados['listamenus'] = Common::retornoWSLista($this->model->ExibirMenu($token));

        parent::prepararView("Config/cad_menus", $dados);
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");

        $nome = filter_input(INPUT_POST, 'nome');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $link = filter_input(INPUT_POST, 'link');
        $menupai = filter_input(INPUT_POST, 'menupai');
        $icone = filter_input(INPUT_POST, 'icone');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MN_ID' => 0,
                'MN_NOME' => $nome,
                'MN_DESC' => $descricao,
                'MN_LINK' => $link,
                'MN_PAI' => $menupai,
                'MN_IMG' => $icone
            ];

            $cadastrar = $this->model->CadastrarEditarMenus($parametros);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Menus');
            } else {

                $msg = 'Menu cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Menus');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, 'nome');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $link = filter_input(INPUT_POST, 'link');
        $menupai = filter_input(INPUT_POST, 'menupai');
        $icone = filter_input(INPUT_POST, 'icone');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MN_ID' => $id,
                'MN_NOME' => $nome,
                'MN_DESC' => $descricao,
                'MN_LINK' => $link,
                'MN_PAI' => $menupai,
                'MN_IMG' => $icone
            ];

            $editar = $this->model->CadastrarEditarMenus($parametros);

            if ($editar['list']['O_COD_RETORNO'] != 0) {
                $msg = $editar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Menus');
            } else {

                $msg = 'Menu editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Menus');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'MN_ID' => $id
        ];

        $deletar = $this->model->ExcluirMenu($dados);

        if ($deletar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Menus';
        } else {
            $msg = 'Menu deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Menus';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {

        $dados['Descrição'] = filter_input(INPUT_POST, 'descricao');
        $dados['Nome'] = filter_input(INPUT_POST, 'nome');
        $dados['Link'] = filter_input(INPUT_POST, 'link');
        $dados['Menu Pai'] = filter_input(INPUT_POST, 'menupai');

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Menus/Menu/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Menus/Menu');
        }
    }
}
