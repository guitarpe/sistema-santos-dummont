<?php
namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Configuracoes extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }
}
