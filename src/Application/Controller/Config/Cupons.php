<?php

namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Cupons extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Cupons Cadastrados";
        $dados['listacupons'] = Common::retornoWSLista($this->model->ListaCuponsCadastrados($token));
        parent::prepararView("Config/pag_cupons", $dados);
    }

    function Cupons($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['cupons'] = Common::retornoWSLista($this->model->DadosCupon($token, $id))[0];
            $dados['urlaction'] = SITE_URL . "/Cupons/Editar";
            $dados['titulopagina'] = "Editar Cupon";
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Cupons/Inserir";
            $dados['titulopagina'] = "Novo Cupon";
        }
        parent::prepararView("Config/cad_cupons", $dados);
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $cupotoken = filter_input(INPUT_POST, "cupom");
        $dtini = filter_input(INPUT_POST, "data_ini");
        $dtfim = filter_input(INPUT_POST, "data_fim");
        $produto = filter_input(INPUT_POST, "produto");
        $desconto = filter_input(INPUT_POST, "desconto");
        $tipo = filter_input(INPUT_POST, "tipo");
        $status = filter_input(INPUT_POST, "status");
        $informacao = filter_input(INPUT_POST, "informacao");

        if (isset($submit)) {

            $dados = [
                'TOKEN' => Session::get('token'),
                'CUP_ID' => 0,
                'CUP_TOKEN' => $cupotoken,
                'DT_INI' => $dtini,
                'DT_FIM' => $dtfim,
                'PRD_ID' => $produto,
                'CUP_DESCONTO' => $desconto,
                'CUP_TIPO' => $tipo,
                'CUP_STATUS' => $status,
                'CUP_DESCRICAO' => $informacao
            ];

            $cadastrar = $this->model->CadastrarEditarCupon($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Cupons');
            } else {
                $msg = 'Cupon cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Cupons');
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");

        $cupotoken = filter_input(INPUT_POST, "cupom");
        $dtini = filter_input(INPUT_POST, "data_ini");
        $dtfim = filter_input(INPUT_POST, "data_fim");
        $produto = filter_input(INPUT_POST, "produto");
        $desconto = filter_input(INPUT_POST, "desconto");
        $tipo = filter_input(INPUT_POST, "tipo");
        $status = filter_input(INPUT_POST, "status");
        $informacao = filter_input(INPUT_POST, "informacao");

        $this->validarCamposObrigatorio($id);

        if (isset($submit)) {
            $dados = [
                'TOKEN' => Session::get('token'),
                'CUP_ID' => $id,
                'CUP_TOKEN' => $cupotoken,
                'DT_INI' => $dtini,
                'DT_FIM' => $dtfim,
                'PRD_ID' => $produto,
                'CUP_DESCONTO' => $desconto,
                'CUP_TIPO' => $tipo,
                'CUP_STATUS' => $status,
                'CUP_DESCRICAO' => $informacao
            ];

            $cadastrar = $this->model->CadastrarEditarCupon($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Cupons');
            } else {
                $msg = 'Cupon editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Cupons');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'CUP_ID' => $id
        ];

        $deletar = $this->model->ExcluirCupon($dados);

        if ($deletar['list']['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Cupons';
        } else {
            $msg = 'Cupon deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Cupons';
        }
    }

    function Status($status)
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'CUP_ID' => $id,
            'CUP_STATUS' => $status
        ];

        $alterar = $this->model->AtivaInativaCupon($dados);

        if ($alterar['list']['O_COD_RETORNO'] != 0) {
            $msg = $alterar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Cupons';
        } else {
            $msg = 'Cupon Alterado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Cupons';
        }
    }

    private function validarCamposObrigatorio($id = null, $img = null)
    {
        $dados['Cupom'] = filter_input(INPUT_POST, "cupom");
        $dados['Desconto'] = filter_input(INPUT_POST, "desconto");
        $dados['Descrição'] = filter_input(INPUT_POST, "informacao");

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Cupons/Cupon/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Cupons/Cupon');
        }
    }
}
