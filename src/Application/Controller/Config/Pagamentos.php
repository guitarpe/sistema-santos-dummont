<?php
namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Pagamentos extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Meios de Pagamentos Cadastrados";
        $dados['listameios'] = Common::retornoWSLista($this->model->ListaMeiosCadastrados($token));
        parent::prepararView("Config/pag_meios_pagamentos", $dados);
    }

    function MeioPagamento($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['meiopagamento'] = Common::retornoWSLista($this->model->DadosMeioPagamento($token, $id))[0];
            $dados['urlaction'] = SITE_URL . "/Pagamentos/Editar";
            $dados['titulopagina'] = "Editar Pagamento";
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Pagamentos/Inserir";
            $dados['titulopagina'] = "Novo Meio de Pagamento";
        }
        parent::prepararView("Config/cad_meio_pagamento", $dados);
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $status = filter_input(INPUT_POST, "status");
        $descricao = filter_input(INPUT_POST, "descricao");

        $pasta = 'meiospagamentos';

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 130, 'altura' => 100);

                $ret = Common::salvarImagem($_FILES['foto'], PATHIMAGEUPLOAD . '/' . $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'MEIO_ID' => 0,
                'NOME_MEIO' => $nome,
                'DESCRICAO_MEIO' => $descricao,
                'MP_STATUS' => $status,
                'IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : null
            ];

            $cadastrar = $this->model->CadastrarEditarMeioPagamento($dados);

            if ($cadastrar['list']['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Pagamentos');
            } else {
                $msg = 'Pagamento cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Pagamentos');
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $status = filter_input(INPUT_POST, "status");
        $descricao = filter_input(INPUT_POST, "descricao");

        $token = Session::get('token');
        $meio = Common::retornoWSLista($this->model->DadosMeioPagamento($token, $id))[0];

        $this->validarCamposObrigatorio($id, $meio['IMAGEM']);
        $pasta = 'meiospagamentos';

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 130, 'altura' => 100);

                $ret = Common::salvarImagem($_FILES['foto'], PATHIMAGEUPLOAD . '/' . $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'MEIO_ID' => $id,
                'NOME_MEIO' => $nome,
                'DESCRICAO_MEIO' => $descricao,
                'MP_STATUS' => $status,
                'IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : $meio['IMAGEM']
            ];

            $cadastrar = $this->model->CadastrarEditarMeioPagamento($dados);

            if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Pagamentos');
            } else {
                $msg = 'Pagamento editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Pagamentos');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'MEIO_ID' => $id
        ];

        $deletar = $this->model->ExcluirMeioPagamento($dados);

        if ($deletar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Pagamentos';
        } else {
            $msg = 'Pagamento deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Pagamentos';
        }
    }

    private function validarCamposObrigatorio($id = null, $img = null)
    {
        $dados['Título'] = filter_input(INPUT_POST, "nome");
        $dados['Descrição'] = filter_input(INPUT_POST, "descricao");

        if (empty($id)) {
            $dados['Foto'] = $_FILES['foto']['name'];
        } else {
            if (empty($img)) {
                $dados['Foto'] = $_FILES['foto']['name'];
            }
        }

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Pagamentos/MeioPagamento/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Pagamentos/MeioPagamento');
        }
    }
}
