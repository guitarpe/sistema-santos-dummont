<?php
namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Aparencia extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function Imagens()
    {
        $dados['titulopagina'] = "Imagens do Site";
        parent::prepararView("Config/pag_imagens", $dados);
    }

    function Layout()
    {
        $dados['titulopagina'] = "Layout do Site";
        parent::prepararView("Config/pag_layout", $dados);
    }
}
