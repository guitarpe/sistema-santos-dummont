<?php

namespace Application\Controller\Config;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

require_once SYSTEM_PATH . 'vendor/santosdummont/vendor/autoload.php';

class Fabricantes extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Fabricantes Cadastrados";
        $dados['acaoimportar'] = SITE_URL . "/Fabricantes/Importar";
        $dados['listafabricante'] = Common::retornoWSLista($this->model->ListaFabricantesCadastrados($token));
        parent::prepararView("Config/pag_fabricante", $dados);
    }

    function Fabricante($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['fabricante'] = Common::retornoWSLista($this->model->DadosFabricante($token, $id))[0];
            $dados['urlaction'] = SITE_URL . "/Fabricantes/Editar";
            $dados['titulopagina'] = "Editar Fabricante";
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Fabricantes/Inserir";
            $dados['titulopagina'] = "Novo Fabricante";
        }
        parent::prepararView("Config/cad_fabricante", $dados);
    }

    function AtivaInativa($id, $tipo)
    {
        $dados = [
            'TOKEN' => Session::get('token'),
            'PAR_ID' => $id,
            'PAR_STATUS' => intval($tipo) == 1 ? 1 : 2,
        ];

        $cadastrar = $this->model->AtivaInativaFabricante($dados);

        if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $cadastrar['list'][0]['O_DESC_CURTO'];

            echo json_encode(['O_COD_RETORNO' => $cadastrar['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = intval($tipo) == 1 ? 'Parceiro Ativado com sucesso!' : 'Parceiro Inativado com sucesso!';

            echo json_encode(['O_COD_RETORNO' => $cadastrar['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $status = filter_input(INPUT_POST, "status");
        $descricao = filter_input(INPUT_POST, "descricao");
        $info = filter_input(INPUT_POST, "informacao");
        $exibir_site = filter_input(INPUT_POST, "exibir_site");

        $now = new \DateTime();

        $pasta = $now->format('Y_m_d_H_i_s');

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 183, 'altura' => 109);

                $ret = Common::salvarImagem($_FILES['foto'], FOTOPARCEIROS_URL . '/' . $pasta, $config);

                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'PAR_ID' => 0,
                'PAR_NOME' => $nome,
                'PAR_URL' => Common::removerEspacosPontos($nome),
                'PAR_STATUS' => $status,
                'PAR_DESCRICAO' => $descricao,
                'PAR_INFO' => $info,
                'PAR_EXIBIR_SITE' => $exibir_site,
                'PASTA_IMAGENS' => $pasta,
                'IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : null
            ];

            $cadastrar = $this->model->CadastrarEditarFabricante($dados);

            if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Fabricantes');
            } else {
                $msg = 'Fabricante cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Fabricantes');
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $status = filter_input(INPUT_POST, "status");
        $descricao = filter_input(INPUT_POST, "descricao");
        $info = filter_input(INPUT_POST, "informacao");
        $exibir_site = filter_input(INPUT_POST, "exibir_site");

        $now = new \DateTime();

        $token = Session::get('token');
        $fabricante = Common::retornoWSLista($this->model->DadosFabricante($token, $id))[0];

        $this->validarCamposObrigatorio($id, $fabricante['IMAGEM']);
        $pasta = empty($fabricante['PASTA_IMAGENS']) ? $now->format('Y_m_d_H_i_s') : $fabricante['PASTA_IMAGENS'];

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 183, 'altura' => 109);

                $ret = Common::salvarImagem($_FILES['foto'], FOTOPARCEIROS_URL . '/' . $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'PAR_ID' => $id,
                'PAR_NOME' => $nome,
                'PAR_URL' => Common::removerEspacosPontos($nome),
                'PAR_STATUS' => intval($status),
                'PAR_DESCRICAO' => $descricao,
                'PAR_INFO' => $info,
                'PAR_EXIBIR_SITE' => intval($exibir_site),
                'PASTA_IMAGENS' => $pasta,
                'IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : $fabricante['IMAGEM']
            ];

            $cadastrar = $this->model->CadastrarEditarFabricante($dados);

            if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Fabricantes');
            } else {
                $msg = 'Fabricante editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Fabricantes');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'PAR_ID' => $id
        ];

        $fabricante = Common::retornoWSLista($this->model->DadosFabricante($token, $id))[0];
        Common::removerImagem(FOTOPARCEIROS_URL . '/' . $fabricante['IMAGEM']);

        Common::remoteDir(FOTOPARCEIRO_PATH . DIRECTORY_SEPARATOR . $fabricante['PASTA_IMAGENS']);

        $deletar = Common::retornoWSLista($this->model->ExcluirFabricante($dados));

        if ($deletar[0]['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Fabricantes';
        } else {
            $msg = 'Fabricante deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Fabricantes';
        }
    }

    function Importar()
    {
        $token = Session::get('token');

        if (empty($_FILES['arquivo_importacao']['name'][0])) {
            $msg = 'É necessário selecionar um arquivo!';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Produtos');
        } else {

            $ret = Common::salvarArquivo($_FILES['arquivo_importacao']);
            $file = $ret['list']['documento'];

            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load(PASTAARQUIVOS_URL . '/' . $file);

            $campos = [];
            $importados = 0;
            $linhasimpo = 0;

            $naoimportados = [];

            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

                $linhas = [];

                foreach ($worksheet->getRowIterator() as $row) {
                    if ($row->getRowIndex() == 1) {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        foreach ($cellIterator as $cell) {
                            if (!is_null($cell)) {
                                array_push($campos, $cell->getCalculatedValue());
                            }
                        }
                    } else {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        $valores = [];
                        foreach ($cellIterator as $cell) {
                            $valor = $cell->getCalculatedValue();
                            array_push($valores, $valor);
                        }

                        if (!empty($valores[0])) {
                            $linhas['TOKEN'] = $token;
                            $linhas['PAR_ID'] = 0;
                            $linhas['PAR_STATUS'] = 0;
                            $linhas['IMAGEM'] = "";
                            $linhas['PASTA_IMAGENS'] = "";

                            foreach ($valores as $key => $value) {
                                $linhas[$campos[$key]] = $value;
                            }

                            $linhas['PAR_URL'] = strtolower(Common::removerEspacosPontos($linhas['PAR_NOME']));

                            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarFabricante($linhas));

                            if (intval($cadastrar[0]['O_COD_RETORNO']) == 0) {
                                $importados++;
                            } else {
                                array_push($naoimportados, $linhas['NOME_USER']);
                            }
                        }

                        $linhasimpo++;
                    }
                }
            }

            Common::removerArquivo($file);

            if ($importados > 0) {
                $msg = 'Foram importados ' . $importados . ' fabricantes com sucesso!';

                if (count($naoimportados) > 0) {
                    $msg .= '\nOs fabricantes ' . implode('\n', $naoimportados) . ' não foram importados.';
                }

                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Fabricantes');
            } else {
                $msg = 'Ocorreu algum erro ao importar os fabricantes';
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Fabricantes');
            }
        }
    }

    private function validarCamposObrigatorio($id = null, $img = null)
    {
        $dados['Nome do Fabricante'] = filter_input(INPUT_POST, "nome");
        $dados['Descrição'] = filter_input(INPUT_POST, "descricao");

        if (empty($id)) {
            $dados['Foto'] = $_FILES['foto']['name'];
        } else {
            if (empty($img)) {
                $dados['Foto'] = $_FILES['foto']['name'];
            }
        }

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Fabricantes/Fabricante/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Fabricantes/Fabricante');
        }
    }


}
