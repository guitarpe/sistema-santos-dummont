<?php
namespace Application\Controller\Estatisticas;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Estatisticas extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelRelatorios', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function Acessos()
    {
        $dados['titulopagina'] = "Acessos do Site";
        parent::prepararView("Estatisticas/pag_estatisticas", $dados);
    }

    function listAcessosLoginClientes()
    {
        $token = Session::get('token');

        echo json_encode(Common::retornoWSLista($this->model->ListaLoginsClientes($token)));
    }

    function listAcessosLoginUsuariosSistema()
    {
        $token = Session::get('token');

        echo json_encode(Common::retornoWSLista($this->model->ListaLoginUsuariosSistema($token)));
    }

    function listUltimasBuscas()
    {
        $token = Session::get('token');

        echo json_encode(Common::retornoWSLista($this->model->ListaUltimasBuscas($token)));
    }

    function listaAcessosCidades()
    {
        $token = Session::get('token');

        echo json_encode(Common::retornoWSLista($this->model->ListaAcessosCidades($token)));
    }
}
