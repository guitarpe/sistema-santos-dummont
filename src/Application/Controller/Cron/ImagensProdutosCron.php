<?php

namespace Application\Controller\Cron;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class ImagensProdutosCron extends Controller
{
    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCron', 'model');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }
    }

    function main()
    {

    }

    function VerificarPastasVazias(){
        $nofiles = ['.', '..'];
        ini_set('max_execution_time', 3000); //300 seconds = 5 minutes

        $token = Session::get('token');

        $dadoslista = Common::retornoWSLista($this->model->CronListaProdutos($token));

        foreach ($dadoslista as $row) {

            //verifica se a pasta atual é igual a que deve ser
            $pastaatual = $row['PASTA_IMAGENS'];


                //pega a pasta atual e a pasta correta
                $pasta = FOTOPRODUTO_URL.'/'.$pastaatual;

                //verifica se o diretorio foi criado
                if(is_dir($pasta)){

                    $file_headers = file_exists(FOTOPRODUTO_URL.'/'.$pastaatual . '/' . $row['PRD_IMAGEM_PRINCIPAL']);

                    if(!$file_headers){
                        //ATUALIZA A TABELA DE PRODUTOS
                        $dados = [
                            'PRD_ID' => $row['PRD_ID'],
                            'PASTA_IMAGENS' => null,
                            'PRD_IMAGEM_PRINCIPAL' => null
                        ];

                        Common::retornoWSLista($this->model->CronUpdateProdutos($dados));
                        echo '1 - PRODUTO '.$row['PRD_ID'].' PASTA E IMAGENS ATUALIZADAS<br/>';
                    }
                }
            /*}else{

                $pastaat = FOTOPRODUTO_URL.'/'.$pastaatual;

                $key = 0;

                 //cria a pasta correta
                mkdir($pastaat.'/large', 0777, true);

                //abre o diretório atual para copiar os arquivo para a nova pasta
                foreach (new \DirectoryIterator($pastaat.'/large') as $file) {

                    if(!in_array($file, $nofiles)){
                        $sku = $row['PRD_SKU'];

                        //verifica se o arquivo é igual ou tem algo igual ao sku
                        if($key == 0){
                            $imagem = $pastaat.'/large/'.$file;
                            $info = pathinfo($imagem);

                            //renomeia o arquivo já na nova pasta
                            rename($imagem, $pastaat.'/large/'.$sku.'.'.$info['extension']);

                            $imagemprincipal = $sku.'.'.$info['extension'];
                        }

                        $key++;
                    }
                }

                //ATUALIZA A TABELA DE PRODUTOS
                $dados = [
                    'PRD_ID' => $row['PRD_ID'],
                    'PASTA_IMAGENS' => $pastacorreta,
                    'PRD_IMAGEM_PRINCIPAL' => $imagemprincipal
                ];

                Common::retornoWSLista($this->model->CronUpdateProdutos($dados));
                echo '2 - PRODUTO '.$row['PRD_ID'].' PASTA E IMAGENS ATUALIZADAS<br/>';
            }*/
        }

        /*foreach ($dadoslista as $row) {
            $pasta = !empty($row['PASTA_IMAGENS']) ? FOTOPRODUTO_URL.'/'.$row['PASTA_IMAGENS'] : FOTOPRODUTO_URL.'/IMGS_PRD_ID_'.$row['PRD_ID'];

            if(is_dir($pasta)){
                if(Common::dir_is_empty($pasta)){

                    //ATUALIZA A TABELA DE PRODUTOS
                    /*$dados = [
                        'PRD_ID' => $row['PRD_ID'],
                        'PASTA_IMAGENS' => null,
                        'PRD_IMAGEM_PRINCIPAL' => null
                    ];

                    Common::retornoWSLista($this->model->CronUpdateProdutos($dados));
                    echo 'PRODUTO '.$row['PRD_ID'].' ATUALIZADO<br/>';

                    Common::removeDir($pasta);*/
                /*}else{
                    echo $pasta. "<br>";

                    $trecho = 'IMGS_PRD_ID';

                    if(strpos($pasta, $trecho) !== false){

                    } else{

                    }

                    if ($opendirectory = opendir($pasta.'/large')){
                        while (($file = readdir($opendirectory)) !== false){

                            if(!in_array($file, $nofiles)){
                                echo "filename:" . $file . "<br>";
                            }
                        }
                        closedir($opendirectory);
                    }
                    echo '---'. "<br>";
                }


            }else{
                /*$pasta_atual = $row['PASTA_IMAGENS'];
                $pasta_nova = 'IMGS_PRD_ID_'.$row['PRD_ID'];

                rename($pasta_atual, $pasta_nova);

                //ATUALIZA A TABELA DE PRODUTOS
                $dados = [
                    'PRD_ID' => $row['PRD_ID'],
                    'PASTA_IMAGENS' => $pasta_nova,
                    'PRD_IMAGEM_PRINCIPAL' => $row['PRD_IMAGEM_PRINCIPAL']
                ];

                Common::retornoWSLista($this->model->CronUpdateProdutos($dados));
                echo 'PRODUTO '.$row['PRD_ID'].' ATUALIZADO<br/>';*/

                /*echo $pasta. "<br>";
                if ($opendirectory = opendir($pasta.'/large')){
                    while (($file = readdir($opendirectory)) !== false){

                        if(!in_array($file, $nofiles)){
                            echo "filename:" . $file . "<br>";
                        }
                    }
                    closedir($opendirectory);
                }
                echo '---'. "<br>";
            }
        }*/
    }
}
