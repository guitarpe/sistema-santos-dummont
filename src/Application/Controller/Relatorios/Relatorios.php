<?php

namespace Application\Controller\Relatorios;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Relatorios extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelRelatorios', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    public function main()
    {
        parent::prepararView("Relatorios/pag_relatorios");
    }

    public function Usuarios()
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        $dados['titulopagina'] = "Relatório de Usuários Cadastrados";
        $dados['listusuarios'] = Common::retornoWSLista($this->model->ListaRelatorioUsuarios($token));
        parent::prepararView("Relatorios/pag_relatorios_usuarios", $dados);
    }

    public function Clientes()
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        $dados['titulopagina'] = "Relatório de Clientes Cadastrados";
        $dados['listaclientes'] = Common::retornoWSLista($this->model->ListaRelatorioClientes($token));
        parent::prepararView("Relatorios/pag_relatorios", $dados);
    }

    public function Acessos()
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        $dados['titulopagina'] = "Relatório de Acessos do Site";
        $dados['listacessos'] = Common::retornoWSLista($this->model->ListaRelatorioAcessos($token));
        parent::prepararView("Relatorios/pag_relatorios_acessos", $dados);
    }

    public function Vendas()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Relatório de Vendas Realizadas";
        $dados['listapedidos'] = Common::retornoWSLista($this->model->ListaPedidosCadastrados($token));
        parent::prepararView("Relatorios/pag_relatorios_vendas", $dados);
    }

    public function Produtos()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Relatório de Produtos em Estoque";
        $dados['listaprodutos'] = Common::retornoWSLista($this->model->ListaProdutosCadastrados($token));
        parent::prepararView("Relatorios/pag_relatorios_produtos", $dados);
    }
}
