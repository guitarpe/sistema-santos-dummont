<?php

namespace Application\Controller\Estoque;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session,
    santosdummont\PHPExcel;

class Importacao extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCron', 'model');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $dados['titulopagina'] = "Produtos Cadastrados";
        $dados['acaoimportar'] = SITE_URL . "/Produtos/Importar";
        parent::prepararView("Estoque/pag_produtos", $dados);
    }

    function TesteSelecionarImagens()
    {

        ini_set('max_execution_time', 3000); //300 seconds = 5 minutes

        $token = Session::get('token');

        $dadoslista = Common::retornoWSLista($this->model->TesteSelecionarImagens($token));

        $retn = [];

        //copia as imagens
        foreach ($dadoslista as $row) {

            $imagemsplit = explode("/", $row['IMAGEM']);
            $imagem = end($imagemsplit);
        }
    }

    function TesteImagensThumbs()
    {

        ini_set('max_execution_time', 3000); //300 seconds = 5 minutes

        $token = Session::get('token');

        $dadoslista = Common::retornoWSLista($this->model->TesteImportacaoImagens($token));

        $retn = [];

        //copia as imagens
        foreach ($dadoslista as $row) {

            $imagemsplit = explode("/", $row['IMAGEM']);
            $imagem = end($imagemsplit);

            if (!empty($row['IMAGEM'])) {
                if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/50x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/50x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/thumb/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/50x' . $row['IMAGEM'] . ' - Copiado');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/50x' . $row['IMAGEM'] . ' - Erro');
                    }
                } else if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/56x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/56x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/thumb/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/56x' . $row['IMAGEM'] . ' - Copiada');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/56x' . $row['IMAGEM'] . ' - Erro');
                    }
                } else if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/75x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/75x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/thumb/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/75x' . $row['IMAGEM'] . ' - Copiado');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/75x' . $row['IMAGEM'] . ' - Erro');
                    }
                } else if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/80x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/80x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/thumb/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/80x' . $row['IMAGEM'] . ' - Copiado');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/80x' . $row['IMAGEM'] . ' - Erro');
                    }
                } else if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/100x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/100x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/thumb/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/100x' . $row['IMAGEM'] . ' - Copiado');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/100x' . $row['IMAGEM'] . ' - Erro');
                    }
                } else if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/135x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/135x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/thumb/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/135x' . $row['IMAGEM'] . ' - Copiado');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/135x' . $row['IMAGEM'] . ' - Erro');
                    }
                } else if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/150x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/thumb', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/150x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/thumb/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/150x' . $row['IMAGEM'] . ' - Copiada');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/150x' . $row['IMAGEM'] . ' - Erro');
                    }
                }
            }
        }

        echo '<pre>';
        print_r($retn);
        echo '</pre>';
    }

    function TesteImagensMiddle()
    {

        ini_set('max_execution_time', 3000); //300 seconds = 5 minutes

        $token = Session::get('token');

        $dadoslista = Common::retornoWSLista($this->model->TesteImportacaoImagens($token));

        $retn = [];

        //copia as imagens
        foreach ($dadoslista as $row) {

            $imagemsplit = explode("/", $row['IMAGEM']);
            $imagem = end($imagemsplit);

            if (!empty($row['IMAGEM'])) {
                if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/250x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/middle')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/middle', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/250x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/middle/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/350x' . $row['IMAGEM'] . ' - Copiado');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/350x' . $row['IMAGEM'] . ' - Erro');
                    }
                } else if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/300x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/middle')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/middle', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/300x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/middle/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/350x' . $row['IMAGEM'] . ' - Copiado');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/350x' . $row['IMAGEM'] . ' - Erro');
                    }
                } else if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/350x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/middle')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/middle', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/350x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/middle/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/350x' . $row['IMAGEM'] . ' - Copiado');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/350x' . $row['IMAGEM'] . ' - Erro');
                    }
                }
            }
        }

        echo '<pre>';
        print_r($retn);
        echo '</pre>';
    }

    function TesteImagensLarge()
    {

        ini_set('max_execution_time', 3000); //300 seconds = 5 minutes

        $token = Session::get('token');

        $dadoslista = Common::retornoWSLista($this->model->TesteImportacaoImagens($token));

        $retn = [];

        //copia as imagens
        foreach ($dadoslista as $row) {

            $imagemsplit = explode("/", $row['IMAGEM']);
            $imagem = end($imagemsplit);

            if (!empty($row['IMAGEM'])) {
                if (file_exists(FOTOPRODUTO_PATH . '/1/thumbnail/600x' . $row['IMAGEM'])) {

                    if (!file_exists(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/large')) {
                        mkdir(FOTOPRODUTO_URL . '/' . $row['PASTA_IMAGENS'] . '/large', 0777, true);
                    }

                    if (copy(FOTOPRODUTO_PATH . '/1/thumbnail/600x' . $row['IMAGEM'], FOTOPRODUTO_PATH . '/' . $row['PASTA_IMAGENS'] . '/large/' . $imagem)) {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/600x' . $row['IMAGEM'] . ' - Copiado');
                    } else {
                        array_push($retn, FOTOPRODUTO_PATH . '/1/thumbnail/600x' . $row['IMAGEM'] . ' - Erro');
                    }
                }
            }
        }

        echo '<pre>';
        print_r($retn);
        echo '</pre>';
    }
}
