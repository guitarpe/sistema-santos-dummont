<?php

namespace Application\Controller\Estoque;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

require_once SYSTEM_PATH . 'vendor/santosdummont/vendor/autoload.php';

class Produtos extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelProdutos', 'model');
        parent::loadModel('Application\Model\ModelCron', 'modelcron');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $dados['titulopagina'] = "Produtos Cadastrados";
        $dados['acaoimportar'] = SITE_URL . "/Produtos/Importar";
        $dados['urlsync'] = SITE_URL . "/Produtos/SincronizarProdutos";
        parent::prepararView("Estoque/pag_produtos", $dados);
    }

    function ListaProdutos()
    {
        $token = Session::get('token');

        $data = [];
        $params = $_REQUEST;

        $columns = array(
            0 => 'PRD_ID',
            1 => 'PRD_NOME',
            2 => 'PRD_STATUS',
            3 => 'PRD_QTDE_ESTOQUE',
            4 => 'PRD_SKU',
            5 => 'EM_ESTOQUE',
            6 => 'CATEGORIAS_NOMES'
        );

        $parametros['nome'] = null;
        $parametros['catnome'] = null;
        $parametros['status'] = 3;
        $parametros['estoque'] = null;
        $parametros['sku'] = null;

        if (!empty($params['columns'][1]['search']['value'])) {
            $parametros['nome'] = $params['columns'][1]['search']['value'];
        }

        if (!empty($params['columns'][2]['search']['value']) && intval($params['columns'][2]['search']['value']) != 3) {
            $parametros['status'] = $params['columns'][2]['search']['value'];
        }

        if (!empty($params['columns'][4]['search']['value'])) {
            $parametros['sku'] = $params['columns'][4]['search']['value'];
        }

        if (!empty($params['columns'][5]['search']['value']) && intval($params['columns'][5]['search']['value']) != 2) {
            $parametros['estoque'] = $params['columns'][5]['search']['value'];
        }

        if (!empty($params['columns'][6]['search']['value'])) {
            $parametros['catnome'] = $params['columns'][6]['search']['value'];
        }

        $parametros['token'] = $token;
        $parametros['order'] = $columns[$params['order'][0]['column']];
        $parametros['dir'] = $params['order'][0]['dir'];
        $parametros['start'] = $params['start'];
        $parametros['length'] = $params['length'];

        $list = Common::retornoWSLista($this->model->ListaProdutosCadastrados($parametros));

        $totalgeral = 0;
        $totalfiltrado = 0;

        foreach ($list as $prod) {
            $row = array();
            $row[] = $prod['PRD_ID'];
            $row[] = $prod['PRD_NOME'];
            $row[] = $prod['PRD_STATUS'];
            $row[] = $prod['PRD_QTDE_ESTOQUE'];
            $row[] = $prod['PRD_SKU'];
            $row[] = null;
            $row[] = $prod['EM_ESTOQUE'];
            $row[] = $prod['CATEGORIAS_NOMES'];

            $data[] = $row;

            $totalgeral = intval($prod['TOTALGERAL']);
            $totalfiltrado = intval($prod['TOTAL']);
        }

        $output = [
            "draw" => intval($params['draw']),
            "recordsTotal" => $totalgeral,
            "recordsFiltered" => $totalfiltrado,
            "data" => $data
        ];

        echo json_encode($output);
    }

    function Produto($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        $dados['listacategorias'] = Common::retornoWSLista($this->model->ListaCategoriasProdutos($token));
        $dados['listafabricantes'] = Common::retornoWSLista($this->model->ListaFabricantesCadastrados($token));

        unset($dados['menu']);
        unset($dados['notificacoes']);
        unset($dados['config']);

        if (!empty($id)) {
            $dados['acaobotao'] = 'Salvar Edição';
            $dados['produto'] = Common::retornoWSLista($this->model->DadosProduto($token, $id))[0];
            $dados['listacategoriasproduto'] = Common::retornoWSLista($this->model->ListaCategoriasdoProduto($token, $id));
            $dados['fotosproduto'] = Common::retornoWSLista($this->model->FotosProduto($token, $id));

            $dados['urlaction'] = SITE_URL . "/Produtos/Editar";
            $dados['titulopagina'] = "Editar Produto";
            $dados['urlsync'] = SITE_URL . "/Produtos/SincronizarProdutos/" . $dados['produto']['PRD_SKU'];

            parent::prepararView("Estoque/edit_produto", $dados);
        } else {
            $dados['acaobotao'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Produtos/Inserir";
            $dados['titulopagina'] = "Novo Produto";

            parent::prepararView("Estoque/cad_produto", $dados);
        }
    }

    function AtivaInativa($id, $tipo)
    {
        $dados = [
            'TOKEN' => Session::get('token'),
            'PRD_ID' => $id,
            'PRD_STATUS' => intval($tipo) == 1 ? 1 : 2,
        ];

        $cadastrar = $this->model->AtivaInativaProduto($dados);

        if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $cadastrar['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $cadastrar['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = intval($tipo) == 1 ? 'Produto Ativado com sucesso!' : 'Produto Inativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $cadastrar['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function BuscaBaseCetrus()
    {
        $token = Session::get('token');

        $sku = filter_input(INPUT_POST, "sku");
        $params['sku'] = $sku;

        $produto = $this->model->ProdutoExisteSKU($token, $sku);

        if ($produto['list'][0]['O_COD_RETORNO'] != 0) {
            $dados = [];
            $msg = $produto['list'][0]['O_DESC_CURTO'];
            $codigo = $produto['list'][0]['O_COD_RETORNO'];
        } else {

            if (intval($produto['list'][0]['V_TOTAL']) == 0) {

                $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];
                $params['ip'] = $config['@IP_INTEGRACAO'];

                $ret = json_decode(parent::consumir_cetrus($params), true);

                if (!empty($ret) && !empty($ret['produtos'])) {

                    $status = 2;

                    if (intval($ret['erro']) == 0) {

                        if ($ret['produtos'][0]['desativado'] == 'S') {
                            $status = 2;
                        }

                        $dados = [
                            'PRD_SKU' => $ret['produtos'][0]['produto_Id'],
                            'PRD_UNIDADE' => $ret['produtos'][0]['und_Est'],
                            'PRD_NOME' => $ret['produtos'][0]['discr'],
                            'PRD_QTDE_ESTOQUE' => $ret['produtos'][0]['qtde_Estoque'],
                            'PRD_PRECO' => number_format((float) $ret['produtos'][0]['preco'], 2, '.', ''),
                            'PRD_MARCA' => $ret['produtos'][0]['marca'],
                            'PRD_COD_CETRUS' => $ret['produtos'][0]['marca_Id'],
                            'PRD_PESO' => $ret['produtos'][0]['peso_Bruto'],
                            'PRD_ALTURA' => $ret['produtos'][0]['altura'],
                            'PRD_LARGURA' => $ret['produtos'][0]['largura'],
                            'PRD_STATUS' => $status,
                            'PRD_COMPRIMENTO' => $ret['produtos'][0]['comprimento'],
                            'PRD_PROFUNFIDADE' => $ret['produtos'][0]['profundidade']
                        ];

                        $msg = 'Produto encontrado com sucesso!';
                        $codigo = 0;
                    } else {
                        $dados = [];
                        $msg = $ret['produtos'][0]['erroMsg'];
                        $codigo = 1;
                    }
                } else {
                    $dados = [];
                    $msg = 'Produto não encontrado na base da Cetrus!';
                    $codigo = 1;
                }
            } else {
                $dados = [];
                $msg = 'Produto já cadastrado no sistema!';
                $codigo = 1;
            }
        }

        echo json_encode(['O_COD_RETORNO' => $codigo, 'O_DESC_CURTO' => $msg, 'DADOS' => $dados]);
    }

    function SincronizarProdutos($sku = null, $tipo = null)
    {
        $token = Session::get('token');

        if (!empty($sku)) {
            $params['sku'] = $sku;

            $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];
            $params['ip'] = $config['@IP_INTEGRACAO'];

            $ret = json_decode(parent::consumir_cetrus($params), true);

            if (!empty($ret) && !empty($ret['produtos'])) {
                if (intval($ret['erro']) == 0) {
                    $dados = [
                        'I_TOKEN' => $token,
                        'I_PRD_SKU' => $ret['produtos'][0]['produto_Id'],
                        'I_PRD_UNIDADE' => $ret['produtos'][0]['und_Est'],
                        'I_PRD_NOME' => $ret['produtos'][0]['discr'],
                        'I_PRD_URL' => Common::removerEspacosPontos($ret['produtos'][0]['discr']),
                        'I_PRD_QTDE_ESTOQUE' => $ret['produtos'][0]['qtde_Estoque'],
                        'I_PRD_PRECO' => number_format((float) $ret['produtos'][0]['preco'], 2, '.', ''),
                        'I_PRD_COD_CETRUS' => $ret['produtos'][0]['marca_Id'],
                        'I_PRD_MARCA' => $ret['produtos'][0]['marca'],
                        'I_PRD_MARCA_URL' => Common::removerEspacosPontos($ret['produtos'][0]['marca']),
                        'I_PRD_PESO' => (string)$ret['produtos'][0]['peso_Bruto'],
                        'I_PRD_ALTURA' => (string)$ret['produtos'][0]['altura'],
                        'I_PRD_LARGURA' => (string)$ret['produtos'][0]['largura'],
                        'I_PRD_STATUS' => $ret['produtos'][0]['desativado'],
                        'I_PRD_COMPRIMENTO' => (string)$ret['produtos'][0]['comprimento'],
                        'I_PRD_PROFUNFIDADE' => (string)$ret['produtos'][0]['profundidade']
                    ];

                    $cadastrar = $this->model->AtualizaAPIProduto($dados);

                    if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
                        $msg = $cadastrar['list'][0]['O_DESC_CURTO'];
                        $situacao = 'danger';
                        $codigo = $cadastrar['list'][0]['O_COD_RETORNO'];
                    } else {

                        $consumir = parent::consulta_cetrus($params);

                        if (intval($consumir['erro']) == 0) {
                            if (intval($consumir["retorno"][0]['retorno']) != 1) {
                                $msg = 'O produto sincronizado com sucesso, porém não foi removido da lista da cetrus';
                                $situacao = 'warning';
                                $codigo = $cadastrar['list'][0]['O_COD_RETORNO'];

                                parent::log_registra('Integração - Erro ao Remover da lista: Produto: ' . $ret['produtos'][0]['produto_Id'] . ' - ' . $consumir["retorno"]['error'], null, false);
                            } else {
                                $msg = 'Produto sincronizado com sucesso!';
                                $situacao = 'success';
                                $codigo = $cadastrar['list'][0]['O_COD_RETORNO'];
                            }
                        }
                    }
                } else {
                    $msg = $ret['produtos'][0]['erroMsg'];
                    $situacao = 'danger';
                    $codigo = $ret['erro'];
                }
            } else {

                $produto = $this->model->ProdutoExisteSKU($token, $sku);
                $infoprd = [
                    'TOKEN' => $token,
                    'PRD_ID' => intval($produto['list'][0]['V_PRD_ID']),
                    'PRD_STATUS' => 2,
                ];

                $atualizado = $this->model->AtivaInativaProduto($infoprd);

                $msg = 'Produto não encontrado ou com informação zeradas na base da Cetrus!';
                $situacao = 'warning';
                $codigo = 0;
            }
        } else {
            self::AtualizarTodos();

            $msg = 'O produtos executaram em segundo plano';
            $situacao = 'success';
            $codigo = 0;
        }

        if (!empty($sku)) {
            if (!empty($tipo)) {
                echo json_encode(['O_COD_RETORNO' => $codigo, 'O_DESC_CURTO' => $msg, 'SITUACAO' => $situacao]);
            } else {

                $produto = $this->model->ProdutoExisteSKU($token, $sku);
                $id = intval($produto['list'][0]['V_PRD_ID']);

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Produtos/Produto/' . $id);
            }
        } else {
            Common::alert($msg, $situacao, 'acao');
            Common::redir('Produtos');
        }
    }

    function SincronizarProdutosCron()
    {
        try {

            set_time_limit(7200);
            ignore_user_abort(true);

            $token = Session::get('token');
            $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];

            $params['ip'] = $config['@IP_INTEGRACAO'];

            $list = json_decode(parent::consumir_cetrus($params), true);

            foreach ($list as $dados) {

                if (!empty($list) || intval($list['erro']) == 0 || !empty($list['produtos'])) {
                    $parametros = [
                        'I_PRD_SKU' => $dados['produtos'][0]['produto_Id'],
                        'I_PRD_UNIDADE' => $dados['produtos'][0]['und_Est'],
                        'I_PRD_NOME' => $dados['produtos'][0]['discr'],
                        'I_PRD_URL' => Common::removerEspacosPontos($dados['produtos'][0]['discr']),
                        'I_PRD_QTDE_ESTOQUE' => $dados['produtos'][0]['qtde_Estoque'],
                        'I_PRD_PRECO' => number_format((float) $dados['produtos'][0]['preco'], 2, '.', ''),
                        'I_PRD_COD_CETRUS' => $dados['produtos'][0]['marca_Id'],
                        'I_PRD_MARCA' => $dados['produtos'][0]['marca'],
                        'I_PRD_MARCA_URL' => Common::removerEspacosPontos($dados['produtos'][0]['marca']),
                        'I_PRD_PESO' => (string)$dados['produtos'][0]['peso_Bruto'],
                        'I_PRD_ALTURA' => (string)$dados['produtos'][0]['altura'],
                        'I_PRD_LARGURA' => (string)$dados['produtos'][0]['largura'],
                        'I_PRD_STATUS' => $dados['produtos'][0]['desativado'],
                        'I_PRD_COMPRIMENTO' => (string)$dados['produtos'][0]['comprimento'],
                        'I_PRD_PROFUNFIDADE' => (string)$dados['produtos'][0]['profundidade']
                    ];

                    $atualiza = $this->model->AtualizaAPIProdutoCron($parametros);

                    $params['sku'] = $dados['produtos'][0]['produto_Id'];

                    if (intval($atualiza['list'][0]['O_COD_RETORNO']) == 0) {

                        $consumir = parent::consulta_cetrus($params);

                        if (intval($consumir["retorno"][0]['retorno']) != 1) {
                            $dados['produtos'][0]['produto_Id'] . 'Atualizado mas não removido da lista';

                            parent::log_registra('Integração - Erro ao Remover da lista: Produto: ' . $dados['produtos'][0]['produto_Id'] . ' - ' . $consumir["retorno"]['error'], null, false);
                        } else {
                            echo $dados['produtos'][0]['produto_Id'] . ' Atualizado';
                        }
                    } else {
                        echo $dados['produtos'][0]['produto_Id'] . ' Erro';
                    }
                } else {
                    echo $dados['produtos'][0]['produto_Id'] . ' Inexistente';
                }
            }
        } catch (\Exception $ex) {
            self::log_registra('Integração', 'Exception', false);
        }
    }

    private function AtualizarTodos()
    {
        set_time_limit(7200);
        ignore_user_abort(true);

        $token = Session::get('token');

        $list = Common::retornoWSLista($this->model->ListaProdutosAPI($token));

        foreach ($list as $dados) {

            $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];
            $params['ip'] = $config['@IP_INTEGRACAO'];

            $params['sku'] = $dados['PRD_SKU'];
            $ret = json_decode(parent::consumir_cetrus($params), true);

            if (!empty($ret) || intval($ret['erro']) == 0 || !empty($list['produtos'])) {
                $parametros = [
                    'I_TOKEN' => $token,
                    'I_PRD_SKU' => $dados['produtos'][0]['produto_Id'],
                    'I_PRD_UNIDADE' => $dados['produtos'][0]['und_Est'],
                    'I_PRD_NOME' => $dados['produtos'][0]['discr'],
                    'I_PRD_URL' => Common::removerEspacosPontos($dados['produtos'][0]['discr']),
                    'I_PRD_QTDE_ESTOQUE' => $dados['produtos'][0]['qtde_Estoque'],
                    'I_PRD_PRECO' => number_format((float) $dados['produtos'][0]['preco'], 2, '.', ''),
                    'I_PRD_COD_CETRUS' => $dados['produtos'][0]['marca_Id'],
                    'I_PRD_MARCA' => $dados['produtos'][0]['marca'],
                    'I_PRD_MARCA_URL' => Common::removerEspacosPontos($dados['produtos'][0]['marca']),
                    'I_PRD_PESO' => (string)$dados['produtos'][0]['peso_Bruto'],
                    'I_PRD_ALTURA' => (string)$dados['produtos'][0]['altura'],
                    'I_PRD_LARGURA' => (string)$dados['produtos'][0]['largura'],
                    'I_PRD_STATUS' => $dados['produtos'][0]['desativado'],
                    'I_PRD_COMPRIMENTO' => (string)$dados['produtos'][0]['comprimento'],
                    'I_PRD_PROFUNFIDADE' => (string)$dados['produtos'][0]['profundidade']
                ];

                $atualiza = $this->model->AtualizaAPIProduto($parametros);

                if (intval($atualiza['list'][0]['O_COD_RETORNO']) == 0) {
                    $consumir = parent::consulta_cetrus($params);

                    if (!empty($consumir["retorno"]['error'])) {
                        parent::log_registra('Integração - Erro ao Remover da lista: Produto: ' . $ret['produtos'][0]['produto_Id'] . ' - ' . $consumir["retorno"]['error'], null, false);
                    }
                }
            }
        }
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio(null, null);

        $submit = filter_input(INPUT_POST, "submit");

        $token = Session::get('token');

        $nome = filter_input(INPUT_POST, "nome");
        $url_prd = strtolower(Common::removerEspacosPontos($nome));
        $sku = filter_input(INPUT_POST, "sku");
        $unidade = filter_input(INPUT_POST, "unidade");
        $status = filter_input(INPUT_POST, "status");
        $categorias = filter_input(INPUT_POST, "categorias", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $preco = Common::returnValor(filter_input(INPUT_POST, "preco"));
        $preco_anterior = Common::returnValor(filter_input(INPUT_POST, "preco_anterior"));
        $taxa = Common::returnValor(filter_input(INPUT_POST, "taxa"));
        $destaque = filter_input(INPUT_POST, "destaque");
        $qtde_estoque = filter_input(INPUT_POST, "qtde_estoque");
        $qtde_minima = filter_input(INPUT_POST, "qtde_minima");
        $nome_tecnico = filter_input(INPUT_POST, "nome_tecnico");
        $modelo = filter_input(INPUT_POST, "modelo");
        $descricao = filter_input(INPUT_POST, "descricao");
        $ficha_tecnica = filter_input(INPUT_POST, "ficha_tecnica");
        $marca = filter_input(INPUT_POST, "marca");
        $peso = filter_input(INPUT_POST, "peso");
        $comprimento = filter_input(INPUT_POST, "comprimento");
        $profundidade = filter_input(INPUT_POST, "profundidade");
        $largura = filter_input(INPUT_POST, "largura");
        $altura = filter_input(INPUT_POST, "altura");
        $voltagem = filter_input(INPUT_POST, "voltagem");
        $cor = filter_input(INPUT_POST, "cor");
        $tags = filter_input(INPUT_POST, "tags");
        $parceiro = filter_input(INPUT_POST, "parceiro");
        $imagens = filter_input(INPUT_POST, "imagens");

        $catgs = empty($categorias) ? null : implode(',', $categorias);

        if (isset($submit)) {
            $config = [
                'tamanho' => 4999999,
                'largura' => 800,
                'altura' => 700
            ];

            if (is_array($_FILES['fotos']['name'])) {
                $fotos = Common::reArrayFiles($_FILES['fotos']);
            } else {
                $fotos = $_FILES['fotos'];
            }

            if (count($fotos) > 0) {
                foreach ($fotos as $file) {
                    $valida = Common::validarImagem($file, $config);

                    if (!empty($valida)) {
                        array_push($ret, $valida);
                    }
                }
            }

            if (empty($ret)) {
                $imagens = [];

                if (count($fotos) > 0) {
                    foreach ($fotos as $file) {
                        array_push($imagens, $file['name']);
                    }
                }

                $dados = [
                    'TOKEN' => $token,
                    'PRD_ID' => 0,
                    'PRD_DESTAQUE' => $destaque,
                    'CATEGORIAS' => $catgs,
                    'PRD_NOME' => $nome,
                    'PRD_PATH' => $url_prd,
                    'PRD_STATUS' => $status,
                    'PRD_UNIDADE' => $unidade,
                    'PRD_QTDE_ESTOQUE' => empty($qtde_estoque) ? 0 : $qtde_estoque,
                    'PRD_QTDE_MINIMA' => empty($qtde_minima) ? 0 : $qtde_minima,
                    'PRD_SKU' => $sku,
                    'PRD_PRECO' => empty($preco) ? 0 : $preco,
                    'PRD_PRECO_ANTERIOR' => empty($preco_anterior) ? 0 : $preco_anterior,
                    'PRD_TAXA' => empty($taxa) ? 0 : $taxa,
                    'PRD_MODELO' => $modelo,
                    'PRD_NOME_TECNICO' => $nome_tecnico,
                    'PRD_FICHA_TECNICA' => $ficha_tecnica,
                    'PRD_DESCRICAO_MINIMA' => $descricao,
                    'PRD_MARCA' => $marca,
                    'PRD_PESO' => $peso,
                    'PRD_LARGURA' => $largura,
                    'PRD_ALTURA' => $altura,
                    'PRD_COMPRIMENTO' => $comprimento,
                    'PRD_PROFUNDIDADE' => $profundidade,
                    'PRD_VOLTAGEM' => $voltagem,
                    'PRD_COR' => $cor,
                    'PRD_TAGS' => $tags,
                    'PAR_ID' => $parceiro,
                    'PASTA_IMAGENS' => '',
                    'IMAGENS' => implode(',', $imagens)
                ];

                $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarProduto($dados));

                //$idproduto = $cadastrar[0]['O_PRD_ID'];
                $pasta = $cadastrar[0]['O_PASTA_IMAGENS'];

                if (intval($cadastrar[0]['O_COD_RETORNO']) != 0) {
                    echo json_encode(['erro' => 1, 'imgerr' => '', 'retorno' => $cadastrar[0]['O_DESC_CURTO'], 'page' => SITE_URL . '/Produtos']);
                } else {

                    if (count($fotos) > 0) {
                        foreach ($fotos as $file) {
                            Common::salvarImagem($file, FOTOPRODUTO_URL . '/' . $pasta.'/large', $config);
                        }
                    }

                    /*atualiza a imagem principal do produto e sua pasta
                    $parametros = [
                        'PRD_ID' => $idproduto,
                        'PASTA_IMAGENS' => $pasta,
                        'PRD_IMAGEM_PRINCIPAL' => !empty($imagens[0]) ? $imagens[0] : ''
                    ];

                    $this->modelcron->CronUpdateProdutos($parametros);*/

                    echo json_encode(['erro' => 0, 'imgerr' => '', 'retorno' => 'Produto cadastrado com sucesso!', 'page' => SITE_URL . '/Produtos']);
                }
            } else {
                echo json_encode(['erro' => 1, 'imgerr' => implode(',', $ret), 'retorno' => 'Ocorreram os seguintes erros:', 'page' => SITE_URL . '/Produtos']);
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");
        $tipoform = intval(filter_input(INPUT_POST, "form_tipo"));
        $submit = filter_input(INPUT_POST, "submit");

        $token = Session::get('token');

        if ($tipoform == 1) {

            $this->validarCamposObrigatorio($id, $tipoform);

            $nome = filter_input(INPUT_POST, "nome");
            $url_prd = strtolower(Common::removerEspacosPontos($nome));
            $sku = filter_input(INPUT_POST, "sku");
            $unidade = filter_input(INPUT_POST, "unidade");
            $status = filter_input(INPUT_POST, "status");
            $categorias = filter_input(INPUT_POST, "categorias", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $preco = Common::returnValor(filter_input(INPUT_POST, "preco"));
            $preco_anterior = Common::returnValor(filter_input(INPUT_POST, "preco_anterior"));
            $taxa = Common::returnValor(filter_input(INPUT_POST, "taxa"));
            $qtde_estoque = filter_input(INPUT_POST, "qtde_estoque");
            $qtde_minima = filter_input(INPUT_POST, "qtde_minima");
            $destaque = filter_input(INPUT_POST, "destaque");

            $catgs = empty($categorias) ? null : implode(',', $categorias);

            Session::set('tab_edit', $tipoform);

            if (isset($submit)) {
                $dados = [
                    'TOKEN' => $token,
                    'PRD_ID' => $id,
                    'CATEGORIAS' => $catgs,
                    'PRD_DESTAQUE' => $destaque,
                    'PRD_SKU' => $sku,
                    'PRD_UNIDADE' => $unidade,
                    'PRD_NOME' => $nome,
                    'PRD_PATH' => $url_prd,
                    'PRD_STATUS' => $status,
                    'PRD_QTDE_ESTOQUE' => empty($qtde_estoque) ? 0 : $qtde_estoque,
                    'PRD_QTDE_MINIMA' => empty($qtde_minima) ? 0 : $qtde_minima,
                    'PRD_PRECO' => empty($preco) ? 0 : $preco,
                    'PRD_PRECO_ANTERIOR' => empty($preco_anterior) ? 0 : $preco_anterior,
                    'PRD_TAXA' => empty($taxa) ? 0 : $taxa
                ];

                $editar = Common::retornoWSLista($this->model->CadastrarEditarProduto($dados, $tipoform));

                if ($editar[0]['O_COD_RETORNO'] != 0) {
                    $msg = $editar[0]['O_DESC_CURTO'];
                    $situacao = 'danger';

                    Common::alert($msg, $situacao, 'acao');
                    Common::redir('Produtos/Produto/' . $id);
                } else {
                    $msg = 'Produto editado com sucesso!';
                    $situacao = 'success';

                    Common::alert($msg, $situacao, 'acao');
                    Common::redir('Produtos/Produto/' . $id);
                }
            }
        }

        if ($tipoform == 2) {
            $this->validarCamposObrigatorio($id, $tipoform);
            $nome_tecnico = filter_input(INPUT_POST, "nome_tecnico");
            $modelo = filter_input(INPUT_POST, "modelo");
            $descricao = filter_input(INPUT_POST, "descricao");
            $ficha_tecnica = filter_input(INPUT_POST, "ficha_tecnica");
            $marca = filter_input(INPUT_POST, "marca");
            $peso = filter_input(INPUT_POST, "peso");
            $comprimento = filter_input(INPUT_POST, "comprimento");
            $profundidade = filter_input(INPUT_POST, "profundidade");
            $largura = filter_input(INPUT_POST, "largura");
            $altura = filter_input(INPUT_POST, "altura");
            $voltagem = filter_input(INPUT_POST, "voltagem");
            $cor = filter_input(INPUT_POST, "cor");
            $tags = filter_input(INPUT_POST, "tags");
            $parceiro = filter_input(INPUT_POST, "parceiro");

            Session::set('tab_edit', $tipoform);

            if (isset($submit)) {

                $dados = [
                    'TOKEN' => $token,
                    'PRD_ID' => $id,
                    'PRD_MODELO' => $modelo,
                    'PRD_NOME_TECNICO' => $nome_tecnico,
                    'PRD_DESCRICAO_MINIMA' => $descricao,
                    'PRD_FICHA_TECNICA' => $ficha_tecnica,
                    'PRD_MARCA' => $marca,
                    'PRD_PESO' => $peso,
                    'PRD_LARGURA' => $largura,
                    'PRD_ALTURA' => $altura,
                    'PRD_COMPRIMENTO' => $comprimento,
                    'PRD_PROFUNDIDADE' => $profundidade,
                    'PRD_VOLTAGEM' => $voltagem,
                    'PRD_COR' => $cor,
                    'PRD_TAGS' => $tags,
                    'PAR_ID' => empty($parceiro) ? 0 : $parceiro
                ];

                $editar = Common::retornoWSLista($this->model->CadastrarEditarProduto($dados, $tipoform));

                if ($editar[0]['O_COD_RETORNO'] != 0) {
                    $msg = $editar[0]['O_DESC_CURTO'];
                    $situacao = 'danger';

                    Common::alert($msg, $situacao, 'acao');
                    Common::redir('Produtos/Produto/' . $id);
                } else {
                    $msg = 'Produto editado com sucesso!';
                    $situacao = 'success';

                    Common::alert($msg, $situacao, 'acao');
                    Common::redir('Produtos/Produto/' . $id);
                }
            }
        }

        if ($tipoform == 3) {

            $config = [
                'tamanho' => 4999999,
                'largura' => 800,
                'altura' => 700
            ];

            $imagens = [];
            $erros = [];
            $imgerr = [];
            $ret = [];

            $campoimagens = filter_input(INPUT_POST, "imagens");
            $pasta_imagens = filter_input(INPUT_POST, "pasta_imagens");

            Session::set('tab_edit', $tipoform);

            if (isset($submit)) {
                $imagens = explode(',', $campoimagens);

                if (is_array($_FILES['fotos']['name'])) {
                    $fotos = Common::reArrayFiles($_FILES['fotos']);
                } else {
                    $fotos = $_FILES['fotos'];
                }

                if (count($fotos) > 0) {
                    foreach ($fotos as $file) {
                        $valida = Common::validarImagem($file, $config);

                        if (!empty($valida)) {
                            array_push($ret, $valida);
                        }
                    }
                }

                if (empty($pasta_imagens)) {
                    $pasta_imagens = 'IMGS_PRD_ID_'.$id;
                }

                if (empty($ret)) {
                    if (count($fotos) > 0) {
                        foreach ($fotos as $file) {
                            $ret = Common::salvarImagem($file, FOTOPRODUTO_URL . '/' . $pasta_imagens.'/large', $config);

                            if (boolval($ret['erro'])) {
                                array_push($erros, 'Erro na imagem: ' . $file['name'] . ' - ' . $ret['message']);
                                array_push($imgerr, $file['name']);
                            } else {
                                array_push($imagens, $ret['list']['imagem']);
                            }
                        }
                    }

                    $imagens = array_filter(array_unique($imagens));
                    foreach (array_keys($imagens, 'undefined') as $key) {
                        unset($imagens[$key]);
                    }
                    $dados = [
                        'TOKEN' => $token,
                        'PRD_ID' => $id,
                        'PASTA_IMAGENS' => $pasta_imagens,
                        'IMAGENS' => implode(',', $imagens)
                    ];

                    $editar = Common::retornoWSLista($this->model->CadastrarEditarProduto($dados, $tipoform));

                    if ($editar[0]['O_COD_RETORNO'] != 0) {
                        echo json_encode(['erro' => 1, 'imgerr' => '', 'retorno' => $editar[0]['O_DESC_CURTO'], 'page' => SITE_URL . '/Produtos/Produto/' . $id]);
                    } else {
                        echo json_encode(['erro' => 0, 'imgerr' => '', 'retorno' => 'Produto editado com sucesso!', 'page' => SITE_URL . '/Produtos/Produto/' . $id]);
                    }
                } else {
                    $msg = implode(',', $ret);
                    echo json_encode(['erro' => 1, 'imgerr' => implode(",", $ret), 'retorno' => 'Ocorreu os seguintes erros:', 'page' => SITE_URL . '/Produtos/Produto/' . $id]);
                }
            }
        }
    }

    function SetarImagemPrincipal()
    {
        $token = Session::get('token');

        $img = filter_input(INPUT_POST, "img");
        $prd = filter_input(INPUT_POST, "prd");

        $dados = [
            'TOKEN' => $token,
            'PRD_ID' => $prd,
            'IMG' => $img
        ];

        $deletar = $this->model->SetarImagemPrincipal($dados);

        if ($deletar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Produtos/Produto/' . $prd;
        } else {
            $msg = 'Realizado com sucesso!';
            $situacao = 'success';

            Session::set('tab_edit', 3);

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Produtos/Produto/' . $prd;
        }
    }

    function ExcluirImagem()
    {
        $token = Session::get('token');

        $img = filter_input(INPUT_POST, "img");
        $prd = filter_input(INPUT_POST, "prd");

        $dados = [
            'TOKEN' => $token,
            'IMG' => $img,
            'PRD_ID' => $prd
        ];

        $pasta = null;

        $listaimagens = Common::retornoWSLista($this->model->ImagemProduto($dados));

        foreach ($listaimagens as $row) {
            $pasta = $row['PASTA_IMAGENS'];

            if(is_dir(FOTOPRODUTO_PATH . '/' . $pasta)){
                if (file_exists(FOTOPRODUTO_PATH . '/' . $pasta . '/large/' . $row['IMAGEM'])) {
                    Common::removerImagem(FOTOPRODUTO_PATH . '/' . $pasta . '/large/' . $row['IMAGEM']);
                }
            }
        }

        //remove a pasta
        $deletar = $this->model->ExcluirImageProduto($dados);

        if ($deletar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Produtos/Produto/' . $prd;
        } else {
            $msg = 'Imagem deletada com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Produtos/Produto/' . $prd;
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'ID' => $id
        ];

        $produto = Common::retornoWSLista($this->model->DadosProduto($token, $id))[0];

        $deletar = $this->model->ExcluirProduto($dados);

        if ($deletar['list'][0]['O_COD_RETORNO'] != 0) {

            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Produtos';
        } else {
            //EXCLUIR AS IMAGENS
            $pasta = FOTOPRODUTO_PATH . '/' . $produto['PASTA_IMAGENS'];
            Common::removeDir($pasta);

            $msg = 'Produto deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Produtos';
        }
    }

    function Importar()
    {
        $token = Session::get('token');

        if (empty($_FILES['arquivo_importacao']['name'][0])) {
            $msg = 'É necessário selecionar um arquivo!';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Produtos');
        } else {

            $ret = Common::salvarArquivo($_FILES['arquivo_importacao']);
            $file = $ret['list']['documento'];

            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load(PASTAARQUIVOS_URL . '/' . $file);

            $campos = [];
            $importados = 0;
            $linhasimpo = 0;

            $naoimportados = [];

            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

                $linhas = [];

                foreach ($worksheet->getRowIterator() as $row) {
                    if ($row->getRowIndex() == 1) {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        foreach ($cellIterator as $cell) {
                            if (!is_null($cell)) {
                                array_push($campos, $cell->getCalculatedValue());
                            }
                        }
                    } else {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        $valores = [];
                        foreach ($cellIterator as $cell) {
                            $valor = $cell->getCalculatedValue();
                            array_push($valores, $valor);
                        }

                        if (!empty($valores[0])) {

                            $linhas['I_TOKEN'] = $token;

                            foreach ($valores as $key => $value) {
                                $linhas[$campos[$key]] = $value;
                            }

                            $linhas['I_PRD_PATH'] = strtolower(Common::removerEspacosPontos($linhas['I_PRD_NOME']));
                            $linhas['I_PRD_PRECO'] = Common::returnValor($linhas['I_PRD_PRECO']);
                            $linhas['I_PRD_PRECO_ANTERIOR'] = Common::returnValor($linhas['I_PRD_PRECO_ANTERIOR']);
                            $linhas['I_PRD_TAXA'] = Common::returnValor($linhas['I_PRD_TAXA']);

                            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarProduto($linhas, 4));

                            if (intval($cadastrar[0]['O_COD_RETORNO']) == 0) {
                                $importados++;
                            } else {
                                array_push($naoimportados, $linhas['I_PRD_NOME']);
                            }
                        }

                        $linhasimpo++;
                    }
                }
            }

            Common::removerArquivo($file);

            if ($importados > 0) {
                $msg = 'Foram importados ' . $importados . ' produtos com sucesso!';

                if (count($naoimportados) > 0) {
                    $msg .= '\nOs produtos ' . implode('\n', $naoimportados) . ' não foram importados.';
                }

                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Produtos');
            } else {
                $msg = 'Ocorreu algum erro ao importar os produtos';
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Produtos');
            }
        }
    }

    private function validarCamposObrigatorio($id = null, $tipo = null)
    {
        if (!empty($id)) {
            if ($tipo == 1) {
                $dados['Nome'] = filter_input(INPUT_POST, "nome");
                $dados['Preço'] = filter_input(INPUT_POST, "preco");
                $dados['Unidade'] = filter_input(INPUT_POST, "unidade");
            }

            if ($tipo == 2) {
                $dados['Nome Técnico'] = filter_input(INPUT_POST, "nome_tecnico");
                $dados['Marca'] = filter_input(INPUT_POST, "marca");
                $dados['Descrição Mínima'] = filter_input(INPUT_POST, "descricao");
                $dados['Ficha Técnica'] = filter_input(INPUT_POST, "ficha_tecnica");
            }
        }

        if (empty($tipo) && empty($id)) {
            $dados['Nome'] = filter_input(INPUT_POST, "nome");
            $dados['Preço'] = filter_input(INPUT_POST, "preco");
            $dados['Nome Técnico'] = filter_input(INPUT_POST, "nome_tecnico");
            $dados['Modelo'] = filter_input(INPUT_POST, "modelo");
            $dados['Marca'] = filter_input(INPUT_POST, "marca");
            $dados['Descrição Mínima'] = filter_input(INPUT_POST, "descricao");
            $dados['Ficha Técnica'] = filter_input(INPUT_POST, "ficha_tecnica");
        }

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Produtos/Produto/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Produtos/Produto');
        }
    }
}
