<?php
namespace Application\Controller\Estoque;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

require_once SYSTEM_PATH . 'vendor/santosdummont/vendor/autoload.php';

class Categorias extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelProdutos', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();
        $dados['titulopagina'] = "Categorias Cadastradas";
        $dados['acaoimportar'] = SITE_URL . "/Categorias/Importar";
        $dados['listacategorias'] = Common::retornoWSLista($this->model->ListaCategoriasCadastradas($token));
        parent::prepararView("Estoque/pag_categorias", $dados);
    }

    function ListaCategorias()
    {
        $token = Session::get('token');

        $data = [];
        $params = $_REQUEST;

        $columns = array(
            0 => 'CT.CAT_ID',
            1 => 'CT.CAT_NOME',
            2 => 'CT2.CAT_NOME',
            3 => 'CT.CAT_STATUS'
        );

        $parametros['nome'] = null;
        $parametros['pai'] = null;
        $parametros['status'] = null;

        if (!empty($params['columns'][1]['search']['value'])) {
            $parametros['nome'] = $params['columns'][1]['search']['value'];
        }

        if (!empty($params['columns'][2]['search']['value'])) {
            $parametros['pai'] = $params['columns'][2]['search']['value'];
        }

        if (!empty($params['columns'][3]['search']['value'])) {
            $parametros['status'] = $params['columns'][3]['search']['value'];
        }

        $parametros['token'] = $token;
        $parametros['order'] = $columns[$params['order'][0]['column']];
        $parametros['dir'] = $params['order'][0]['dir'];
        $parametros['start'] = $params['start'];
        $parametros['length'] = $params['length'];
        $parametros['nome'] = 0;

        $list = Common::retornoWSLista($this->model->ListaCategoriasBuscaCadastrados($parametros));

        foreach ($list as $prod) {
            $row = array();
            $row[] = $prod['CAT_ID'];
            $row[] = $prod['CAT_NOME'];
            switch ((int) $prod['CAT_STATUS']) {
                case 1:
                    $row[] = "Ativo";
                    break;
                default:
                    $row[] = "Inativo";
            }
            $row[] = $prod['CAT_PAI_NOME'];

            $row[] = null;

            $data[] = $row;
        }

        $output = [
            "draw" => intval($params['draw']),
            "recordsTotal" => $list[0]['TOTALGERAL'],
            "recordsFiltered" => $list[0]['TOTAL'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    function Categoria($id = null)
    {
        $token = Session::get('token');

        if (!empty($id)) {
            $dados['titulopagina'] = "Editar Categoria";
            $dados['categoria'] = Common::retornoWSLista($this->model->DadosCategoria($token, $id))[0];
            $dados['urlaction'] = SITE_URL . "/Categorias/Editar";
            $dados['acaobotao'] = 'Salvar Edição';
        } else {
            $dados['titulopagina'] = "Nova Categoria";
            $dados['urlaction'] = SITE_URL . "/Categorias/Inserir";
            $dados['acaobotao'] = 'Cadastrar';
        }

        $dados['listacategorias'] = Common::retornoWSLista($this->model->ListaCategoriasCadastradas($token));
        parent::prepararView("Estoque/cad_categoria", $dados);
    }

    function AtivaInativa($id, $tipo)
    {
        $dados = [
            'TOKEN' => Session::get('token'),
            'CAT_ID' => $id,
            'CAT_STATUS' => intval($tipo) == 1 ? 1 : 2,
        ];

        $cadastrar = $this->model->AtivaInativaCategoria($dados);

        if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $cadastrar['list'][0]['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $cadastrar['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = intval($tipo) == 1 ? 'Categoria Ativado com sucesso!' : 'Categoria Inativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $cadastrar['list'][0]['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function Inserir()
    {
        $token = Session::get('token');

        $this->validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $status = filter_input(INPUT_POST, "status");
        $categoria = filter_input(INPUT_POST, "categoria");
        $menu_principal = filter_input(INPUT_POST, "menu_principal");
        $nivel = filter_input(INPUT_POST, "nivel");

        $pasta = '';

        if (isset($submit)) {

            $imagens = [];

            if (!empty($_FILES['imagem']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 2100, 'altura' => 240);

                $ret = Common::salvarImagem($_FILES['imagem'], FOTOCATEGORIAS_URL . '/' . $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            if ($categoria != 0) {
                $catpai = Common::retornoWSLista($this->model->DadosCategoria($token, $categoria))[0];
                $pai = Common::removerEspacosPontos($catpai['CAT_NOME']);
                $cat = Common::removerEspacosPontos($nome);

                $url = $pai . '-' . $cat;
            } else {
                $url = Common::removerEspacosPontos($nome);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'CAT_ID' => 0,
                'CAT_NOME' => $nome,
                'CAT_STATUS' => $status,
                'CAT_PAI' => $categoria,
                'CAT_POSICAO' => $menu_principal,
                'CAT_NIVEL' => $nivel,
                'CAT_URL' => strtolower($url),
                'CAT_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : '',
            ];

            $cadastrar = $this->model->CadastrarEditarCategoria($dados);

            if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                $img = implode(',', $imagens);

                if (!empty($img)) {
                    Common::removerImagem(FOTOCATEGORIAS_URL . '/' . $img);
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Categorias');
            } else {
                $msg = 'Categoria cadastrada com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Categorias');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $status = filter_input(INPUT_POST, "status");
        $categoria = filter_input(INPUT_POST, "categoria");
        $menu_principal = filter_input(INPUT_POST, "menu_principal");
        $nivel = filter_input(INPUT_POST, "nivel");

        $cat = Common::retornoWSLista($this->model->DadosCategoria($token, $id))[0];

        $this->validarCamposObrigatorio($id, $cat['CAT_IMAGEM']);

        $pasta = '';

        if (isset($submit)) {

            $imagens = [];

            if (!empty($_FILES['imagem']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 2100, 'altura' => 240);

                $ret = Common::salvarImagem($_FILES['imagem'], FOTOCATEGORIAS_URL . '/' . $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            if ($categoria != 0) {
                $catpai = Common::retornoWSLista($this->model->DadosCategoria($token, $categoria))[0];
                $pai = Common::removerCaracteresEspeciais($catpai['CAT_NOME']);
                $cat = Common::removerCaracteresEspeciais($nome);

                $url = $pai . '/' . $cat;
            } else {
                $url = Common::removerEspacosPontos($nome);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'CAT_ID' => $id,
                'CAT_NOME' => $nome,
                'CAT_STATUS' => $status,
                'CAT_PAI' => $categoria,
                'CAT_POSICAO' => $menu_principal,
                'CAT_NIVEL' => $nivel,
                'CAT_URL' => strtolower($url),
                'CAT_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : $cat['CAT_IMAGEM'],
            ];

            $cadastrar = $this->model->CadastrarEditarCategoria($dados);

            if ($cadastrar['list'][0]['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['list']['O_DESC_CURTO'];
                $situacao = 'danger';

                $img = implode(',', $imagens);

                if (!empty($img)) {
                    Common::removerImagem(FOTOCATEGORIAS_URL . '/' . $img);
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Categorias');
            } else {
                $msg = 'Categoria editada com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Categorias');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");

        $token = Session::get('token');
        $categoria = Common::retornoWSLista($this->model->DadosCategoria($token, $id))[0];

        $dados = [
            'TOKEN' => Session::get('token'),
            'ID' => $id
        ];

        $deletar = $this->model->ExcluirCategoria($dados);

        if ($deletar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Categorias';
        } else {
            $msg = 'Categoria deletada com sucesso!';
            $situacao = 'success';

            Common::removerImagem(FOTOCATEGORIAS_URL . '/' . $categoria['CAT_IMAGEM']);

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Categorias';
        }
    }

    function RemoveImagem()
    {
        $id = filter_input(INPUT_POST, "id");

        $token = Session::get('token');
        $categoria = Common::retornoWSLista($this->model->DadosCategoria($token, $id))[0];

        $dados = [
            'TOKEN' => Session::get('token'),
            'ID' => $id
        ];

        $deletar = $this->model->ExcluirImagemCategoria($dados);

        if ($deletar['list'][0]['O_COD_RETORNO'] != 0) {
            $msg = $deletar['list']['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Categorias';
        } else {
            $msg = 'Imagem removida com sucesso!';
            $situacao = 'success';

            Common::removerImagem(FOTOCATEGORIAS_URL . '/' . $categoria['CAT_IMAGEM']);

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Categorias';
        }
    }

    function Importar()
    {
        $token = Session::get('token');

        if (empty($_FILES['arquivo_importacao']['name'][0])) {
            $msg = 'É necessário selecionar um arquivo!';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Produtos');
        } else {

            $ret = Common::salvarArquivo($_FILES['arquivo_importacao']);
            $file = $ret['list']['documento'];

            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load(PASTAARQUIVOS_URL . '/' . $file);

            $campos = [];
            $importados = 0;
            $linhasimpo = 0;

            $naoimportados = [];

            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

                $linhas = [];

                foreach ($worksheet->getRowIterator() as $row) {
                    if ($row->getRowIndex() == 1) {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        foreach ($cellIterator as $cell) {
                            if (!is_null($cell)) {
                                array_push($campos, $cell->getCalculatedValue());
                            }
                        }
                    } else {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);

                        $valores = [];
                        foreach ($cellIterator as $cell) {
                            $valor = $cell->getCalculatedValue();
                            array_push($valores, $valor);
                        }

                        if (!empty($valores[0])) {

                            $linhas['TOKEN'] = $token;
                            $linhas['CAT_ID'] = 0;
                            $linhas['CAT_PAI'] = 0;
                            $linhas['CAT_NIVEL'] = 0;
                            $linhas['CAT_POSICAO'] = 0;
                            $linhas['CAT_IMAGEM'] = "";

                            foreach ($valores as $key => $value) {
                                $linhas[$campos[$key]] = $value;
                            }

                            $linhas['CAT_URL'] = strtolower(Common::removerEspacosPontos($linhas['CAT_NOME']));

                            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarCategoria($linhas));

                            if (intval($cadastrar[0]['O_COD_RETORNO']) == 0) {
                                $importados++;
                            } else {
                                array_push($naoimportados, $linhas['CAT_NOME']);
                            }
                        }

                        $linhasimpo++;
                    }
                }
            }

            Common::removerArquivo($file);

            if ($importados > 0) {
                $msg = 'Foram importados ' . $importados . ' categorias com sucesso!';

                if (count($naoimportados) > 0) {
                    $msg .= '\nAs categorias ' . implode('\n', $naoimportados) . ' não foram importados.';
                }

                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Categorias');
            } else {
                $msg = 'Ocorreu algum erro ao importar as categorias';
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Categorias');
            }
        }
    }

    private function validarCamposObrigatorio($id = null, $img = null)
    {
        $dados['Nome da Categoria'] = filter_input(INPUT_POST, "nome");

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Categorias/Categoria/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Categorias/Categoria');
        }
    }
}
