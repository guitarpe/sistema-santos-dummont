<?php

namespace Application\Controller\Estoque;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class ImagensLote extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelProdutos', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $dados['titulopagina'] = "Imagens em Lote";
        $dados['urlaction'] = "";
        $dados['acaobotao'] = "Publicar Imagens";
        parent::prepararView("Estoque/pag_imagens_lote", $dados);
    }

    function UploadImagems()
    {
        $token = Session::get('token');

        if (count($_FILES['fotos']) > 1) {
            $fotos = Common::reArrayFiles($_FILES['fotos']);
        } else {
            $fotos = $_FILES['fotos'];
        }

        $config = [
            'tamanho' => 4999999,
            'largura' => 800,
            'altura' => 700
        ];

        $imagens = [];
        $erros = [];
        $imgerr = [];

        if (count($fotos) > 0) {
            foreach ($fotos as $file) {
                $sku = explode('.', $file['name'])[0];

                $dados = [
                    'I_TOKEN' => $token,
                    'I_PRD_SKU' => $sku
                ];

                $dadosprd = Common::retornoWSLista($this->model->DadosProdutoPorSKU($dados));

                if (intval($dadosprd[0]['O_PRD_ID']) > 0) {
                    $pasta = $dadosprd[0]['O_PASTA'];

                    $ret = Common::salvarImagemAjax($file, FOTOPRODUTO_URL . '/' . $pasta.'/large', $config);

                    if (boolval($ret['erro'])) {
                        array_push($erros, 'Erro na imagem: ' . $file['name'] . ' - ' . $ret['message']);
                        array_push($imgerr, $file['name']);
                    } else {
                        array_push($imagens, $ret['list']['imagem']);

                        $dados['I_IMAGEM'] = $ret['list']['imagem'];

                        $cadastrar = Common::retornoWSLista($this->model->GravaProdutoPorSKU($dados));

                        if (intval($cadastrar[0]['O_COD_RETORNO']) != 0) {
                            array_push($erros, 'Erro na imagem: ' . $cadastrar[0]['O_IMAGEM'] . ' - ' . $cadastrar[0]['O_DESC_CURTO']);
                            array_push($imgerr, $cadastrar[0]['O_IMAGEM']);
                        }
                    }
                }
            }
        }

        echo json_encode(['erros' => implode(",", $erros), 'imgerr' => implode(",", $imgerr), 'sucesso' => implode(",", $imagens)]);
    }
}
