<?php
namespace Application\Controller\Regras;

use santosdummont\Controller,
    santosdummont\Common,
    santosdummont\Session;

class Regras extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function Taxas()
    {
        $dados['titulopagina'] = "Taxas";
        parent::prepararView("Regras/pag_taxas", $dados);
    }
}
