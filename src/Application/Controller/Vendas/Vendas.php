<?php

namespace Application\Controller\Vendas;

require_once SYSTEM_PATH . 'vendor/santosdummont/vendor/autoload.php';

use santosdummont\Controller;
use santosdummont\Common;
use santosdummont\Session;

class Vendas extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelVenda', 'model');
        parent::loadModel('Application\Model\ModelConfig', 'modelsistema');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    public function Pedidos()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Pedidos Realizados";
        $dados['acaoimportar'] = SITE_URL . "/Vendas/Importar";
        $dados['listapedidos'] = Common::retornoWSLista($this->model->ListaPedidosRealizados($token));
        $dados['contatosite'] = Common::retornoWSLista($this->modelsistema->ConfiguracoesADM($token))[0];
        parent::prepararView("Vendas/pag_pedidos", $dados);
    }

    public function Detalhe($id)
    {
        $token = Session::get('token');
        $dados['pedido'] = Common::retornoWSLista($this->model->ListaDetalhePedido($token, $id))[0];
        $dados['itenspedido'] = Common::retornoWSLista($this->model->ListaItensDetalhePedido($token, $id));
        $dados['contatosite'] = Common::retornoWSLista($this->modelsistema->ConfiguracoesADM($token))[0];
        parent::prepararBasicView("Vendas/pag_modal_detalhe", $dados);
    }

    public function Processar()
    {
        $ped = filter_input(INPUT_POST, 'pedido');

        $countpedidos = explode(",", $ped);

        $token = Session::get('token');

        $dados['countpedidos'] = count($countpedidos);

        if (count($countpedidos) == 1) {
            $dados['pedido'] = Common::retornoWSLista($this->model->ListaDetalhePedido($token, $ped))[0];
        }
        $dados['valorpedido'] = $ped;
        $dados['separarestoque'] = SITE_URL . "/Vendas/EnviarParaSeparacao";
        $dados['separarparenvio'] = SITE_URL . "/Vendas/SepararParaEnvio";
        $dados['cancelarpedido'] = SITE_URL . "/Vendas/Cancelar";
        $dados['reverterpedido'] = SITE_URL . "/Vendas/Reverter";
        parent::prepararBasicView("Vendas/pag_processar_pedido", $dados);
    }

    public function EnviarEmail()
    {
        $ped = filter_input(INPUT_POST, 'pedido');

        $dados['pedido'] = $ped;
        $dados['enviarmensagem'] = SITE_URL . "/Vendas/EnviarMensagem";
        parent::prepararBasicView("Vendas/pag_modal_enviar_mensagem", $dados);
    }

    public function Imprimir($pedidos)
    {
        $token = Session::get('token');

        $countpedidos = explode(",", $pedidos);

        $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];
        $contatosite = Common::retornoWSLista($this->modelsistema->ConfiguracoesADM($token))[0];

        $numped = null;

        $pdf = new \FPDF("P", "pt", "A4");
        $caminhologo = PATHSTATIC . '/' . $config['@LOGOTIPO'];
        $pdf->AddPage();
        $pdf->Image($caminhologo);

        $pdf->SetFont('arial', 'B', 14);
        $pdf->Cell(0, 5, "Pedidos", 0, 1, 'C');
        $pdf->Cell(0, 5, "", "B", 1, 'C');
        $pdf->Ln(8);

        foreach ($countpedidos as $ped) {

            $pedido = Common::retornoWSLista($this->model->ListaDetalhePedido($token, $ped))[0];
            $logomeiopag = PATHSTATIC . '/img/meiospagamentos/' . $pedido['MEIO_IMG'];
            $logomeioentrega = PATHSTATIC . '/img/' . $pedido['MEIO_IMG_ENT'];
            $itenspedido = Common::retornoWSLista($this->model->ListaItensDetalhePedido($token, $ped));

            $numped = $pedido['TOKEN'];

            $pdf->SetFont('arial', '', 10);
            $pdf->Cell(100, 15, utf8_decode("Identificação:"), 0, 0, 'L');
            $pdf->SetFont('arial', 'B', 10);
            $pdf->Cell(160, 15, $pedido["TOKEN"], 0, 0, 'L');
            $pdf->Cell(100, 15, utf8_decode("Cliente:"), 0, 1, 'L');

            $pdf->SetFont('arial', '', 10);
            $pdf->Cell(100, 15, "Total do Pedido:", 0, 0, 'L');
            $pdf->setFont('arial', 'B', 10);
            $pdf->Cell(160, 15, 'R$ ' . number_format($pedido["VALOR_PEDIDO"], 2, ',', '.'), 0, 0, 'L');
            $pdf->SetFont('arial', '', 9);
            $pdf->Cell(200, 15, utf8_decode('NOME: ' . $pedido['NOME_COMPLETO']), 0, 1, 'L');

            $pdf->SetFont('arial', '', 10);
            $pdf->Cell(100, 12, "Produtos:", 0, 0, 'L');
            $pdf->setFont('arial', 'B', 10);
            $pdf->Cell(160, 12, 'R$ ' . number_format($pedido['VALOR_PRODUTOS'], 2, ',', '.'), 0, 0, 'L');
            $pdf->SetFont('arial', '', 9);
            $pdf->Cell(200, 15, utf8_decode('E-MAIL: ' . $pedido['EMAIL']), 0, 1, 'L');

            $pdf->SetFont('arial', '', 10);
            $pdf->Cell(100, 12, "Frete:", 0, 0, 'L');
            $pdf->setFont('arial', 'B', 10);
            $pdf->Cell(160, 12, 'R$ ' . number_format($pedido['VALOR_FRETE'], 2, ',', '.'), 0, 0, 'L');
            $pdf->SetFont('arial', '', 9);
            $pdf->Cell(200, 15, utf8_decode($pedido['TIPO_DOC'] == 'CPF' ? 'CPF: ' . $pedido['NUM_DOC'] : 'CNPJ: ' . $pedido['NUM_DOC']), 0, 1, 'L');

            $pdf->SetFont('arial', '', 10);
            $pdf->Cell(100, 15, "Status:", 0, 0, 'L');
            $pdf->setFont('arial', 'B', 10);
            $pdf->Cell(160, 15, utf8_decode($pedido["STATUS_PED"]), 0, 0, 'L');
            $pdf->SetFont('arial', '', 9);
            $pdf->Cell(200, 15, utf8_decode('TELEFONE DE CONTATO: ' . $pedido['TELEFONE']), 0, 1, 'L');

            $pdf->SetFont('arial', '', 10);
            $pdf->Cell(100, 15, "Data do Pedido:", 0, 0, 'L');
            $pdf->setFont('arial', 'B', 10);
            $pdf->Cell(100, 15, $pedido["DATA_CADASTRO"], 0, 1, 'L');

            $pdf->SetFont('arial', '', 10);
            $pdf->Cell(100, 15, "Quantidade de Itens:", 0, 0, 'L');
            $pdf->setFont('arial', 'B', 10);
            $pdf->Cell(160, 15, $pedido["QTDE_ITENS"], 0, 0, 'L');

            if (intval($pedido['COD_MP_PAG']) != 719680) {
                $pdf->Cell(100, 15, utf8_decode("Local de Entrega"), 0, 1, 'L');
                $pdf->SetFont('arial', '', 9);
                $pdf->Cell(260, 15, "", 0, 0, 'L');
                $pdf->Cell(200, 15, utf8_decode($pedido['END_ENDERECO'] . ', ' . $pedido['END_NUM']), 0, 1, 'L');
                $pdf->Cell(260, 15, "", 0, 0, 'L');
                $pdf->Cell(200, 15, utf8_decode('CEP: ' . $pedido['END_CEP'] . ', ' . $pedido['END_BAIRRO'] . ', ' . $pedido['END_CIDADE'] . '-' . $pedido['END_UF']), 0, 1, 'L');
                if (!empty($pedido['END_COMPLEMENTO'])) {
                    $pdf->Cell(260, 15, "", 0, 0, 'L');
                    $pdf->Cell(200, 15, utf8_decode('Complemento: ' . $pedido['END_COMPLEMENTO']), 0, 1, 'L');
                }
            } else {
                $pdf->Cell(100, 15, utf8_decode("Local de Retirada"), 0, 1, 'L');
                $pdf->Cell(260, 15, "", 0, 0, 'L');
                $pdf->SetFont('arial', '', 9);
                $pdf->Cell(200, 15, utf8_decode('Loja Santos Dumont Componentes Eletrônicos'), 0, 1, 'L');
                $pdf->Cell(260, 15, "", 0, 0, 'L');
                $pdf->Cell(200, 15, utf8_decode($contatosite['@ENDERECO'] . ', ' . $contatosite['@NUMERO']), 0, 1, 'L');
                $pdf->Cell(260, 15, "", 0, 0, 'L');
                $pdf->Cell(200, 15, utf8_decode('CEP: ' . $contatosite['@CEP'] . ', ' . $contatosite['@BAIRRO'] . ', ' . $contatosite['@CIDADE'] . '-' . $contatosite['@UF']), 0, 1, 'L');
            }

            $pdf->Cell(260, 15, "", 0, 1, 'L');
            $pdf->SetFont('arial', 'B', 10);
            $pdf->Cell(260, 15, "Forma de Pagamento", 0, 1, 'L');
            $pdf->Image($logomeiopag, null, null, -150);
            $pdf->Cell(100, 15, utf8_decode("Método de entrega"), 0, 1, 'L');
            $pdf->Image($logomeioentrega, null, null, -300);


            $pdf->ln(10);

            $pdf->SetFont('arial', 'B', 10);
            $pdf->Cell(100, 15, utf8_decode("Observações:"), 0, 1, 'L');
            $pdf->setFont('arial', '', 8);
            $pdf->MultiCell(0, 15, $pedido['COMENTARIO'], 0, 'J');

            $pdf->ln(10);

            //detalhemeNto
            $pdf->SetFont('arial', 'B', 10);
            $pdf->Cell(100, 15, "Itens do Pedidos:", 0, 1, 'L');
            $pdf->ln(5);
            $pdf->Cell(0, 5, "", "B", 1, 'C');

            $pdf->SetFont('arial', 'B', 10);
            $pdf->Cell(250, 15, "Item", 0, 0, 'L');
            $pdf->Cell(100, 15, utf8_decode("Preço Unitário"), 0, 0, 'L');
            $pdf->Cell(100, 15, "Quantidade", 0, 0, 'L');
            $pdf->Cell(100, 15, "Total", 0, 0, 'L');

            $pdf->ln(10);
            $pdf->Cell(0, 5, "", "B", 1, 'C');
            $pdf->ln(5);


            foreach ($itenspedido as $item) {
                $pdf->setFont('arial', '', 8);
                $pdf->Cell(250, 15, $item["PRD_NOME"], 0, 0, 'L');
                $pdf->Cell(100, 15, 'R$ ' . number_format($item["PRD_PRECO"], 2, ',', '.'), 0, 0, 'L');
                $pdf->Cell(100, 15, $item["QTDE"], 0, 0, 'L');
                $pdf->Cell(100, 15, 'R$ ' . number_format($item["PRECO_TOTAL"], 2, ',', '.'), 0, 0, 'L');

                $pdf->ln(10);
                $pdf->Cell(0, 5, "", "B", 1, 'C');
            }
        }

        if (count($countpedidos) > 1) {
            $file_name = "SANTOS_DUMONT_Relatorios_Pedidos.pdf";
        } else {
            $file_name = "SANTOS_DUMONT_" . $numped . ".pdf";
        }

        $pdf->Output($file_name, "I");
    }

    public function EnviarMensagem()
    {
        $token = Session::get('token');

        $pedidos = filter_input(INPUT_POST, "pedido");
        $assunto = filter_input(INPUT_POST, "assunto");
        $mensagem = filter_input(INPUT_POST, "mensagem");

        $countpedidos = explode(",", $pedidos);

        $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];
        $info = [
            'host' => Common::encrypt_decrypt('decrypt', $config['@SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['@SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['@SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['@SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['@MAIL_CONTATO_DEFAULT']),
            'fromname' => $config['@NOME_CONTATO_DEFAULT'],
            'logotipo' => $config['@LOGOTIPO']
        ];

        $ct = 0;
        foreach ($countpedidos as $ped) {

            $pedido = Common::retornoWSLista($this->model->ListaDetalhePedido($token, $ped))[0];

            $html = '<html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <title>Santos Dumont - ' . $assunto . '</title>
                            </head>
                            <body>
                                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                    <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                        <img src="' . URL . PATHSTATIC_URL . '/' . $info['logotipo'] . '">
                                    </div>
                                    <div class="container" style="padding:20px">
                                        <table>
                                            <tr>
                                                <td>
                                                    <h2><b></b></h2>
                                                    <h2><b>' . $assunto . '</b></h2><br/>
                                                    <p>' . $mensagem . '</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>';

            Common::dispararEmailPersonalizado($assunto, $html, $pedido['EMAIL'], $info);
            $ct++;
        }

        if ($ct == count($countpedidos)) {
            $msg = 'Mensagem enviada com sucesso!';
            $situacao = 'success';
        } else {
            $msg = 'Algumas mensagens não foram enviadas!';
            $situacao = 'warning';
        }

        Common::alert($msg, $situacao, 'acao');
        Common::redir('Vendas/Pedidos');
    }

    public function Cancelar($pedidos)
    {
        $token = Session::get('token');

        $countpedidos = explode(",", $pedidos);

        $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];

        $info = [
            'host' => Common::encrypt_decrypt('decrypt', $config['@SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['@SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['@SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['@SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['@MAIL_CONTATO_DEFAULT']),
            'fromname' => $config['@NOME_CONTATO_DEFAULT'],
            'logotipo' => $config['@LOGOTIPO']
        ];

        $ct = 0;
        foreach ($countpedidos as $ped) {
            $pedido = Common::retornoWSLista($this->model->ListaDetalhePedido($token, $ped))[0];

            $separar = Common::retornoWSLista($this->model->CancelarPedido($token, $ped))[0];

            if (intval($separar['O_COD_RETORNO']) != 0) {
                $msg = $separar['list']['O_DESC_CURTO'];
                $situacao = 'danger';
            } else {

                $html = '<html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <title>Santos Dumont - Pedido Cancelad</title>
                            </head>
                            <body>
                                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                    <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                        <img src="' . URL . PATHSTATIC_URL . '/' . $info['logotipo'] . '">
                                    </div>
                                    <div class="container" style="padding:20px">
                                        <table>
                                            <tr>
                                                <td>
                                                    <h2><b></b></h2>
                                                    <h2><b></b></h2>
                                                    <br/>Pedido Cancelado!
                                                    <br/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>';

                Common::dispararEmailPersonalizado('Pedido Cancelado', $html, $pedido['EMAIL'], $info);

                $msg = 'Pedido cancelado com sucesso!';
                $situacao = 'success';
            }

            $ct++;
        }

        if ($ct == count($countpedidos)) {
            $msg = 'Pedido cancelado com sucesso!';
            $situacao = 'success';
        } else {
            $msg = 'Alguns pedidos não foram cancelados!';
            $situacao = 'warning';
        }

        Common::alert($msg, $situacao, 'acao');
        return SITE_URL . '/Vendas/Pedidos';
    }

    public function EnviarParaSeparacao()
    {
        $token = Session::get('token');

        $ped = filter_input(INPUT_POST, 'pedido');

        $countpedidos = explode(",", $ped);

        $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];

        $info = [
            'host' => Common::encrypt_decrypt('decrypt', $config['@SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['@SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['@SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['@SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['@MAIL_CONTATO_DEFAULT']),
            'fromname' => $config['@NOME_CONTATO_DEFAULT'],
            'logotipo' => $config['@LOGOTIPO']
        ];

        $ct = 0;
        foreach ($countpedidos as $ped) {
            $pedido = Common::retornoWSLista($this->model->ListaDetalhePedido($token, $ped))[0];

            $separar = Common::retornoWSLista($this->model->SepararDoEstoque($token, $ped))[0];

            if (intval($separar['O_COD_RETORNO']) != 0) {
                $msg = $separar['list']['O_DESC_CURTO'];
                $situacao = 'danger';
            } else {

                $html = '<html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <title>Santos Dumont - Pedido em processo de separação de estoque</title>
                            </head>
                            <body>
                                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                    <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                        <img src="' . URL . PATHSTATIC_URL . '/' . $info['logotipo'] . '">
                                    </div>
                                    <div class="container" style="padding:20px">
                                        <table>
                                            <tr>
                                                <td>
                                                    <h2><b></b></h2>
                                                <h2><b></b></h2>
                                                <br/>Pedido em processo de separação de estoque!
                                                <br/>
                                            </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>';

                Common::dispararEmailPersonalizado('Pedido em processo de separação de estoque', $html, $pedido['EMAIL'], $info);

                $msg = 'Pedido separado para estoque com sucesso!';
                $situacao = 'success';
            }
            $ct++;
        }

        if ($ct == count($countpedidos)) {
            $msg = 'Mensagem enviada com sucesso!';
            $situacao = 'success';
        } else {
            $msg = 'Alguns pedidos não foram separados!';
            $situacao = 'warning';
        }

        Common::alert($msg, $situacao, 'acao');
        return SITE_URL . '/Vendas/Pedidos';
    }

    public function SepararParaEnvio()
    {
        $token = Session::get('token');

        $ped = filter_input(INPUT_POST, 'pedido');

        $dados['numpedidos'] = $ped;
        $dados['pedidos'] = Common::retornoWSLista($this->model->ListaDetalhePedido($token, $ped));
        $dados['actionseparar'] = SITE_URL . "/Vendas/Separar";
        parent::prepararBasicView("Vendas/pag_modal_enviar_pedido_cliente", $dados);
    }

    public function Separar()
    {
        $token = Session::get('token');

        $pedidos = filter_input(INPUT_POST, 'pedido', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $localizadores = filter_input(INPUT_POST, 'localizador', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $observacao = filter_input(INPUT_POST, 'obeservacao');

        $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];
        $info = [
            'host' => Common::encrypt_decrypt('decrypt', $config['@SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['@SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['@SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['@SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['@MAIL_CONTATO_DEFAULT']),
            'fromname' => $config['@NOME_CONTATO_DEFAULT'],
            'logotipo' => $config['@LOGOTIPO']
        ];

        $ct = 0;
        foreach ($pedidos as $key => $value) {

            $dados = [
                'I_TOKEN' => $token,
                'I_PED_ID' => $pedidos[$key],
                'I_LOCALIZADOR' => $localizadores[$key],
                'I_OBSERVACAO' => $observacao,
            ];

            $pedido = Common::retornoWSLista($this->model->ListaDetalhePedido($token, $pedidos[$key]))[0];
            $separar = Common::retornoWSLista($this->model->SepararEnvioCliente($dados))[0];

            if (intval($separar['O_COD_RETORNO']) != 0) {
                $msg = $separar['list']['O_DESC_CURTO'];
                $situacao = 'danger';
            } else {

                $html = '<html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <title>Santos Dumont - Pedido em processo de envio</title>
                            </head>
                            <body>
                                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                    <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                        <img src="' . URL . PATHSTATIC_URL . '/' . $info['logotipo'] . '">
                                    </div>
                                    <div class="container" style="padding:20px">
                                        <table>
                                            <tr>
                                                <td>
                                                    <h2><b></b></h2>
                                                    <br/>Pedido em processo de envio!
                                                    <br/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>';

                Common::dispararEmailPersonalizado('Pedido em processo de envio', $html, $pedido['EMAIL'], $info);

                $msg = 'Pedido separado para envio com sucesso!';
                $situacao = 'success';
            }

            $ct++;
        }

        if ($ct == count($pedidos)) {
            $msg = 'Mensagem enviada com sucesso!';
            $situacao = 'success';
        } else {
            $msg = 'Alguns pedidso não foram separados!';
            $situacao = 'warning';
        }

        Common::alert($msg, $situacao, 'acao');
        Common::redir('Vendas/Pedidos');
    }

    public function Reverter()
    {
        $token = Session::get('token');

        $ped = filter_input(INPUT_POST, 'pedido');

        $countpedidos = explode(",", $ped);

        $config = Common::retornoWSLista($this->model->Configuracoes($token))[0];
        $info = [
            'host' => Common::encrypt_decrypt('decrypt', $config['@SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['@SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['@SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['@SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['@MAIL_CONTATO_DEFAULT']),
            'fromname' => $config['@NOME_CONTATO_DEFAULT'],
            'logotipo' => $config['@LOGOTIPO']
        ];

        $ct = 0;
        foreach ($countpedidos as $ped) {
            $pedido = Common::retornoWSLista($this->model->ListaDetalhePedido($token, $ped))[0];

            $separar = Common::retornoWSLista($this->model->ReverterPedido($token, $ped))[0];

            if (intval($separar['O_COD_RETORNO']) != 0) {
                $msg = $separar['list']['O_DESC_CURTO'];
                $situacao = 'danger';
            } else {

                $html = '<html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <title>Santos Dumont - Pedido removido da separação de estoque</title>
                            </head>
                            <body>
                                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                    <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                        <img src="' . URL . PATHSTATIC_URL . '/' . $info['logotipo'] . '">
                                    </div>
                                    <div class="container" style="padding:20px">
                                        <table>
                                            <tr>
                                                <td>
                                                <h2><b></b></h2>
                                                <br/>Pedido removido da separação de estoque!
                                                <br/>
                                            </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>';

                Common::dispararEmailPersonalizado('Pedido removido da separação de estoque', $html, $pedido['EMAIL'], $info);

                $msg = 'Pedido removido da separação de estoque com sucesso!';
                $situacao = 'success';
            }
            $ct++;
        }

        if ($ct == count($countpedidos)) {
            $msg = 'Mensagem enviada com sucesso!';
            $situacao = 'success';
        } else {
            $msg = 'Alguns pedidos não foram separados!';
            $situacao = 'warning';
        }

        Common::alert($msg, $situacao, 'acao');
        return SITE_URL . '/Vendas/Pedidos';
    }
}
