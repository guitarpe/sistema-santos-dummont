<?php

namespace Application\Controller\Common;

use santosdummont\Controller,
    santosdummont\Common;

require_once SYSTEM_PATH . 'vendor/santosdummont/vendor/autoload.php';

class Automatico extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelProdutos', 'model');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }
    }

    function InativarTokensExp()
    {

        $resetar = $this->model->InativarTokensExpirados();

        if ($resetar['list'][0]['O_COD_RETORNO'] == 0) {
            parent::log_registra('TOKENS EXPIRADOS INATIVADOS', $resetar['list'][0]['O_DESC_CURTO'], true);
        } else {
            parent::log_registra('ERRO AO INATIVAR TOKENS EXPIRADOS', $resetar['list'][0]['O_DESC_CURTO'], false);
        }
    }

    function JsonProdutos()
    {

        header("Content-type: application/json; charset=utf-8");

        $config = Common::retornoWSLista($this->model->ConfiguracoesNoToken())[0];
        $params['ip'] = $config['IP_INTEGRACAO'];

        echo parent::consumir_cetrus($params);
    }

    function SincronizarProdutosCron($limite = null)
    {
        try {
            set_time_limit(7200);
            ignore_user_abort(true);

            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );

            $json = file_get_contents(URL . SITE_URL . '/Automatico/JsonProdutos', false, stream_context_create($arrContextOptions));

            $json_data = json_decode($json, true);

            $config = Common::retornoWSLista($this->model->ConfiguracoesNoToken())[0];
            $params['ip'] = $config['IP_INTEGRACAO'];

            $count = 0;

            if (intval($json_data['erro']) == 0) {
                foreach ($json_data['produtos'] as $row) {
                    $parametros = [
                        'I_PRD_SKU' => $row["produto_Id"],
                        'I_PRD_UNIDADE' => $row["und_Est"],
                        'I_PRD_NOME' => $row['discr'],
                        'I_PRD_URL' => Common::removerEspacosPontos($row['discr']),
                        'I_PRD_QTDE_ESTOQUE' => $row['qtde_Estoque'],
                        'I_PRD_PRECO' => number_format((float) $row['preco'], 2, '.', ''),
                        'I_PRD_COD_CETRUS' => $row['marca_Id'],
                        'I_PRD_MARCA' => $row['marca'],
                        'I_PRD_MARCA_URL' => Common::removerEspacosPontos($row['marca']),
                        'I_PRD_PESO' => (string)$row['peso_Bruto'],
                        'I_PRD_ALTURA' => (string)$row['altura'],
                        'I_PRD_LARGURA' => (string)$row['largura'],
                        'I_PRD_STATUS' => $row['desativado'],
                        'I_PRD_COMPRIMENTO' => (string)$row['comprimento'],
                        'I_PRD_PROFUNDIDADE' => (string)$row['profundidade']
                    ];

                    $atualiza = $this->model->AtualizaAPIProdutoCron($parametros);

                    if (intval($atualiza['list'][0]['O_COD_RETORNO']) == 0) {

                        $params['sku'] = $row['produto_Id'];
                        $consumir = parent::consulta_cetrus($params);

                        $erroacesso = intval($consumir["erro"]);

                        if ($erroacesso != 0) {
                            parent::log_registra('Integração - Erro ao Acessar API - Remover da lista - ', $consumir["retorno"], false);
                            break;
                        } else {
                            if (intval($consumir["retorno"][0]['retorno']) == 1) {
                                parent::log_registra('Integração - Produto: ', $row['produto_Id'] . ' Atualizado', true);
                            } else {
                                parent::log_registra('Integração - Erro ao Remover da lista: Produto: ', $row['produto_Id'] . ' - ' . $consumir["retorno"][0]['erroMsg'], null, false);
                            }
                        }
                    } else {
                        parent::log_registra('Integração - Erro ao Atualizar Produto', $atualiza['list'][0]['O_DESC_CURTO'], false);
                    }

                    $count++;
                    if ($count == intval($limite)) {
                        break;
                    }
                }
            } else {
                parent::log_registra('Integração - Erro ao Acessar API - ', $json_data['msg'], false);
            }
        } catch (\Exception $ex) {
            parent::log_registra('Automatico', $ex->getMessage(), false);
        }
    }

    function SincronizarPedidosCron()
    {
        try {
            set_time_limit(7200);
            ignore_user_abort(true);

            $listapedidos = Common::retornoWSLista($this->model->ListaPedidosCron());
            $now = new \DateTime();

            \PagSeguro\Library::initialize();
            $options = [
                'initial_date' => $now->format('Y-M-dTH:i')
            ];

            $credenciais = $this->retornaCredenciaisPagSeguro();
            foreach ($listapedidos as $pedido) {
                $code = $pedido['CODE'];

                $response = \PagSeguro\Services\Transactions\Search\Code::search(
                    $credenciais,
                    $code,
                    $options
                );

                $dados['PED_ID'] = $pedido['PED_ID'];
                $dados['PED_STATUS'] = $response->getStatus();
                $dados['PED_DATE'] = $response->getLastEventDate();

                $atualiza = $this->model->AtualizaAPIPedidoCron($dados);

                if (intval($atualiza['list'][0]['O_COD_RETORNO']) == 0) {
                    parent::log_registra('Integração - PEDIDO: ', $dados['PED_ID'] . ' Sincronizado', true);
                } else {
                    parent::log_registra('Integração - Sincronizar Pedido Erro: ', $dados['PED_ID'] . ' - ' . $atualiza['list'][0]['O_DESC_CURTO'], null, false);
                }
            }
        } catch (\Exception $ex) {
            parent::log_registra('Integração - Sincronizar Pedido Erro:', Common::retornoMsgXml($ex->getMessage()), false);
        }
    }

    private function retornaCredenciaisPagSeguro()
    {
        $config = Common::retornoWSLista($this->model->ConfiguracoesNoToken())[0];
        $email = Common::encrypt_decrypt('decrypt', $config['PAGSEG_EMAIL']);

        if (!empty($email)) {
            if (!empty($config['PAGSEG_TIPO_ENVIO'])) {
                if ($config['PAGSEG_TIPO_ENVIO'] == 'sandbox') {
                    $token = Common::encrypt_decrypt('decrypt', $config['PAGSEG_SAND_TOKEN']);
                } else {
                    $token = Common::encrypt_decrypt('decrypt', $config['PAGSEG_PRD_TOKEN']);
                }

                $configPagSeguro = new \PagSeguro\Configuration\Configure();
                $configPagSeguro->setEnvironment($config['PAGSEG_TIPO_ENVIO']);
                $configPagSeguro->setCharset('UTF-8');
                $configPagSeguro->setLog(true, LOGS_URL . '/pseg.log');
                $configPagSeguro::setAccountCredentials($email, $token);

                return $configPagSeguro::getAccountCredentials();
            }
        }
    }
}
