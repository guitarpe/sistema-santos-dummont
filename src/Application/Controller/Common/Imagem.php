<?php

namespace Application\Controller\Common;

use Exception;
use santosdummont\Controller,
    santosdummont\Common;

class Imagem
{

    private $config = array();

    public function __construct($configPadrao = array('tamanho' => 4999999, 'largura' => 2000, 'altura' => 1500))
    {
        $this->config = $configPadrao;
    }

    public function executar($caminho, $file, $op = 0)
    {
        try {
            $msg = '';

            if ($caminho === null) {
                $msg = "Não foi possível localizar o diretório.";
            } else {
                $msg = $this->validarImagem($file);
            }

            if (strlen($msg) > 0) {
                Common::alert($msg, 'warning', 'acao', 'imagens');
                Common::voltar();
            }

            $imagemsalva = $this->criarImagemDiretorio($caminho, $file, $op);

            if (empty($imagemsalva)) {
                $ret = ['erro' => true, 'message' => null, 'list' => ['imagem' => null]];
            } else {
                $ret = ['erro' => false, 'message' => null, 'list' => ['imagem' => $imagemsalva]];
            }

            return $ret;
        } catch (\Exception $ex) {
            $ex->getMessage();
        }
    }

    public function executarajax($caminho, $file, $op = 0)
    {
        try {
            $msg = '';

            if ($caminho === null) {
                $msg = "Não foi possível localizar o diretório.";
            } else {
                $msg = $this->validarImagem($file);
            }

            if (strlen($msg) > 0) {
                $ret = ['erro' => true, 'message' => $msg, 'list' => ['imagem' => null]];
            } else {

                $ret = $this->criarImagemDiretorioajax($caminho, $file, $op);

                if ($ret['erro']) {
                    $ret = ['erro' => true, 'message' => $ret['message'], 'list' => ['imagem' => null]];
                } else {
                    $ret = ['erro' => false, 'message' => null, 'list' => ['imagem' => $ret['novo_nome']]];
                }
            }

            return $ret;
        } catch (\Exception $ex) {
            $ex->getMessage();
        }
    }

    private function regraNomenclaturaImagem($file)
    {
        $ext = pathinfo($file["name"], PATHINFO_EXTENSION);
        $nome_imagem = \md5(uniqid(\time())) . "." . $ext;

        return $nome_imagem;
    }

    private function convertToWebp($caminho, $imagem){

        $compression_quality = 80;

        if (!file_exists($caminho . '/' . $imagem)) {
            return false;
        }

        $nomewebp = explode('.',$imagem);

        $output_file = $nomewebp[0] . '.webp';

        if (file_exists($output_file)) {
            return $caminho . '/' . $output_file;
        }

        $file_type = strtolower(pathinfo($caminho . '/' . $imagem, PATHINFO_EXTENSION));

        switch ($file_type) {
            case 'jpeg':
            case 'jpg':
                $image = imagecreatefromjpeg($caminho . '/' . $imagem);
                break;

            case 'png':
                $image = imagecreatefrompng($caminho . '/' . $imagem);
                imagepalettetotruecolor($image);
                imagealphablending($image, true);
                imagesavealpha($image, true);
                break;

            case 'gif':
                $image = imagecreatefromgif($caminho . '/' . $imagem);
                break;
            default:
                return false;
        }

        $result = imagewebp($image, $caminho . '/' . $output_file, $compression_quality);

        if (false === $result) {
            return null;
        }

        imagedestroy($image);
        self::deletarImagem($caminho . '/' . $imagem);

        return $output_file;
    }

    private function criarImagemDiretorio($caminho, $file, $op)
    {
        if ($op == 0) {
            $novoNome = strtolower($this->regraNomenclaturaImagem($file));
        } else {
            $novoNome = strtolower($file["name"]);
        }

        $gravado = move_uploaded_file($file["tmp_name"], $caminho . '/' . $novoNome);

        if (!$gravado) {
            $msg = 'Ocorreu um erro no momento de salvar a imagem.';
            Common::alert($msg, 'warning', 'acao', 'imagens');
            Common::voltar();
        }

        $imagemnome = self::convertToWebp($caminho, $novoNome);

        return $imagemnome;
    }

    private function criarImagemDiretorioajax($caminho, $file, $op)
    {
        try{

            $nomeimagem = null;

            if ($op == 0) {
                $novoNome = strtolower($this->regraNomenclaturaImagem($file));
            } else {
                $novoNome = strtolower($file["name"]);
            }

            $gravado = move_uploaded_file($file["tmp_name"], $caminho . '/' . $novoNome);

            if (!$gravado) {
                $erro = true;
                $msg = 'Ocorreu um erro no momento de salvar a imagem.';
            } else {
                $erro = false;
                $msg = '';

                $nomeimagem = self::convertToWebp($caminho, $novoNome);
            }

            return ['erro' => $erro, 'message' => $msg, 'novo_nome' => $nomeimagem];

        }catch(Exception $ex){
            return ['erro' => true, 'message' => $ex->getMessage(), 'novo_nome' => $ex->getMessage()];
        }
    }

    public function validarImagem($file)
    {
        $aviso = "";
        $msgErro = [];

        if (empty($file['name'])) {
            $msgErro[] = "É necessário que selecione uma imagem.";
            goto gerarMsg;
        }

        if (!preg_match('/^[a-zA-Z0-9]+/', $file["name"])) {
            $msgErro[] = "Não é permitido caracteres especiais no nome da imagem.";
        }

        if (!preg_match("/^image\/(pjpeg|jpeg|jpg|png|gif|bmp|ico)$/", $file["type"])) {
            $msgErro[] = "Desculpe, isto não é uma imagem, não foi possível salvar a imagem.";
        } else {
            if ($file["size"] > $this->config["tamanho"]) {
                $tamanho = explode(".", ($this->config["tamanho"] / 1024));
                $msgErro[] = sprintf("Imagem com tamanho muito grande! O arquivo deve ser de no máximo %s KB.", $tamanho[0]);
            }

            $tamanhos = getimagesize($file["tmp_name"]);

            if ($tamanhos[0] > $this->config["largura"]) {
                $msgErro[] = sprintf("Largura da imagem não deve ser superior a %s pixels.", $this->config["largura"]);
            }

            if ($tamanhos[1] > $this->config["altura"]) {
                $msgErro[] = sprintf("Altura da imagem não deve ser superior a %s pixels.", $this->config["altura"]);
            }
        }

        gerarMsg:
        if (sizeof($msgErro)) {
            foreach ($msgErro as $msg) {
                $aviso .= $msg . "<br>";
            }

            return $aviso;
        }
    }

    public function excluir($caminho, $docs)
    {
        $msg = 'Erro na esclusão de imagens:\n\n';
        $erro = 0;
        foreach ($docs as $nome => $cod) {
            if (!$this->deletarImagemDiretorio($caminho, $nome)) {
                $msg .= $nome . 'Cód:' . $cod . '\n';
                $erro++;
            }
        }
        if ($erro > 0) {
        }
    }

    public function deletarImagemDiretorio($caminho, $arquivo)
    {
        if (file_exists($caminho . '/' . $arquivo)) {
            return unlink($caminho . '/' . $arquivo);
        }
    }

    public function deletarImagem($arquivo)
    {
        if (file_exists($arquivo)) {
            return unlink($arquivo);
        }
    }
    ################### CAPTURA DE IMAGEM WEBCAM ##############################
    //    function salvarFotoCapturadoWebcam($caminho, $cpf)
    //    {
    //        $imagem = $func->filterInputPost('fotoPerfil');
    //        return $this->gerarImagemWebcam($caminho, $cpf, $imagem);
    //    }
    //    private function gerarImagemWebcam($caminho, $cpf, $imgCapturado)
    //    {
    //        $img = str_replace('data:image/png;base64,', '', $imgCapturado);
    //        $img = str_replace(' ', '+', $img);
    //        $img = base64_decode($img);
    //        $novoNome = $cpf . date("__d-m-Y_s_", time()) . '_Perfil.png';
    //        $file = $caminho . '/' . $novoNome;
    //
    //        file_put_contents($file, $img);
    //
    //        $imagensSalvas = [];
    //
    //        $imagem = [
    //            'ISPD_URL' => $novoNome,
    //            'ISIN_COD' => 82
    //        ];
    //
    //        array_push($imagensSalvas, $imagem);
    //
    //        return $imagensSalvas;
    //    }
}
